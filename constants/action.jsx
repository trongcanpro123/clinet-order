export const LOAD_LIST_PAGE = 'LOAD_LIST_PAGE';
export const LOAD_LIST_PAGE_SUCCESS = 'LOAD_LIST_PAGE_SUCCESS';
export const LOAD_LIST_PAGE_FAILURE = 'LOAD_LIST_PAGE_FAILURE';

export const LOAD_FORM_PAGE = 'LOAD_FORM_PAGE';
export const LOAD_FORM_PAGE_SUCCESS = 'LOAD_FORM_PAGE_SUCCESS';
export const LOAD_FORM_PAGE_FAILURE = 'LOAD_FORM_PAGE_FAILURE';

export const GET_FULL_LIST = 'GET_FULL_LIST';
export const GET_FULL_LIST_SUCCESS = 'GET_FULL_LIST_SUCCESS';
export const GET_FULL_LIST_FAILURE = 'GET_FULL_LIST_FAILURE';

export const GET_LIST = 'GET_LIST';
export const GET_LIST_SUCCESS = 'GET_LIST_SUCCESS';
export const GET_LIST_FAILURE = 'GET_LIST_FAILURE';

export const GET_DATA = 'GET_DATA';
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS';
export const GET_DATA_FAILURE = 'GET_DATA_FAILURE';

export const CHANGE_QUERY = 'CHANGE_QUERY';
export const CHANGE_DATA = 'CHANGE_DATA';

export const SET_DEFAULT_QUERY = 'SET_DEFAULT_QUERY';
export const SET_DEFAULT_DATA = 'SET_DEFAULT_DATA';

export const VALIDATE_DATA = 'VALIDATE_DATA';
export const VALIDATE_DATA_SUCCESS = 'VALIDATE_DATA_SUCCESS';
export const VALIDATE_DATA_FAILURE = 'VALIDATE_DATA_FAILURE';

export const SAVE = 'SAVE';
export const SAVE_SUCCESS = 'SAVE_SUCCESS';
export const SAVE_FAILURE = 'SAVE_FAILURE';

export const CONFIRM = 'CONFIRM';
export const CONFIRMED = 'CONFIRMED';

export const DELETE = 'DELETE';
export const DELETE_SUCCESS = 'DELETE_SUCCESS';
export const DELETE_FAILURE = 'DELETE_FAILURE';

export const UPLOAD_FILE = 'UPLOAD_FILE';
export const UPLOAD_FILE_SUCCESS = 'UPLOAD_FILE_SUCCESS';
export const UPLOAD_FILE_FAILURE = 'UPLOAD_FILE_FAILURE';

export const DELETE_FILE = 'DELETE_FILE';
export const DELETE_FILE_SUCCESS = 'DELETE_FILE_SUCCESS';
export const DELETE_FILE_FAILURE = 'DELETE_FILE_FAILURE';

export const ADD_PERMISSION = 'ADD_PERMISSION';
export const SELECT_PERMISSION = 'SELECT_PERMISSION';
export const CHANGE_USER_PERMISSION = 'CHANGE_USER_PERMISSION';
export const UPDATE_PERMISSION = 'UPDATE_PERMISSION';
export const SKIP_UPDATE_PERMISSION = 'SKIP_UPDATE_PERMISSION';
export const DELETE_PERMISSION = 'DELETE_PERMISSION';

export const ACTION_STATES = [
  LOAD_LIST_PAGE, LOAD_LIST_PAGE_SUCCESS, LOAD_LIST_PAGE_FAILURE,
  LOAD_FORM_PAGE, LOAD_FORM_PAGE_SUCCESS, LOAD_FORM_PAGE_FAILURE,
  SET_DEFAULT_QUERY, SET_DEFAULT_DATA,
  GET_FULL_LIST, GET_FULL_LIST_SUCCESS, GET_FULL_LIST_FAILURE,
  GET_LIST, GET_LIST_SUCCESS, GET_LIST_FAILURE,
  GET_DATA, GET_DATA_SUCCESS, GET_DATA_FAILURE,
  CHANGE_QUERY, CHANGE_DATA,
  VALIDATE_DATA, VALIDATE_DATA_SUCCESS, VALIDATE_DATA_FAILURE,
  SAVE, SAVE_SUCCESS, SAVE_FAILURE,
  CONFIRM, CONFIRMED,
  DELETE, DELETE_SUCCESS, DELETE_FAILURE,
  UPLOAD_FILE, UPLOAD_FILE_SUCCESS, UPLOAD_FILE_FAILURE,
  DELETE_FILE, DELETE_FILE_SUCCESS, DELETE_FILE_FAILURE,

  ADD_PERMISSION, SELECT_PERMISSION, CHANGE_USER_PERMISSION, UPDATE_PERMISSION, SKIP_UPDATE_PERMISSION, DELETE_PERMISSION,
];
