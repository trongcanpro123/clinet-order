export const EDIT_MODE = 'EDIT_MODE';
export const VIEW_MODE = 'VIEW_MODE';

export const ACTIVE_STATUS_SEARCHING_OPTIONS = [
  { text: 'Có hiệu lực', value: 'true' },
  { text: 'Không có hiệu lực', value: 'false' },
  { text: 'Tất cả', value: '' },
];

export const HOW_TO_SIGN = [
  { text: 'ký bản cứng', value: 'ký bản cứng' },
  { text: 'e-sign', value: 'e-sign' },
  { text: '', value: '' },
];

export const LETER_OF_CREDIT = [
  { text: 'Có', value: 'Yes' },
  { text: 'Không', value: 'No' },
  { text: '', value: '' },
];

export const EXTENDED_PERIOD = [
  { text: '1', value: '1' },
  { text: '3', value: '3' },
  { text: '6', value: '6' },
  { text: '9', value: '9' },
  { text: '12', value: '12' },
  { text: '24', value: '24' },
  { text: '36', value: '36' },
];

export const COMPANY_NAME_OPTIONS = [
  { text: 'SFDC HN', value: 'SFDC HN' },
  { text: 'SFDC HCM', value: 'SFDC HCM' },
  { text: 'FTP HN', value: 'FTP HN' },
  { text: 'FTP HCM', value: 'FTP HCM' },
  { text: 'Tất cả', value: 'Tất cả' },
];

export const ORDER_INDICATORS_PS = [
  { text: 'Chọn giá trị', value: '' },
  { text: 'Inventory Value', value: 'Inventory Value' },
  { text: 'Age Inventory', value: 'Age Inventory' },
  { text: 'Sell-through', value: 'Sell-through' },
  { text: 'WOI', value: 'WOI' }
];

const yearContractGetData = () => {
  const optionList = [];
  optionList.push({
    text: 'Chọn tất cả',
    value: null,
  });
  for (let i = 2011; i < 2030; i += 1) {
    const obj = {
      text: i,
      value: i,
    };
    optionList.push(obj);
  }
  return optionList;
};
export const yearExpiredGetData = () => {
  const optionList = [];
  optionList.push({
    text: 'Chọn tất cả',
    value: null,
  });
  for (let i = 2011; i < 2030; i += 1) {
    const obj = {
      text: i,
      value: i,
    };
    optionList.push(obj);
  }
  return optionList;
};

export const yearContract = yearContractGetData();
export const yearExpired = yearExpiredGetData();

export const CERTIFICATE_OF_FPT_WITH_VENDOR = [
  { text: 'Chứng nhận membership', value: 'Chứng nhận membership' },
  { text: 'Chứng nhận competency', value: 'Chứng nhận competency' },
  { text: 'Khác', value: 'Khác' },
];

export const VOUCHER_TYPE_OPTION = [
  { text: 'Báo cáo tài chính', value: 'Báo cáo tài chính' },
  { text: 'Chứng từ kế toán', value: 'Chứng từ kế toán' },
  { text: 'Chứng từ khác', value: 'Chứng từ khác' },
];

export const ACTIVE_STATUS_BROWSER_OPTIONS = [
  { text: 'Dự thảo', value: 'draft' },
  { text: 'Chờ BD xem xét', value: 'penddingBD' },
  { text: 'Chờ FAF xem xét', value: 'penddingFAF' },
  { text: 'Chờ FBP xem xét', value: 'penddingFBP' },
  { text: 'Chờ BOD phê duyệt', value: 'penddingBOD' },
  { text: 'Tất cả', value: '' },
];

export const CLASSIFY = [
  { text: 'Người đại diện', value: 'Người đại diện' },
  { text: 'Người liên hệ', value: 'Người liên hệ' },
  { text: 'Người giới thiệu', value: 'Người giới thiệu' },
  { text: 'Khác', value: 'Khác' },
];

export const EVALUATE_VENDOR_OPTION = [
  { text: 'Đánh giá nhà cung cấp', value: 'Đánh giá nhà cung cấp' },
];

export const PARTICIPANTS_TYPE_OPTION = [
  { text: 'Chọn giá trị', value: '' },
  { text: 'Enduser', value: 'Enduser' },
  { text: 'Reseller', value: 'Reseller' },
  { text: 'Distributor', value: 'Distributor' },
  { text: 'Other', value: 'Other' },
];

export const FUNDING_SOURCES = [
  { text: 'Chọn giá trị', value: '' },
  { text: 'SFDC ', value: 'SFDC ' },
  { text: 'Hãng hỗ trợ', value: 'VMDF' },
  { text: 'Co-fund', value: 'VJFD' },
  { text: 'Co-op', value: 'COOP' }
];

export const PURPOSE_FUND = [
  { key: 1, text: 'Chọn giá trị', value: ''},
  { key: 2, text: 'Giữ làm lợi nhuận', value: 'Giữ làm lợi nhuận' },
  { key: 3, text: 'CHH', value: 'CHH' },
  { key: 4, text: 'KM', value: 'KM' },
  { key: 5, text: 'PR', value: 'PR' },
  { key: 6, text: 'Event', value: 'Event' },
  { key: 7, text: 'Other', value: 'Other' },
];

export const REQUIRE_DOCUMENT = [
  { text: 'Chọn giá trị', value: '' },
  { text: 'Hóa đơn đầu ra', value: 'Hóa đơn đầu ra' },
  { text: 'Hóa đơn đầu vào', value: 'Hóa đơn đầu vào' },
  { text: 'IMEI', value: 'IMEI' },
  { text: 'Hình ảnh', value: 'Hình ảnh' }
];

// export const REQUIRE_DOCUMENT = [
//   { text: 'Chọn giá trị', value: '' },
//   { text: 'Hóa đơn đầu ra', value: 'Hóa đơn đầu ra' },
//   { text: 'Hóa đơn đầu vào', value: 'Hóa đơn đầu vào' },
//   { text: 'IMEI', value: 'IMEI' },
//   { text: 'Hình ảnh', value: 'Hình ảnh' }
// ];
export const FUNDING_TYPE = [
  { text: 'Chọn giá trị', value: '' },
  { text: 'Sell in rebate', value: 'SI-Sell in rebate' },
  { text: 'Sell thru rebate', value: 'ST-Sell thru rebate' },
  { text: 'Inventory Protection', value: 'IP-Inventory Protection' },
  { text: 'Marketing fund', value: 'MDF-Marketing fund' },
  { text: 'Payment discount', value: 'EPD-Payment discount' },
  { text: 'Reseller rebate', value: 'RR-Reseller rebate' },
  { text: 'RMA', value: 'RMA' },
  { text: 'Other support', value: 'OTHR-Other support' },
  { text: 'Quarterly rebate', value: 'QTR-Quarterly rebate' },
  { text: 'Half-year rebate', value: 'HYR-Half-year rebate' },

];

export const PAYMENT_METHOD = [
  { text: 'Chọn giá trị', value: '' },
  { text: 'Credit note', value: 'Credit note' },
  { text: 'Tiền mặt/CK', value: 'Tiền mặt/CK' },
  { text: 'Voucher', value: 'Voucher' },
  { text: 'Giảm trừ đơn hàng', value: 'Giảm trừ đơn hàng' },
  { text: 'Hãng xuất hóa đơn', value: 'Hãng xuất hóa đơn' },
  { text: 'SFDC xuất hóa đơn', value: 'SFDC xuất hóa đơn' },
  { text: 'Hàng khuyến mãi', value: 'Hàng khuyến mãi' }
];

export const expiresList = [
  { key: 'Week', value: 7, text: '1 tuần' },
  { key: 'Month', value: 30, text: '1 tháng' },
  { key: 'precious', value: 90, text: '1 quý' },
  { key: 'Year', value: 365, text: '1 năm' },
  { key: '', text: 'Tất cả' },
];



