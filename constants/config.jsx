export const APP_NAME = 'Synnex FPT';
export const APP_CODE = 'order';
export const API_GATEWAY_URL = 'http://dev-api.ftg.vn';
// export const API_GATEWAY_URL = 'https://api.ftg.vn';

export const ITEM_AMOUNT_PER_PAGE_VALUES = [5, 15, 30, 50, 100, 200];
export const ITEM_AMOUNT_PER_PAGE = 15;
export const PAGE_RANGE_DISPLAYED = 5;
export const SEPARATE_STRING = ' - ';
export const UNLIMITED_RETURNED_RESULT = 9999;

export const DATE_FORMAT = 'DD/MM/YYYY';
export const DATETIME_FORMAT = 'DD/MM/YYYY HH:mm';
export const TIME_FORMAT = 'HH:mm';

export const UPLOAD_FILE_MAX_SIZE = 41900000;
export const UPLOAD_FILE_EXTENSION = '.png, .jpg, .doc, .xls, .docx, .xlsx, .pdf, .zip, .rar, .7zip, .msg, .csv, .gz';

export const PRIMARY_COLOR = 'blue';
export const SECONDARY_COLOR = 'red';
export const THIRD_COLOR = 'black';

export const VALIDATE_FAILURE = 'msg.validate.failure';
export const TINY_MCE_CONFIG = {
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed mentions linkchecker',
  toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment',
  height: 480,
  min_height: 300,
};
