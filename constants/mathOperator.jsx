import _ from 'lodash';

const EQ = '$eq';
const NEQ = '$neq';

const GT = '$gt';
const GTE = '$gte';

const LT = '$lt';
const LTE = '$lte';

const IN = '$in';
const NOT_IN = '$nin';
const EXISTS = '$exists';

// const EXITS_INTERSECTION = '$exits.intersection';
// const NOT_EXITS_INTERSECTION = '$not.exits.intersection';

export const SET_OPERATOR = [IN, NOT_IN, EXISTS];

export const SET_OPERATOR_LIST = [
  { key: IN, value: IN, text: IN },
  { key: NOT_IN, value: NOT_IN, text: NOT_IN },
  { key: EXISTS, value: EXISTS, text: EXISTS },

  // { key: EXITS_INTERSECTION, value: EXITS_INTERSECTION, text: EXITS_INTERSECTION },
  // { key: NOT_EXITS_INTERSECTION, value: NOT_EXITS_INTERSECTION, text: NOT_EXITS_INTERSECTION },
];

export const LOGIC_OPERATOR = [EQ, NEQ, GT, GTE, LT, LTE];

export const LOGIC_OPERATOR_LIST = [
  { key: EQ, value: EQ, text: EQ },
  { key: NEQ, value: NEQ, text: NEQ },

  { key: GT, value: GT, text: GT },
  { key: GTE, value: GTE, text: GTE },

  { key: LT, value: LT, text: LT },
  { key: LTE, value: LTE, text: LTE },
];

export const OPERATOR = {};

OPERATOR.EQ = EQ;
OPERATOR.NEQ = NEQ;

OPERATOR.GT = GT;
OPERATOR.GTE = GTE;

OPERATOR.LT = LT;
OPERATOR.LTE = LTE;

OPERATOR.IN = IN;
OPERATOR.NOT_IN = NOT_IN;
OPERATOR.EXISTS = EXISTS;

// OPERATOR.EXITS_INTERSECTION = EXITS_INTERSECTION;
// OPERATOR.NOT_EXITS_INTERSECTION = NOT_EXITS_INTERSECTION;

export const OPERATOR_LIST = _.union(LOGIC_OPERATOR_LIST, SET_OPERATOR_LIST);
