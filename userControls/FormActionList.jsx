/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Confirm } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { bindComponentToContext } from '../libs/componentHelper';
import FormActionContainer from './FormActionContainer';
import FormActionCreate from './FormActionCreate';
import FormActionUpdate from './FormActionUpdate';
import FormActionDelete from './FormActionDelete';
import FormActionGoBack from './FormActionGoBack';

const ThisContext = React.createContext({});

class FormActionList extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      state,
      onDeleteCancle, onDeleteConfirm,
    } = self;

    const { deleting } = state;

    bindComponentToContext([FormActionCreate, FormActionUpdate, FormActionDelete, FormActionGoBack], ThisContext);

    return (
      <ThisContext.Provider value={{ self }}>
        <Translation ns="system">
          {
            t => (
              <React.Fragment>
                <FormActionContainer>
                  <FormActionCreate />
                  <FormActionUpdate />
                  <FormActionDelete />
                  <FormActionGoBack />
                </FormActionContainer>

                <Confirm open={deleting} onCancel={onDeleteCancle} onConfirm={onDeleteConfirm} header="Thông báo" content={t('msg.delete.confirmMessage')} />
              </React.Fragment>)
          }
        </Translation>
      </ThisContext.Provider>);
  }
}

export default FormActionList;
