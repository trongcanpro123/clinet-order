/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Accordion, Icon } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { PropsChildrenPropType } from '../libs/componentHelper';

export default class ListAdvancedQuery extends Component {
  static get propTypes() {
    return {
      children: PropsChildrenPropType.isRequired,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { state, onClickAdvancedSearch } = self;
    const { showAdvancedSearch } = state;
    const advancedSearchTitle = 'system:tab.advancedSearch';

    return (
      <Translation>
        {
          (t, { i18n }) => (
            <Accordion>
              <Accordion.Title active={showAdvancedSearch} index={0} onClick={onClickAdvancedSearch}>
                <Icon name="dropdown" />
                {i18n.exists(advancedSearchTitle) ? t(advancedSearchTitle) : advancedSearchTitle}
              </Accordion.Title>

              <Accordion.Content active={showAdvancedSearch}>
                { this.props.children }
              </Accordion.Content>
            </Accordion>)
        }
      </Translation>);
  }
}
