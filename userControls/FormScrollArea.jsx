/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Translation } from 'react-i18next';

export default class FormScrollArea extends Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      children: PropTypes.object.isRequired,
    };
  }

  render() {
    const { name, children } = this.props;

    if (!name) {
      return (
        <React.Fragment>
          <div className="gridBody">
            {children}
          </div>
        </React.Fragment>);
    }

    return (
      <Translation>
        {
        (t, { i18n }) => (
          <React.Fragment>
            <div className="gridTitle">
              {i18n.exists(name) ? t(name) : name}
            </div>

            <div className="gridBody">
              {children}
            </div>
          </React.Fragment>)
      }
      </Translation>);
  }
}

