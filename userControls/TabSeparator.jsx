import React from 'react';

// @desc: make distance between tab and buton list (semantic react ui bug)
const TabSeparator = () => (<div style={{ clear: 'both', height: 20 }} />);

export default TabSeparator;
