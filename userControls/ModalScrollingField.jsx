/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Button, Header, Icon, Image, Modal } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import renderHTML from 'react-render-html';

import { getFieldAttribute } from '../libs/commonHelper';

class ModalScrollingField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      type: PropTypes.string,
      placeholder: PropTypes.string,
      readOnly: PropTypes.bool,
      label: PropTypes.bool,
      onChange: PropTypes.func,
    };
  }

  static get defaultProps() {
    return {
      type: 'text',
      placeholder: '',
      readOnly: false,
      label: true,
      onChange: undefined,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      name, type,
      placeholder, readOnly,
      label,
    } = this.props;

    const userDefinedOnChange = this.props.onChange;
    const { state, onChange } = self;
    const { error, messages } = state;
    const { fieldValue } = getFieldAttribute(self, name);

    return (
      <Translation>
        {
          (t, { i18n }) => (<Modal size='fullscreen' trigger={<Button>Chi tiết</Button>}>
            <Modal.Header>Nội dung chi tiết</Modal.Header>
            <Modal.Content image scrolling>
              <Modal.Description>
                <div>{renderHTML(fieldValue)}</div>
              </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
              {/* <Button primary>
                Ok <Icon name='right chevron' />
              </Button> */}
            </Modal.Actions>
          </Modal>)
        }
      </Translation>);
  }
}

export default ModalScrollingField;
