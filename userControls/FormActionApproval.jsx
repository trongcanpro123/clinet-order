/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Button } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { PRIMARY_COLOR } from '../constants/config';

export default class FormActionApproval extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { state, onApprove } = self;

    const { loading } = state;
    const { permmission, id } = self.props;
    if (!permmission.canSend || id === '0') return (<React.Fragment />);
    return (
      <Translation ns="system">
        {
          t => (<Button type="button" loading={loading} color={PRIMARY_COLOR} onClick={onApprove}>{t('btn.approval')}</Button>)
        }
      </Translation>);
  }
}

