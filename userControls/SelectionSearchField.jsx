/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Select } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import _ from 'lodash';

import { fieldErrorSelector } from '../libs/errorHelper';
import { getFieldAttribute } from '../libs/commonHelper';

class SelectionSearchField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      options: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string,
        value: PropTypes.string,
        text: PropTypes.string,
      })),
      search: PropTypes.bool,
      multiple: PropTypes.bool,
      label: PropTypes.bool,
      onChange: PropTypes.func,
      onSearchChange: PropTypes.func,
      disabled: PropTypes.bool,
      value: PropTypes.string,
    };
  }

  static get defaultProps() {
    return {
      options: undefined,
      search: true,
      multiple: false,
      label: true,
      onChange: undefined,
      onSearchChange: undefined,
      disabled: false,
      value: undefined,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      name, options,
      search, multiple,
      label, disabled,
      value,
    } = this.props;
    const userDefinedOnChange = this.props.onChange;

    const { state, onChange, onSearchChange } = self;
    const { error, messages } = state;
    const { fieldType, fieldValue } = getFieldAttribute(self, name);

    if (!fieldType) {
      return (<React.Fragment />);
    }

    // TODO: check case both of  options && fieldType.options is undefined

    return (
      <Translation>
        {
          (t, { i18n }) => (<Form.Field
            name={name}
            value={value || fieldValue}
            required={fieldType.required || false}
            onChange={userDefinedOnChange || onChange}
            label={label && (i18n.exists(name) ? t(name) : name)}
            options={_.isArray(options) ? options : fieldType.options}
            error={error ? fieldErrorSelector(name, messages) : false}
            search={search}
            multiple={multiple}
            disabled={disabled}
            onSearchChange={onSearchChange}
            control={Select}
            fluid
            selection
          />)
        }
      </Translation>);
  }
}

export default SelectionSearchField;

