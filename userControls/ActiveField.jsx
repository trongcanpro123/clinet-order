/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';
import PropTypes from 'prop-types';

class ActiveField extends Component {
  static get propTypes() {
    return {
      active: PropTypes.bool.isRequired,
    };
  }

  render() {
    const { active } = this.props;

    if (active) {
      return (<Icon name="check circle outline" />);
    }

    return (<Icon name="circle outline" />);
  }
}

export default ActiveField;
