/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Form } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import { Editor } from '@tinymce/tinymce-react';
import renderHTML from 'react-render-html';

import { getFieldAttribute } from '../libs/commonHelper';
import { TINY_MCE_API_KEY, TINY_MCE_CONFIG } from '../constants/config';

class HtmlField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      readOnly: PropTypes.bool,
      label: PropTypes.bool,
      onChange: PropTypes.func,
    };
  }

  static get defaultProps() {
    return {
      readOnly: false,
      label: true,
      onChange: undefined,
    };
  }

  constructor(props) {
    super(props);

    this.onValueChange = this.onValueChange.bind(this);
  }

  onValueChange(value) {
    const { self } = this.context;
    const { name } = this.props;
    const { onChange } = self;
    const userDefinedOnChange = this.props.onChange;

    const data = { type: 'input.wusiwug', name, value };
    const event = { preventDefault: () => {} };

    if (userDefinedOnChange) {
      userDefinedOnChange(event, data);
    } else {
      onChange(event, data);
    }
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { name, readOnly, label } = this.props;

    const { fieldType, fieldValue } = getFieldAttribute(self, name);
    const required = (fieldType && fieldType.required) ? fieldType.required : false;

    if (readOnly) {
      return (
        <Translation>
          {
            (t, { i18n }) => (
              <Form.Field>
                {
                  label && (
                    <label>
                      {(i18n.exists(name) ? t(name) : name)} {required && (<span style={{ color: 'red' }}>* </span>)}
                    </label>
                  )
                }

                <div className="gridBody" style={{ padding: '4px 10px', height: 300 }}>
                  { renderHTML(fieldValue) }
                </div>
              </Form.Field>)
          }
        </Translation>);
    }

    return (
      <Translation>
        {
          (t, { i18n }) => (
            <Form.Field>
              {
                label && (
                  <label>
                    {(i18n.exists(name) ? t(name) : name)} {required && (<span style={{ color: 'red' }}>* </span>)}
                  </label>
                )
              }

              <Editor value={fieldValue} onEditorChange={this.onValueChange} apiKey={TINY_MCE_API_KEY} init={TINY_MCE_CONFIG} />
            </Form.Field>)
        }
      </Translation>);
  }
}

export default HtmlField;
