/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Button } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { THIRD_COLOR } from '../constants/config';

export default class FormActionGoBack extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { state, onGoBack } = self;

    const { loading } = state;

    return (
      <Translation ns="system">
        {
          t => (<Button type="button" loading={loading} color={THIRD_COLOR} onClick={onGoBack}>{t('btn.goBack')}</Button>)
        }
      </Translation>);
  }
}

