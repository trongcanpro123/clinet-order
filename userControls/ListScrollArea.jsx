/* eslint-disable react/prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';

export default class ListScrollArea extends Component {
  render() {
    return (
      <div className="search-result">
        {this.props.children}
      </div>
    );
  }
}