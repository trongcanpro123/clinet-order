/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';

import { PRIMARY_COLOR } from '../constants/config';

class ButtonDeleteLine extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      onClick: PropTypes.func,
      disabled: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      onClick: undefined,
      disabled: false,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { name, onClick, disabled } = this.props;
    const { state, onDeleteSubDocument } = self;

    return (
      <Button
        onClick={onClick || (e => onDeleteSubDocument(e, name))}
        disabled={disabled}
        loading={state.loading}

        color={PRIMARY_COLOR}
        size="mini"
        type="button"
        icon="delete"
      />);
  }
}

export default ButtonDeleteLine;
