/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Form, TextArea, Input } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { fieldErrorSelector } from '../libs/errorHelper';

class TextAreaField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      placeholder: PropTypes.string,
      onChange: PropTypes.func,
      readOnly: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      placeholder: '',
      onChange: undefined,
      readOnly: false,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { name, placeholder, readOnly } = this.props;

    const userDefinedOnChange = this.props.onChange;
    const { state, onChange } = self;

    const {
      model, object, query,
      error, messages,
    } = state;

    const fieldType = model[name];

    if (readOnly) {
      return (
        <Translation>
          {
            (t, { i18n }) => (<Form.Field
              name={name}
              value={query ? query[name] : object[name]}
              label={i18n.exists(name) ? t(name) : name}

              control={Input}
              readOnly
              className="readonly"
            />)
          }
        </Translation>);
    }

    return (
      <Translation>
        {
          (t, { i18n }) => (<Form.Field
            name={name}
            value={query ? query[name] : object[name]}
            onChange={userDefinedOnChange || onChange}
            label={i18n.exists(name) ? t(name) : name}
            required={fieldType.required ? fieldType.required : false}
            error={error ? fieldErrorSelector(name, messages) : false}
            placeholder={placeholder}

            control={TextArea}
          />)
        }
      </Translation>);
  }
}

export default TextAreaField;
