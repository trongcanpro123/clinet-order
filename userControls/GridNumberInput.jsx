import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'semantic-ui-react';
import NumberFormat from 'react-number-format';

export default class GridNumberInput extends Component {
  static get propTypes() {
    return {
      name: PropTypes.string,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      label: PropTypes.string,
      placeholder: PropTypes.string,
      error: PropTypes.bool,
      required: PropTypes.bool,

      onChange: PropTypes.func.isRequired,
    };
  }

  static get defaultProps() {
    return {
      name: '',
      value: '',
      label: '',
      placeholder: '',
      error: false,
      required: false,
    };
  }

  constructor(props) {
    super(props);

    this.onValueChange = this.onValueChange.bind(this);
  }

  onValueChange(values) {
    const { value } = values;
    const { name, onChange } = this.props;

    const data = {
      type: 'input.gridNumberInput',
      name,
      value,
    };

    const event = {};
    event.preventDefault = (() => {});

    onChange(event, data);
  }

  render() {
    const {
      name, value,
      placeholder, label,
      required, error,
    } = this.props;

    if (required) {
      return (
        <Form.Field error={error}>
          <label>
            {label} <span style={required ? { color: 'red' } : {}}>* </span>
          </label>
          <NumberFormat name={name} value={value} customInput={Input} placeholder={placeholder} onValueChange={this.onValueChange} thousandSeparator="." decimalSeparator="," />
        </Form.Field>
      );
    }

    return (
      <Form.Field>
        <label>{label}</label>
        <NumberFormat name={name} value={value} customInput={Input} placeholder={placeholder} onValueChange={this.onValueChange} thousandSeparator="." decimalSeparator="," />
      </Form.Field>
    );
  }
}
