import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Menu, Dropdown } from 'semantic-ui-react';

import { ITEM_AMOUNT_PER_PAGE, PAGE_RANGE_DISPLAYED } from '../constants/config';

const PageRender = ({
  page, active, disabled,
  onPageChange,
}) => (
  <Menu.Item as="a" active={active} disabled={disabled} onClick={onPageChange} size="tiny">{page}</Menu.Item>
);

PageRender.propTypes = {
  page: PropTypes.number,
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  onPageChange: PropTypes.func,
};

PageRender.defaultProps = {
  page: 1,
  active: true,
  disabled: false,
  onPageChange: () => {},
};

const ITEM_AMOUNT_PER_PAGE_OPTIONS = [
  { key: 5, text: '05 (bản ghi/trang)', value: 5 }, // TODO: fix language hardcode
  { key: 15, text: '15 (bản ghi/trang)', value: 15 },
  { key: 30, text: '30 (bản ghi/trang)', value: 30 },
  { key: 50, text: '50 (bản ghi/trang)', value: 50 },
  { key: 100, text: '100 (bản ghi/trang)', value: 100 },
  { key: 200, text: '200 (bản ghi/trang)', value: 200 },
];

export class PageNavigator extends Component {
  static get propTypes() {
    return {
      itemsPerPage: PropTypes.number,
      totalAmount: PropTypes.number,
      activePage: PropTypes.number,
      onPageChange: PropTypes.func,
      onItemsPerPageChange: PropTypes.func,
    };
  }

  static get defaultProps() {
    return {
      itemsPerPage: ITEM_AMOUNT_PER_PAGE,
      activePage: 1,
      totalAmount: 0,
      onPageChange: () => {},
      onItemsPerPageChange: () => {},
    };
  }

  constructor(props) {
    super(props);

    this.onItemsPerPageChange = this.props.onItemsPerPageChange.bind(this);
  }

  render() {
    const {
      itemsPerPage, totalAmount, activePage, onPageChange,
    } = this.props;

    let pageAmount = 0;
    const remainAmount = totalAmount % itemsPerPage;

    if (remainAmount > 0) {
      pageAmount = ((totalAmount - remainAmount) / itemsPerPage) + 1;
    } else {
      pageAmount = (totalAmount / itemsPerPage);
    }

    const HALF_PAGE_RANGE_DISPLAYED = (PAGE_RANGE_DISPLAYED - (PAGE_RANGE_DISPLAYED % 2)) / 2;
    let minPage = (activePage - HALF_PAGE_RANGE_DISPLAYED - 1);
    let maxPage = (activePage + HALF_PAGE_RANGE_DISPLAYED + 1);

    minPage = minPage > 1 ? minPage : 1;
    maxPage = maxPage < pageAmount ? maxPage : pageAmount;

    const pageList = [];

    for (let i = minPage; i <= maxPage; i += 1) {
      pageList.push(i);
    }

    // console.log(pageList);

    return (
      <React.Fragment>
        <Menu floated="right" pagination size="mini">
          { minPage > 1 ? <PageRender key={1} active={false} page={1} onPageChange={onPageChange} /> : '' }

          {pageList.map(page =>
                (<PageRender
                  key={page}
                  active={page === activePage}
                  disabled={!!((page < (activePage - HALF_PAGE_RANGE_DISPLAYED)) || (page > (activePage + HALF_PAGE_RANGE_DISPLAYED)))}
                  page={(page < (activePage - HALF_PAGE_RANGE_DISPLAYED)) || (page > (activePage + HALF_PAGE_RANGE_DISPLAYED)) ? '...' : page}
                  onPageChange={onPageChange}
                />))}

          {totalAmount === 0 ? <PageRender key={0} active={false} page={0} /> : ''}
          { maxPage < pageAmount ? <PageRender key={pageAmount} active={false} page={pageAmount} onPageChange={onPageChange} /> : '' }
          <Dropdown value={itemsPerPage} options={ITEM_AMOUNT_PER_PAGE_OPTIONS} simple item onChange={this.onItemsPerPageChange} />
        </Menu>
      </React.Fragment>
    );
  } // render()
}

export default PageNavigator;
