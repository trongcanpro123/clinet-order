import React, { Component } from 'react';

export default class ScrollArea extends Component {
  render() {
    return (
      <div className="search-result">
        {this.props.children}
      </div>
    );
  }
}
