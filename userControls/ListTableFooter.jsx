/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { PropsChildrenPropType } from '../libs/componentHelper';

class ListTableFooter extends Component {
  static get propTypes() {
    return {
      colSpan: PropTypes.string,
      children: PropsChildrenPropType.isRequired,
    };
  }

  static get defaultProps() {
    return {
      colSpan: '1',
    };
  }

  render() {
    const { colSpan, children } = this.props;
    return (
      <Table.Footer>
        <Table.Row>
          <Table.HeaderCell colSpan={colSpan} >
            { children }
          </Table.HeaderCell>
        </Table.Row>
      </Table.Footer>
    );
  }
}

export default ListTableFooter;
