/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Button } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { PRIMARY_COLOR } from '../constants/config';

export default class FormActionCreate extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { state, onCreate } = self;

    const { loading } = state;
    const { permmission, id } = self.props;

    if (!permmission.canCreate || id !== '0') return (<React.Fragment />);

    return (
      <Translation ns="system">
        {
          t => (<Button type="button" loading={loading} color={PRIMARY_COLOR} onClick={onCreate}>{t('btn.create')}</Button>)
        }
      </Translation>);
  }
}

