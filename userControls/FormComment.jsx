/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Button, Message, Form, Container, List, Label, TextArea } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import date from 'date-and-time';
import createCachedSelector from 're-reselect';
import _ from 'lodash';

import { PRIMARY_COLOR, THIRD_COLOR, SECONDARY_COLOR } from '../constants/config';
import { SESSION__USER_NAME } from '../libs/constants/sessionKeys';
import FormBody from './FormBody';
import FormRow from './FormRow';

const commentListRenderSelector = createCachedSelector(
  self => self,
  (self, commentList) => (commentList ? commentList.data : []),

  (self, commentList) => {
    const currentUserName = localStorage.getItem(SESSION__USER_NAME);

    if (_.isArray(commentList)) {
      return (
        <List selection>
          {
            commentList.map(comment => (
              <List.Item key={`comment.${comment._id}`}>
                <Label
                  color={comment.createdByUserName === currentUserName ? PRIMARY_COLOR : SECONDARY_COLOR}
                  horizontal
                  style={{ marginRight: 6 }}
                >
                  {`${comment.createdByUserName}#${date.format(new Date(comment.createdAt), 'DD/MM/YYYY HH:mm')}`}
                </Label>

                {comment.content}
              </List.Item>
            ))
          }
        </List>
      );
    }

    return (<React.Fragment />);
  },
)((self, commentList, cacheName) => cacheName);

export default class FormComment extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { state, onChangeComment, onSendComment } = self;

    const {
      loading, comment,
      object, pageLoad,
    } = state;

    if (object._id === '0' || object._id === '') {
      return (<React.Fragment />);
    }

    const cannotSend = comment.length === 0;

    return (
      <Translation ns="system">
        {
          t => (
            <React.Fragment>
              <Message attached header={t('form.commentList')} />

              <FormBody>
                <FormRow>
                  <Form.Field name="comment" value={comment} control={TextArea} onChange={onChangeComment} label={t('form.comment')} />
                </FormRow>

                <Container textAlign="center">
                  <Button
                    onClick={onSendComment}

                    type="button"
                    loading={loading}
                    color={cannotSend ? THIRD_COLOR : PRIMARY_COLOR}
                    disabled={cannotSend}
                    content={t('btn.send')}
                  />
                </Container>

                { commentListRenderSelector(self, pageLoad.commentList, 'commentList') }
              </FormBody>

              <Message attached="bottom" />
            </React.Fragment>
          )
        }
      </Translation>
    );
  }
}
