/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Button } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { PRIMARY_COLOR } from '../constants/config';

export default class ListActionExport extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { state, onExport } = self;

    const { loading } = state;
    const { permmission } = self.props;

    if (!permmission.canExport) return (<React.Fragment />);

    return (
      <Translation ns="system">
        {
          t => (<Button type="button" loading={loading} color={PRIMARY_COLOR} onClick={onExport}>{t('btn.export')}</Button>)
        }
      </Translation>);
  }
}
