/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Message } from 'semantic-ui-react';
import _ from 'lodash';
import { Trans } from 'react-i18next';

class Footer extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      error, warning, success,
      messages,
    } = self.state;

    if (_.isString(messages)) {
      if (messages.startsWith('system:msg.')) { // system message
        return (
          <Message attached="bottom" error={error} warning={warning} success={success} >
            <Message.Content>
              <Trans i18nKey={messages} />
            </Message.Content>
          </Message>);
      }

      return (
        <Message attached="bottom" error={error} warning={warning} success={success} >
          <Message.Content>
            {messages}
          </Message.Content>
        </Message>);
    } else if (_.isArray(messages)) {
      return (
        <Message attached="bottom" error={error} warning={warning} success={success}>
          <Message.Content>
            {
              messages.map((msg, index) => (
                <React.Fragment key={`${msg.name}.${index}`}>
                  <Trans i18nKey={`common:${msg.name}`} />: <Trans i18nKey={`${msg.message}`} />
                  <br />
                </React.Fragment>
              ))
            }
          </Message.Content>
        </Message>
      );
    }

    return (
      <Message attached="bottom" error={error} warning={warning} success={success} >
        <Message.Content>
          {JSON.stringify(messages)}
        </Message.Content>
      </Message>);
  }
}

export default Footer;
