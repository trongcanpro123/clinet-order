import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';

class NumberValue extends Component {
  static get propTypes() {
    return {
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    };
  }

  static get defaultProps() {
    return {
      value: '',
    };
  }

  render() {
    let { value } = this.props;

    value = (value !== null) ? value : '';

    return <NumberFormat value={value} displayType="text" thousandSeparator="." decimalSeparator="," />;
  }
}

export default NumberValue;
