/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Message, Grid, Button, Icon } from 'semantic-ui-react';

import { PRIMARY_COLOR } from '../constants/config';

class ListTitle extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      state,
      onClickFirstObject, onClickFunctionRegister,
    } = self;

    const { functionName, isAdmin } = self.props;
    const { loading, objectList } = state;
    const isEnabled = objectList && objectList.data && objectList.data.length > 0;

    return (
      <Message attached>
        <Message.Content>
          <Message.Header>
            <Grid columns={2}>
              <Grid.Row>
                <Grid.Column>{functionName}</Grid.Column>
                <Grid.Column textAlign="right">
                  { isAdmin && (
                    <Button icon name="configure" color={PRIMARY_COLOR} size="mini" onClick={onClickFunctionRegister} loading={loading} >
                      <Icon name="configure" />
                    </Button>
                  )}

                  <Button icon name="list" color={PRIMARY_COLOR} size="mini" disabled loading={loading}>
                    <Icon name="list" />
                  </Button>

                  <Button icon name="form" color={PRIMARY_COLOR} size="mini" disabled={!isEnabled} onClick={onClickFirstObject} loading={loading} >
                    <Icon name="file outline" />
                  </Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Message.Header>
        </Message.Content>
      </Message>
    );
  }
}

export default ListTitle;

