/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Container } from 'semantic-ui-react';
import { PropsChildrenPropType } from '../libs/componentHelper';

export default class ListActionList extends React.Component {
  static get propTypes() {
    return {
      children: PropsChildrenPropType.isRequired,
    };
  }

  render() {
    return (
      <Container textAlign="center">
        { this.props.children }
      </Container>);
  }
}

