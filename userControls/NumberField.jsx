/* eslint-disable react/forbid-prop-types */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';
import { Form, Input } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { fieldErrorSelector } from '../libs/errorHelper';
import { getFieldAttribute } from '../libs/commonHelper';
import RequiredSign from './RequiredSign';

export default class NumberField extends Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      placeholder: PropTypes.string,
      label: PropTypes.bool,
      readOnly: PropTypes.bool,
      style: PropTypes.object,
    };
  }

  static get defaultProps() {
    return {
      placeholder: '',
      label: true,
      readOnly: false,

      style: {
        textAlign: 'right',
        width: 150,
      },
    };
  }

  constructor(props) {
    super(props);

    this.onValueChange = this.onValueChange.bind(this);
  }

  onValueChange(values) {
    const { value } = values;
    const { name } = this.props;
    const { self } = this.context;
    const { onChange } = self;

    const data = { type: 'input.numberField', name, value };
    const event = {};
    event.preventDefault = (() => {});

    onChange(event, data);
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      name, placeholder,
      label, readOnly,
      style,
    } = this.props;

    const { error, messages } = self.state;
    const { fieldType, fieldValue } = getFieldAttribute(self, name);

    if (readOnly) {
      return (
        <Translation>
          {
            (t, { i18n }) => (
              <Form.Field className="readonly">
                { label && (
                  <label>
                    {i18n.exists(name) ? t(name) : name}
                  </label>)
                }

                <NumberFormat
                  name={name}
                  value={fieldValue}
                  thousandSeparator="."
                  decimalSeparator=","
                  displayType="text"

                  renderText={value => (
                    <div className="ui input">
                      <input name={name} placeholder="" readOnly type="text" value={value} style={style} />
                    </div>
                  )}
                />
              </Form.Field>)
          }
        </Translation>
      );
    }

    return (
      <Translation>
        {
          (t, { i18n }) => (
            <Form.Field>
              { label && (
                <label>
                  {i18n.exists(name) ? t(name) : name} {fieldType.required && (<RequiredSign />)}
                </label>)
              }

              <NumberFormat
                name={name}
                value={fieldValue}
                customInput={Input}
                placeholder={placeholder}
                onValueChange={this.onValueChange}
                thousandSeparator="."
                decimalSeparator=","
                error={error ? fieldErrorSelector(name, messages) : false}
                // TODO: add style to get WARNING effect
                style={style}
              />
            </Form.Field>)
        }
      </Translation>
    );
  }
}
// {<Input error={error ? fieldErrorSelector(name, messages) : false} />}
