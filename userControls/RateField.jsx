/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Rating } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { getFieldAttribute } from '../libs/commonHelper';
import RequiredSign from './RequiredSign';

class DateField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      label: PropTypes.bool,
      readOnly: PropTypes.bool,
      maxRating: PropTypes.number,
    };
  }

  static get defaultProps() {
    return {
      label: true,
      readOnly: false,
      maxRating: 5,
    };
  }

  constructor(props) {
    super(props);

    this.onRate = this.onRate.bind(this);
  }

  onRate(e, d) {
    e.preventDefault();

    const { self } = this.context;

    const { name } = this.props;
    const { onChange } = self;

    const value = d.rating;
    const data = { type: 'input.rate', name, value };
    const event = { preventDefault: () => {} };

    onChange(event, data);
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      name, maxRating,
      label, readOnly,
    } = this.props;

    const { fieldType, fieldValue } = getFieldAttribute(self, name);
    const required = (fieldType && fieldType.required) ? fieldType.required : false;
    const fieldName = name.replace('.$', '_');

    if (readOnly) {
      return (
        <Translation>
          {
            (t, { i18n }) => (
              <Form.Field>
                <label>
                  { i18n.exists(fieldName) ? t(fieldName) : fieldName }
                </label>

                <Rating
                  name={name}
                  defaultRating={fieldValue}
                  maxRating={maxRating}
                  icon="star"
                  disabled
                />
              </Form.Field>
            )
          }
        </Translation>);
    }

    return (
      <Translation>
        {
        (t, { i18n }) => (
          <Form.Field>
            {
              label && (
                <label>
                  {(i18n.exists(fieldName) ? t(fieldName) : fieldName)} {required && (<RequiredSign />)}
                </label>
              )
            }

            <Rating
              name={name}
              defaultRating={fieldValue}
              onRate={this.onRate}
              maxRating={maxRating}
              icon="star"
              clearable
            />
          </Form.Field>)
      }
      </Translation>
    );
  }
}

export default DateField;

