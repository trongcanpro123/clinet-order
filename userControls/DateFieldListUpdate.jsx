import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import { Form, Input } from 'semantic-ui-react';
import moment from 'moment';
import { DATE_FORMAT } from '../constants/config';

export default class DateFieldListUpdate extends Component {
  static get propTypes() {
    return {
      name: PropTypes.string,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
      ]),
      label: PropTypes.string,
      required: PropTypes.bool,

      onChange: PropTypes.func.isRequired,
    };
  }

  static get defaultProps() {
    return {
      name: '',
      value: null,
      label: '',
      required: false,
    };
  }

  constructor(props) {
    super(props);

    this.onValueChange = this.onValueChange.bind(this);
  }

  onValueChange(choosenDate) {
    const { name, onChange } = this.props;

    const value = choosenDate ? choosenDate.toDate() : null;
    const data = { type: 'input.date', name, value };
    const event = { preventDefault: () => {} };

    onChange(event, data);
  }

  render() {
    const {
      name, value,
      label, required,
    } = this.props;
    if (required) {
      return (
        <Form.Field>
          <label>
            {label} <span style={{ color: 'red' }}>* </span>
          </label>
          <DatePicker name={name} selected={value ? moment(value) : null} customInput={<Input icon="calendar" placeholder={DATE_FORMAT} required style={{ width: 150 }} />} onChange={this.onValueChange} dateFormat={DATE_FORMAT} />
        </Form.Field>
      );
    }

    return (
      <Form.Field>
        <label>{label}</label>
        <DatePicker name={name} selected={value ? moment(value) : null} customInput={<Input icon="calendar" placeholder={DATE_FORMAT} style={{ width: 150 }} />} onChange={this.onValueChange} dateFormat={DATE_FORMAT} />
      </Form.Field>
    );
  }
}
