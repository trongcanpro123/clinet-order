import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon, Table, Form, Input, Button, Divider, Container, Select } from 'semantic-ui-react';

import { convertDataList2OptionList } from '../libs/commonHelper';

export default class PartnerSelect extends Component {
  static get propTypes() {
    return {
      partner: PropTypes.object.isRequired,
      partnerList: PropTypes.array.isRequired,
      partnerGroupList: PropTypes.array.isRequired,

      onChange: PropTypes.func.isRequired,
      onGetPartnerList: PropTypes.func.isRequired,
      onSelectPartner: PropTypes.func.isRequired,
    };
  }

  constructor(props) {
    super(props);

    this.render = this.render.bind(this);
  }

  render() {
    const PartnerModalRender = ({
      customer,
      index,
      onSelectPartner,
      selected,
    }) => (
      <Table.Row onClick={() => { onSelectPartner(customer._id); }} positive={selected}>
        <Table.Cell textAlign="center">{index}</Table.Cell>
        <Table.Cell textAlign="left" >{`${customer.partnerCode} - ${customer.partnerName}`} </Table.Cell>
        <Table.Cell textAlign="left">{customer.partnerGroupName}</Table.Cell>
        <Table.Cell textAlign="left">{customer.phone}</Table.Cell>
        <Table.Cell textAlign="left">{customer.email}</Table.Cell>
        <Table.Cell textAlign="left">{customer.address}</Table.Cell>
      </Table.Row>
    );

    const PartnerListModalRender = ({ partner, partnerList, onSelectPartner }) => (
      <Table.Body>
        {partnerList.map((customer, index) =>
          <PartnerModalRender key={customer._id} customer={customer} onSelectPartner={onSelectPartner} index={index + 1} selected={partner.selectedPartnerId === customer._id} />)}
      </Table.Body>
    );

    const {
      partner, partnerList, partnerGroupList,
      onChange, onGetPartnerList, onSelectPartner,
    } = this.props;

    const partnerListOptions = convertDataList2OptionList(partnerList, '_id', 'partnerCode', 'partnerName');

    const partnerGroupListOptions = convertDataList2OptionList(partnerGroupList, '_id', 'partnerGroupCode', 'partnerGroupName');

    return (
      <Container>
        <Form.Group>
          <Form.Field width="7" name="partnerId" value={partner.partnerId} control={Select} search selection options={partnerListOptions} onChange={onChange} label="Tên khách hàng" required autoFocus />
          <Form.Field width="1" className="icon_partner_search" >
            <label> &nbsp; </label>
            <Modal trigger={<Button type="button" icon color="blue" size="small"><Icon name="address book" /></Button>} closeIcon>
              <Modal.Header>Tìm kiếm khách hàng</Modal.Header>
              <Modal.Content scrolling>
                <Form>
                  <Form.Group widths="equal">
                    <Form.Field name="filteredPartnerGroupId" value={partner.filteredPartnerGroupId} control={Select} onChange={onChange} selection options={partnerGroupListOptions} label="Nhóm khách hàng" placeholder="Nhóm khách hàng" />
                    <Form.Field name="filteredPartnerName" value={partner.filteredPartnerName} control={Input} onChange={onChange} label="Tên khách hàng" placeholder="Tên khách hàng" />
                  </Form.Group>
                  <Form.Group widths="equal">
                    <Form.Field name="filteredPhone" value={partner.filteredPhone} control={Input} label="Điện thoại" placeholder="Điện thoại" onChange={onChange} />
                    <Form.Field name="filteredEmail" value={partner.filteredEmail} control={Input} label="Email" placeholder="Email" onChange={onChange} />
                  </Form.Group>
                  <Container textAlign="center">
                    <Button type="button" color="blue" onClick={onGetPartnerList}>Tìm kiếm</Button>
                  </Container>
                </Form>

                <Table celled selectable>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell width="one" textAlign="center">STT</Table.HeaderCell>
                      <Table.HeaderCell textAlign="center">Tên khách hàng</Table.HeaderCell>
                      <Table.HeaderCell textAlign="center">Nhóm khách hàng</Table.HeaderCell>
                      <Table.HeaderCell textAlign="center">Điện thoại</Table.HeaderCell>
                      <Table.HeaderCell textAlign="center">Email</Table.HeaderCell>
                      <Table.HeaderCell textAlign="center">Địa chỉ</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>

                  <PartnerListModalRender partner={partner} partnerList={partnerList} onSelectPartner={onSelectPartner} />

                  <Table.Footer />
                </Table>

                <Divider hidden />
              </Modal.Content>
            </Modal>
          </Form.Field>
          <Form.Field width="8" value={partner.partnerGroupName} control={Input} label="Nhóm khách hàng" readOnly className="readonly" />
        </Form.Group>

        <Form.Group widths="equal">
          <Form.Field value={partner.partnerPhone} control={Input} label="Điện thoại" readOnly className="readonly" />
          <Form.Field value={partner.partnerEmail} control={Input} label="Email" readOnly className="readonly" />
        </Form.Group>

        <Form.Field value={partner.partnerAddress} control={Input} label="Địa chỉ" readOnly className="readonly" />

        <Form.Field name="contactName" value={partner.contactName} control={Input} onChange={onChange} label="Người liên hệ" />

        <Form.Group widths="equal">
          <Form.Field name="contactPhone" value={partner.contactPhone} control={Input} onChange={onChange} label="Điện thoại liên hệ" />
          <Form.Field name="contactEmail" value={partner.contactEmail} control={Input} onChange={onChange} label="Email liên hệ" />
        </Form.Group>

        <Form.Field name="contactAddress" value={partner.contactAddress} control={Input} onChange={onChange} label="Địa chỉ liên hệ" />

      </Container>
    );
  }
}

