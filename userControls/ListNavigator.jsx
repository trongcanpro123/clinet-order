/* eslint-disable class-methods-use-this */
/* eslint-disable react/prop-types */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Dropdown, Grid, GridColumn } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { ITEM_AMOUNT_PER_PAGE, ITEM_AMOUNT_PER_PAGE_VALUES, PAGE_RANGE_DISPLAYED } from '../constants/config';

const PageRender = ({
  page, active, disabled,
  onPageChange,
}) => (<Menu.Item as="a" active={active} disabled={disabled} onClick={onPageChange} size="tiny">{page}</Menu.Item>);

PageRender.propTypes = {
  page: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  active: PropTypes.bool,
  disabled: PropTypes.bool,
  onPageChange: PropTypes.func,
};

PageRender.defaultProps = {
  page: 1,
  active: false,
  disabled: false,
  onPageChange: () => {},
};

class ListNavigator extends React.Component {
  async onChangeActionList(self, e, d) {
    e.preventDefault();

    const { value } = d;
    const { actionList } = self.props;

    if (value) {
      console.log('ListNavigator.value', value);
      const selecteAction = actionList.find(f => f.actionCode === value);

      if (selecteAction) {
        await (selecteAction.actionHandler)(self);
      }
    }
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    // [!] Cause of props change twice but component load once => CAN NOT save in state
    const { actionList } = self.props;

    const {
      state,
      onPageChange, onItemsPerPageChange,
    } = self;

    const {
      query, loading,
      objectList, selectedObjectList,
    } = state;

    const activePage = query.page ? query.page : 1;
    const itemsPerPage = query.itemsPerPage ? query.itemsPerPage : ITEM_AMOUNT_PER_PAGE;
    const totalAmount = objectList && objectList.length ? objectList.length : 0;
    const selectedAmount = selectedObjectList ? selectedObjectList.length : 0;

    let pageAmount = 0;
    const remainAmount = totalAmount % itemsPerPage;

    if (remainAmount > 0) {
      pageAmount = ((totalAmount - remainAmount) / itemsPerPage) + 1;
    } else {
      pageAmount = (totalAmount / itemsPerPage);
    }

    const HALF_PAGE_RANGE_DISPLAYED = (PAGE_RANGE_DISPLAYED - (PAGE_RANGE_DISPLAYED % 2)) / 2;
    let minPage = (activePage - HALF_PAGE_RANGE_DISPLAYED - 1);
    let maxPage = (activePage + HALF_PAGE_RANGE_DISPLAYED + 1);

    minPage = minPage > 1 ? minPage : 1;
    maxPage = maxPage < pageAmount ? maxPage : pageAmount;

    const pageList = [];

    for (let i = minPage; i <= maxPage; i += 1) {
      pageList.push(i);
    }

    const actionListOptions = [];
    const actionCount = actionList.length;

    actionList.forEach((action) => {
      actionListOptions.push({
        key: action.actionCode,
        value: action.actionCode,
        text: action.actionName,
      });
    });

    return (
      <Translation>
        {
          (t) => {
            const ITEM_AMOUNT_PER_PAGE_OPTIONS = ITEM_AMOUNT_PER_PAGE_VALUES.map(amount => ({ key: amount, text: t('system:pageNav.itemsPerPage', { itemsPerPage: amount }), value: amount }));

            return (
              <React.Fragment>
                <Grid>
                  <GridColumn width="9" verticalAlign="middle" textAlign="left">
                    { t('system:pageNav.totalAmount', { totalAmount })}

                    { (selectedAmount > 0) && (actionCount > 0) && (
                      <Dropdown
                        selection
                        text="Thực hiện..."
                        options={actionListOptions}
                        onChange={(e, d) => { this.onChangeActionList(self, e, d); }}
                        loading={loading}
                      />) }&nbsp;

                    { t('system:pageNav.selectedAmount', { selectedAmount }) }
                  </GridColumn>
                  <GridColumn width="7">
                    <Menu floated="right" pagination size="mini">
                      { minPage > 1 ? <PageRender key={1} active={false} page={1} onPageChange={onPageChange} /> : '' }

                      {pageList.map(page =>
                              (<PageRender
                                key={page}
                                active={page === activePage}
                                disabled={!!((page < (activePage - HALF_PAGE_RANGE_DISPLAYED)) || (page > (activePage + HALF_PAGE_RANGE_DISPLAYED)))}
                                page={(page < (activePage - HALF_PAGE_RANGE_DISPLAYED)) || (page > (activePage + HALF_PAGE_RANGE_DISPLAYED)) ? '...' : page}
                                onPageChange={onPageChange}
                              />))}

                      {totalAmount === 0 ? <PageRender key={0} active={false} page="x" /> : ''}

                      { maxPage < pageAmount ? <PageRender key={pageAmount} active={false} page={pageAmount} onPageChange={onPageChange} /> : '' }

                      <Dropdown
                        value={itemsPerPage}
                        onChange={onItemsPerPageChange}
                        options={ITEM_AMOUNT_PER_PAGE_OPTIONS}
                        simple
                        item
                      />
                    </Menu>
                  </GridColumn>
                </Grid>
              </React.Fragment>
            );
          }
        }
      </Translation>
    );
  }
}

export default ListNavigator;
