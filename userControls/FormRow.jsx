/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import { PropsChildrenPropType } from '../libs/componentHelper';

class FormRow extends Component {
  static get propTypes() {
    return {
      children: PropsChildrenPropType.isRequired,
    };
  }

  render() {
    return (
      <Form.Group widths="equal">
        { this.props.children }
      </Form.Group>
    );
  }
}

export default FormRow;

