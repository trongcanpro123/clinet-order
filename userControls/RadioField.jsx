/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Radio } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { PRIMARY_COLOR } from '../constants/config';
import { fieldErrorSelector } from '../libs/errorHelper';
import { getFieldAttribute } from '../libs/commonHelper';

class RadioField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      label: PropTypes.bool,
      onChange: PropTypes.func,
      readOnly: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      label: true,
      onChange: undefined,
      readOnly: false,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { name, label, readOnly } = this.props;
    const userDefinedOnChange = this.props.onChange;
    const { state, onChange } = self;
    const { error, messages } = state;
    const { fieldType, fieldValue } = getFieldAttribute(self, name);

    return (
      <Translation>
        {
          (t, { i18n }) => (<Form.Field
            name={name}
            checked={fieldValue}
            onChange={userDefinedOnChange || onChange}
            label={label && (i18n.exists(name) ? t(name) : name)}
            error={error ? fieldErrorSelector(name, messages) : false}
            required={fieldType.required ? fieldType.required : false}
            disabled={readOnly}

            control={Radio}
            toggle
            color={PRIMARY_COLOR}
          />)
        }
      </Translation>);
  }
}

export default RadioField;
