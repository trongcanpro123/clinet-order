/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';

class EmptyField extends Component {
  render() {
    return (<Form.Field />);
  }
}

export default EmptyField;

