/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { PropsChildrenPropType } from '../libs/componentHelper';

export default class ListTable extends Component {
  static get propTypes() {
    return {
      children: PropsChildrenPropType.isRequired,
    };
  }

  render() {
    return (
      <Table striped selectable compact celled singleLine>
        { this.props.children }
      </Table>
    );
  }
}

