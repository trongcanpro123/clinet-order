/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Message, Grid, Button, Icon } from 'semantic-ui-react';

import { PRIMARY_COLOR } from '../constants/config';

class FormTitle extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { functionName, permmission } = self.props;
    const {
      state,
      onObjectListClick, onCopyObjectClick,
      onPrevObjectClick, onNextObjectClick,
    } = self;

    const {
      loading,
      objectId, prevObjectId, nextObjectId,
    } = state;

    return (
      <Message attached>
        <Message.Content>
          <Message.Header>
            <Grid columns={2}>
              <Grid.Row>
                <Grid.Column computer={12}>Danh mục khách hàng</Grid.Column>
                <Grid.Column computer={4} textAlign="right">
                  <Button icon name="list" color={PRIMARY_COLOR} size="mini" onClick={onObjectListClick} loading={loading} >
                    <Icon name="list" />
                  </Button>
                  <Button icon name="form" color={PRIMARY_COLOR} size="mini" disabled loading={loading}>
                    <Icon name="file outline" />
                  </Button>
                  <Button icon name="copy" color={PRIMARY_COLOR} size="mini" disabled={(objectId === '') || (objectId === '0') || (!permmission.canCreate)} onClick={onCopyObjectClick} loading={loading}>
                    <Icon name="copy" />
                  </Button>
                  <Button icon name="prev" color={PRIMARY_COLOR} size="mini" disabled={!prevObjectId} onClick={onPrevObjectClick} loading={loading}>
                    <Icon name="angle left" />
                  </Button>
                  <Button icon name="next" color={PRIMARY_COLOR} size="mini" disabled={!nextObjectId} onClick={onNextObjectClick} loading={loading}>
                    <Icon name="angle right" />
                  </Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Message.Header>
        </Message.Content>
      </Message>
    );
  }
}

export default FormTitle;

