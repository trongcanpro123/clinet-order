/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Select } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import _ from 'lodash';

import { fieldErrorSelector } from '../libs/errorHelper';
import { getFieldAttribute } from '../libs/commonHelper';

class SelectionField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      options: PropTypes.arrayOf(PropTypes.shape({
        key: PropTypes.string,
        value: PropTypes.oneOfType([
          PropTypes.string,
          PropTypes.bool,
          PropTypes.number,
        ]),
        text: PropTypes.string,
      })),
      search: PropTypes.bool,
      multiple: PropTypes.bool,
      alt: PropTypes.string,
      label: PropTypes.bool,
      onChange: PropTypes.func,
      disabled: PropTypes.bool,
      style: PropTypes.object,
    };
  }

  static get defaultProps() {
    return {
      options: undefined,
      search: true,
      multiple: false,
      alt: '',
      label: true,
      onChange: undefined,
      disabled: false,
      style: { whiteSpace: 'nowrap' },
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      name, options,
      search, multiple,
      label, disabled, alt,
      style,
    } = this.props;

    const userDefinedOnChange = this.props.onChange;
    const { state, onChange } = self;
    const { error, messages } = state;
    const { fieldType, fieldValue } = getFieldAttribute(self, name);
    const title = (alt !== '') ? alt : name;

    if (!fieldType) {
      return (<React.Fragment />);
    }

    // TODO: check case both of  options && fieldType.options is undefined

    return (
      <Translation>
        {
          (t, { i18n }) => (<Form.Field
            name={name}
            value={fieldValue}
            required={fieldType.required || false}
            onChange={userDefinedOnChange || onChange}
            label={label && (i18n.exists(title) ? t(title) : title)}
            options={_.isArray(options) ? options : fieldType.options}
            error={error ? fieldErrorSelector(name, messages) : false}
            search={search}
            multiple={multiple}
            disabled={disabled}

            control={Select}
            fluid
            selection
            style={style}
          />)
        }
      </Translation>);
  }
}

export default SelectionField;

