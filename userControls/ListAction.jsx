/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import _ from 'lodash';

import { PRIMARY_COLOR } from '../constants/config';

export default class ListAction extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string,
      onClick: PropTypes.func.isRequired,
      type: PropTypes.string,
      icon: PropTypes.string,
      color: PropTypes.string,
      size: PropTypes.string,
      permission: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      name: '',
      type: 'button',
      icon: undefined,
      color: PRIMARY_COLOR,
      size: 'medium',
      permission: true,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { loading } = self.state;
    const { functionActionList } = self.props;

    if (!_.isArray(functionActionList)) return (<React.Fragment />);

    const {
      name, type,
      color, icon, size,
      onClick,
      permission,
    } = this.props;

    if (permission) {
      if (functionActionList.indexOf(name) < 0) return (<React.Fragment />);
    }

    if (icon) {
      return (<Button onClick={onClick} type={type} icon={icon} loading={loading} color={color} size={size} />);
    }

    return (
      <Translation ns="system">
        {
          (t, { i18n }) => (
            <Button onClick={onClick} type={type} loading={loading} color={color} size={size} >
              {i18n.exists(name) ? t(name) : name}
            </Button>
          )
        }
      </Translation>
    );
  }
}
