/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';
import { Form, Input } from 'semantic-ui-react';
import moment from 'moment';
import { Translation } from 'react-i18next';
import { format } from 'date-and-time';

import { DATETIME_FORMAT, TIME_FORMAT } from '../constants/config';
import { fieldErrorSelector } from '../libs/errorHelper';
import { getFieldAttribute } from '../libs/commonHelper';
import { OPERATOR_SIGN, OPERATOR_REPLACER } from '../libs/constants/mongoOperator';

export default class DateTimeField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      placeholder: PropTypes.string,
      label: PropTypes.bool,
      readOnly: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      placeholder: [DATETIME_FORMAT, TIME_FORMAT],
      label: true,
      readOnly: false,
    };
  }

  constructor(props) {
    super(props);

    this.onValueChange = this.onValueChange.bind(this);
  }

  onValueChange(choosenDate) {
    const { self } = this.context;
    const { name } = this.props;
    const { onChange } = self;

    const value = choosenDate ? choosenDate.toDate() : null;
    const data = { type: 'input.date', name, value };
    const event = { preventDefault: () => {} };

    onChange(event, data);
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      name, placeholder,
      label, readOnly,
    } = this.props;

    const { error, messages } = self.state;
    const { fieldType, fieldValue } = getFieldAttribute(self, name);
    const required = (fieldType && fieldType.required) ? fieldType.required : false;
    const fieldName = name.replace(OPERATOR_SIGN, OPERATOR_REPLACER);

    if (readOnly) {
      return (
        <Translation>
          {
            (t, { i18n }) => (<Form.Field
              name={name}
              value={fieldValue ? format(new Date(fieldValue), DATETIME_FORMAT) : ''}
              label={label && (i18n.exists(name) ? t(name) : name)}

              readOnly
              fluid
              className="readonly"
              style={{ minWidth: 150 }}
              control={Input}
            />)
          }
        </Translation>);
    }

    return (
      <Translation>
        {
        (t, { i18n }) => (
          <Form.Field>
            {
              label && (
                <label>
                  {(i18n.exists(fieldName) ? t(fieldName) : fieldName)} {required && (<span style={{ color: 'red' }}>* </span>)}
                </label>
              )
            }

            <DatePicker
              name={name}
              selected={fieldValue ? moment(fieldValue) : null}
              customInput={<Input icon="calendar" placeholder={placeholder} required fluid style={{ minWidth: 200 }} error={error ? fieldErrorSelector(name, messages) : false} />}
              onChange={this.onValueChange}
              showTimeSelect
              timeFormat={TIME_FORMAT}
              timeIntervals={5}
              timeCaption="Time"
              style={{ padding: 0 }}
              dateFormat={DATETIME_FORMAT}
            />
          </Form.Field>)
      }
      </Translation>
    );
  }
}
