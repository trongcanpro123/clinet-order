/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Message, Container, Step } from 'semantic-ui-react';
import PropTypes from 'prop-types';

class FormWorkFlow extends Component {
  static get propTypes() {
    return {
      steps: PropTypes.arrayOf(PropTypes.object).isRequired,
    };
  }

  render() {
    const { steps } = this.props;
    return (
      <Message attached>
        <Container textAlign="left">
          <Step.Group items={steps} size="tiny" widths={steps.length} fluid />
        </Container>
      </Message>
    );
  }
}

export default FormWorkFlow;

