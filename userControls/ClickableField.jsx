/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class ClickableField extends Component {
  static get propTypes() {
    return {
      baseUrl: PropTypes.string.isRequired,
      objectId: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
      active: PropTypes.bool,

      onClick: PropTypes.func.isRequired,
    };
  }

  static get defaultProps() {
    return {
      active: false,
    };
  }

  render() {
    const {
      baseUrl, objectId,
      label, active, onClick,
    } = this.props;

    return (
      <Link
        to={`${baseUrl}/${objectId}`.replace('//', '/')}
        onClick={() => { onClick(objectId); return false; }}
        color={active ? 'red' : 'blue'}
      >
        {label}
      </Link>);
  }
}

export default ClickableField;
