/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Message } from 'semantic-ui-react';
import _ from 'lodash';
import { Trans } from 'react-i18next';

import FormComment from './FormComment';

const ThisContext = React.createContext({});

class FormFooter extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      error, warning, success,
      messages,
    } = self.state;

    if (_.isString(messages)) {
      if (messages.startsWith('system:msg.')) { // system message
        return (
          <ThisContext.Provider value={{ self }}>
            <Message attached="bottom" error={error} warning={warning} success={success} >
              <Message.Content>
                <Trans i18nKey={messages} />
              </Message.Content>
            </Message>

            <FormComment />
          </ThisContext.Provider>
        );
      }

      return (
        <ThisContext.Provider value={{ self }}>
          <Message attached="bottom" error={error} warning={warning} success={success} >
            <Message.Content>
              {messages}
            </Message.Content>
          </Message>

          <FormComment />
        </ThisContext.Provider>);
    } else if (_.isArray(messages)) {
      return (
        <ThisContext.Provider value={{ self }}>
          <Message attached="bottom" error={error} warning={warning} success={success}>
            <Message.Content>
              {
                messages.map(msg => (
                  <React.Fragment key={msg.name}>
                    <Trans i18nKey={`common:${msg.name}`} />: <Trans i18nKey={`${msg.message}`} />
                    <br />
                  </React.Fragment>
                ))
              }
            </Message.Content>
          </Message>

          <FormComment />
        </ThisContext.Provider>
      );
    }

    return (
      <ThisContext.Provider value={{ self }}>
        <Message attached="bottom" error={error} warning={warning} success={success} >
          <Message.Content>
            {JSON.stringify(messages)}
          </Message.Content>
        </Message>

        <FormComment />
      </ThisContext.Provider>
    );
  }
}

export default FormFooter;
