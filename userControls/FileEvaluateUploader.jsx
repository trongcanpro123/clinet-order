/* eslint-disable react/no-array-index-key */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone';
import date from 'date-and-time';
import { Form, Input, Button, Table } from 'semantic-ui-react';
import _ from 'lodash';
import createCachedSelector from 're-reselect';
import { Translation } from 'react-i18next';

import {
  UPLOAD_FILE_MAX_SIZE, UPLOAD_FILE_EXTENSION, DATETIME_FORMAT,
  PRIMARY_COLOR,
} from '../../pm/constants/config';

import FormTable from '../../pm/userControls/FormTable';
import FormTableHeader from '../../pm/userControls/FormTableHeader';
import FormScrollArea from '../../pm/userControls/FormScrollArea';

const fileName = 'system:file.fileName';
const fileType = 'system:file.fileType';
const fileSize = 'system:file.fileSize';
const uploadedAt = 'system:file.uploadedAt';

const evaluateListHeaderRender = (
  <Translation>
    {
    (t, { i18n }) => (
      <FormTableHeader>
        <Table.HeaderCell width={1} textAlign="center">
          { t('index')}
        </Table.HeaderCell>
        <Table.HeaderCell width={5} textAlign="center">
          {i18n.exists(fileName) ? t(fileName) : fileName}
        </Table.HeaderCell>
        <Table.HeaderCell width={3} textAlign="center">
          {i18n.exists(fileType) ? t(fileType) : fileType}
        </Table.HeaderCell>
        <Table.HeaderCell width={2} textAlign="center">
          {i18n.exists(fileSize) ? t(fileSize) : fileSize}
        </Table.HeaderCell>
        <Table.HeaderCell width={3} textAlign="center">
          {i18n.exists(uploadedAt) ? t(uploadedAt) : uploadedAt}
        </Table.HeaderCell>
        <Table.HeaderCell width={2} />
      </FormTableHeader>
    )
  }
  </Translation>
);

const evaluateListRenderSelector = createCachedSelector(
  self => self,
  (self, state, fieldName) => state.object[fieldName],
  (self, state, fieldName) => fieldName,
  (self, evaluateList, fieldName) => {
    const { onDownloadFile, onDeleteSubDocument } = self;
    if (_.isArray(evaluateList)) {
      return (
        <Table.Body>
          {evaluateList.map((file, index) =>
          (
            <Table.Row key={`${fieldName}.${index}`}>
              <Table.Cell textAlign="center" verticalAlign="top">{index + 1}</Table.Cell>
              <Table.Cell textAlign="left" verticalAlign="top">{file.originalName}</Table.Cell>
              <Table.Cell textAlign="center" verticalAlign="top">{file.contentType}</Table.Cell>
              <Table.Cell textAlign="right" verticalAlign="top">{file.size}</Table.Cell>
              <Table.Cell textAlign="center" verticalAlign="top">{date.format(new Date(file.uploadDate), DATETIME_FORMAT)}</Table.Cell>
              <Table.Cell textAlign="center" verticalAlign="top">
                <Button type="button" icon="download" onClick={() => { onDownloadFile(file.fileId); }} color={PRIMARY_COLOR} size="mini" />
                <Button type="button" icon="delete" onClick={(e) => { onDeleteSubDocument(e, `${fieldName}.${index}.fileId`); }} color={PRIMARY_COLOR} size="mini" />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>);
    }

    return (<Table.Body />);
  },
)((self, state, fieldName, cacheName) => cacheName);

export default class FileEvaluateUploader extends Component {
  static get propTypes() {
    return {
      name: PropTypes.string,
      label: PropTypes.string,
      placeholder: PropTypes.string,
      maxSize: PropTypes.number,
      extension: PropTypes.string,
    };
  }

  static get defaultProps() {
    return {
      name: 'file',
      label: 'File đính kèm',
      placeholder: 'Kéo file vào đây...',
      maxSize: UPLOAD_FILE_MAX_SIZE,
      extension: UPLOAD_FILE_EXTENSION,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { onUploadFile } = self;

    const {
      name, maxSize, placeholder, label,
      extension,
    } = this.props;

    return (
      <React.Fragment>
        <Form.Group widths="equal">
          <Dropzone
            name={name}
            maxSize={maxSize}
            onDrop={(uploadedFileList) => { onUploadFile(uploadedFileList, name); }}
            accept={extension}
            style={{
              position: 'relative',
              marginLeft: '8px',
            }}
          >
            <Form.Field control={Input} placeholder={placeholder} label={label} icon="attach" />
          </Dropzone>
          <Form.Field />
        </Form.Group>

        <FormScrollArea name="Danh sách file đính kèm">
          <FormTable>
            { evaluateListHeaderRender }
            { evaluateListRenderSelector(self, self.state, name, name) }
          </FormTable>
        </FormScrollArea>

      </React.Fragment>
    );
  }
}
