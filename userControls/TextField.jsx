/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { fieldErrorSelector } from '../libs/errorHelper';
import { getFieldAttribute } from '../libs/commonHelper';

class TextField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      type: PropTypes.string,
      placeholder: PropTypes.string,
      readOnly: PropTypes.bool,
      label: PropTypes.bool,
      alt: PropTypes.string,
      onChange: PropTypes.func,
    };
  }

  static get defaultProps() {
    return {
      type: 'text',
      placeholder: '',
      readOnly: false,
      label: true,
      alt: '',
      onChange: undefined,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      name, type,
      placeholder, readOnly,
      label, alt
    } = this.props;

    const userDefinedOnChange = this.props.onChange;
    const { state, onChange } = self;
    const { error, messages } = state;
    const { fieldType, fieldValue } = getFieldAttribute(self, name);
    const required = fieldType ? fieldType.required : false;
    const translated = fieldType ? fieldType.translated : false;
    const title = (alt !== '') ? alt : name;

    if (readOnly) {
      return (
        <Translation>
          {
            (t, { i18n }) => (<Form.Field
              name={name}
              value={(translated && (i18n.exists(fieldValue))) ? t(fieldValue) : fieldValue}
              label={label && (i18n.exists(title) ? t(title) : title)}
              type={type}
              placeholder={placeholder}

              readOnly
              className="readonly"
              control={Input}
            />)
          }
        </Translation>);
    }

    return (
      <Translation>
        {
          (t, { i18n }) => (<Form.Field
            name={name}
            value={fieldValue}
            onChange={userDefinedOnChange || onChange}
            label={label && (i18n.exists(title) ? t(title) : title)}
            required={required}
            error={error ? fieldErrorSelector(name, messages) : false}
            type={type}
            placeholder={placeholder}

            control={Input}
          />)
        }
      </Translation>);
  }
}

export default TextField;
