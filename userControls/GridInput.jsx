import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form } from 'semantic-ui-react';

// fix lost focus is case dynamic binding
export default class GridInput extends Component {
  static get propTypes() {
    return {
      name: PropTypes.string,
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      label: PropTypes.string,
      placeholder: PropTypes.string,
      error: PropTypes.bool,
      required: PropTypes.bool,
      style: PropTypes.object,

      onChange: PropTypes.func.isRequired,
    };
  }

  static get defaultProps() {
    return {
      name: '',
      value: '',
      label: '',
      placeholder: '',
      error: false,
      required: false,
      style: {},
    };
  }

  constructor(props) {
    super(props);

    this.onBlur = this.onBlur.bind(this);
  }

  onBlur(e) { // DONOT use onChange because occur lost focus
    const { name, onChange } = this.props;
    const oldValue = this.props.value;
    const { value } = e.target;

    if (oldValue !== value) {
      const data = { type: 'input.gridInput', name, value };
      const event = { preventDefault: () => {} };

      onChange(event, data);
    }
  }

  render() {
    const {
      name, value,
      placeholder, label,
      required, error, style,
    } = this.props;

    if (required) {
      return (
        <Form.Field error={error}>
          <label>
            {label} <span style={required ? { color: 'red' } : {}}>* </span>
          </label>
          <input type="text" name={name} defaultValue={value} onBlur={this.onBlur} onChange={() => {}} placeholder={placeholder} style={style} />
        </Form.Field>
      );
    }

    return (
      <Form.Field>
        <label>{label}</label>
        <input type="text" defaultValue={value} onBlur={this.onBlur} onChange={() => {}} placeholder={placeholder} style={style} />
      </Form.Field>
    );
  }
}
