/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { Image, Form } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import Dropzone from 'react-dropzone';

import { LOADING_STATE } from '../libs/componentHelper';
import { getFieldAttribute } from '../libs/commonHelper';
import { API_GATEWAY_URL } from '../constants/config';
import { apiUpload } from '../libs/apiHelper';
import { imageAvatar, imageUnavailable } from '../constants/htmlResource';
import { apiError2Messages } from '../libs/errorHelper';

class ImageField extends React.Component {
  static get propTypes() {
    return {
      name: PropTypes.string.isRequired,
      avatar: PropTypes.bool,
      bordered: PropTypes.bool,
      circular: PropTypes.bool,
      className: PropTypes.string,
      fluid: PropTypes.bool,
      size: PropTypes.string,
      rounded: PropTypes.bool,
      label: PropTypes.bool,
      href: PropTypes.string,
      readOnly: PropTypes.bool,
    };
  }

  static get defaultProps() {
    return {
      avatar: false,
      bordered: false,
      circular: false,
      className: '',
      fluid: false,
      size: '',
      rounded: false,
      label: true,
      href: '',
      readOnly: false,
    };
  }

  constructor(props) {
    super(props);

    this.onUploadFile = this.onUploadFile.bind(this);
  }

  async onUploadFile(uploadedFileList) {
    const { self } = this.context;

    self.setState(LOADING_STATE);

    const { data, error } = await apiUpload(uploadedFileList);

    if (error) {
      self.setState({
        loading: false,
        error: true,
        messages: apiError2Messages(error),
      });
    } else if (data) {
      const { name } = this.props;

      const {
        isListComponent,
        query, object,
      } = self.state;

      const imageFileId = data.data[0].id;

      if (isListComponent) {
        self.setState({
          loading: false,

          query: {
            ...query,
            [name]: imageFileId,
          },
        });
      } else {
        self.setState({
          loading: false,

          object: {
            ...object,
            [name]: imageFileId,
          },
        });
      }
    } else {
      self.setState({
        loading: false,
      });
    }
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const {
      name, readOnly,
      avatar, circular, rounded, bordered,
      className, label, href,
      fluid, size,
    } = this.props;

    const { fieldValue } = getFieldAttribute(self, name);
    let imageSrc = '';

    if (fieldValue !== '' && fieldValue !== '0') {
      imageSrc = `${API_GATEWAY_URL}/v2/files/${fieldValue}`;
    } else {
      imageSrc = avatar ? imageAvatar : imageUnavailable;
    }

    if (readOnly) {
      return (
        <Translation>
          {
            (t, { i18n }) => (
              <Form.Field>
                {
                  label && (
                    <label>
                      {(i18n.exists(name) ? t(name) : name)}
                    </label>
                  )
                }

                <Image
                  src={imageSrc}
                  avatar={avatar}
                  bordered={bordered}
                  circular={circular}
                  className={className}
                  fluid={fluid}
                  size={size}
                  rounded={rounded}
                  href={href}
                />
              </Form.Field>)
          }
        </Translation>
      );
    }

    return (
      <Translation>
        {
          (t, { i18n }) => (
            <Form.Field>
              {
                label && (
                  <label>
                    {(i18n.exists(name) ? t(name) : name)}
                  </label>
                )
              }

              <Dropzone
                name={name}
                maxSize={10000000}
                onDrop={(uploadedFileList) => { this.onUploadFile(uploadedFileList, name); }}
                accept=".png, .jpg, .gif"
                style={{
                  position: 'relative',
                  marginLeft: '8px',
                }}
              >
                <Image
                  src={imageSrc}
                  avatar={avatar}
                  bordered={bordered}
                  circular={circular}
                  className={className}
                  fluid={fluid}
                  size={size}
                  rounded={rounded}
                  href={href}
                />
              </Dropzone>
            </Form.Field>)
        }
      </Translation>
    );
  }
}

export default ImageField;
