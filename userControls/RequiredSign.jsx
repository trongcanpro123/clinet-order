import React from 'react';

const RequiredSign = () => (
  <span style={{ color: 'rgb(219, 40, 40)' }}>&nbsp;*</span>
);

export default RequiredSign;
