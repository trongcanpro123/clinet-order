/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Table, Checkbox } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import createCachedSelector from 're-reselect';
import _ from 'lodash';
import { format } from 'date-and-time';

import { bindComponentToContext } from '../libs/componentHelper';
import { DATETIME_FORMAT, DATE_FORMAT } from '../constants/config';
import ClickableField from '../userControls/ClickableField';
import ActiveField from '../userControls/ActiveField';
import ListNavigator from './ListNavigator';
import ListScrollArea from './ListScrollArea';
import ListTable from './ListTable';
import { containId, equalToId } from '../libs/commonHelper';
import { DATA_TYPE } from '../constants/dataType';
// import { STATE_LIST } from '../../pm/modules/pim/constants/priceConstant';
// import { MARKETING_STATE_LIST } from '../../pm/modules/var/constants/approveMarketingConstant';
// import { CONTRACT_STATE_LIST } from '../../pm/modules/vendorProfile/constants/contractConstant';

const headerRenderSelector = createCachedSelector(
  self => self,
  (self, state) => state.query.fields,
  (self, state) => state.query.hiddenFields,
  (self, state) => state.selectedAll,
  (self, state, sortedField) => sortedField,
  (self, state, sortedField, sortDirection) => sortDirection,

  (self, fields, hiddenFields, selectedAll, sortedField, sortDirection) => {
    if (_.isArray(fields)) {
      const { onSortBy, onSelectAllObjectList } = self;

      return (
        <Translation>
          {
            (t, { i18n }) => (
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width="one" textAlign="center">
                    {t('index')}
                  </Table.HeaderCell>

                  <Table.HeaderCell width="one" textAlign="center">
                    <Checkbox checked={selectedAll} onChange={onSelectAllObjectList} />
                  </Table.HeaderCell>

                  {
                    fields.map((field) => {
                      if (hiddenFields.findIndex(f => f === field) > -1) {
                        return (<React.Fragment key={`HeaderCell.${field}`} />);
                      }

                      return (
                        <Table.HeaderCell
                          key={`HeaderCell.${field}`}
                          textAlign="center"
                          sorted={sortedField === field ? sortDirection : null}
                          onClick={(e) => {
                            e.preventDefault();
                            onSortBy(field);
                          }
                          }
                        >
                          {i18n.exists(field) ? t(field) : field}
                        </Table.HeaderCell>
                      );
                    })
                  }
                </Table.Row>
              </Table.Header>)
          }
        </Translation>);
    }

    return (<Table.Header />);
  },
)((self, state, sortedField, sortDirection, cacheName) => cacheName);

const objectListRenderSelector = createCachedSelector(
  self => self,
  (self, state) => state.objectId,
  (self, state) => state.objectList,
  (self, state) => state.selectedObjectList,
  (self, state) => state.query.fields,
  (self, state) => state.query.hiddenFields,
  (self, state, keyField) => keyField,

  (self, objectId, objectList, selectedObjectList, fields, hiddenFields, keyField) => {
    if (objectList && objectList.data) {
      const { onObjectClick, onSelectObject } = self;
      const { offset } = objectList.query;
      const { model } = self.state;
      const { baseUrl } = self.props;
      const listKeyField = keyField.split(',');

      if (_.isArray(fields)) {
        return (
          <Table.Body>
            {objectList.data.map((cod, index) =>
              (
                <Table.Row key={`Rows.${cod._id}`} >
                  <Table.Cell textAlign="center">
                    {offset + index + 1}
                  </Table.Cell>

                  <Table.Cell textAlign="center">
                    <Checkbox name={cod._id} checked={containId(selectedObjectList, cod._id)} onChange={onSelectObject} />
                  </Table.Cell>

                  {
                    fields.map((field) => {
                      const fieldType = model[field] ? model[field].type : DATA_TYPE.STRING;

                      if (hiddenFields.findIndex(f => f === field) > -1) {
                        return (<React.Fragment key={`TableCell.${field}`} />);
                      } else if (listKeyField.includes(field)) {
                        return (
                          <Table.Cell textAlign="left" key={`TableCell.${field}`}>
                            <ClickableField
                              baseUrl={baseUrl}
                              objectId={cod._id}
                              label={cod[field]}
                              onClick={onObjectClick}
                              active={equalToId(cod._id, objectId)}
                            />
                          </Table.Cell>
                        );
                      } else if (field === 'active') {
                        return (
                          <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                            <ActiveField active={cod[field]} />
                          </Table.Cell>);
                      } else if (field === 'createdAt') {
                        return (
                          <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                            <React.Fragment>{cod[field] ? format(new Date(cod[field]), DATETIME_FORMAT) : ''}</React.Fragment>
                          </Table.Cell>);
                      } else if (field === 'state') {
                        return (
                          <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                            <React.Fragment>{STATE_LIST[cod[field]]}</React.Fragment>
                          </Table.Cell>
                        );
                      } else if (field === 'marketingState') {
                        return (
                          <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                            <React.Fragment>{MARKETING_STATE_LIST[cod[field]]}</React.Fragment>
                          </Table.Cell>
                        );
                      } else if (field === 'contractState') {
                        return (
                          <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                            <React.Fragment>{CONTRACT_STATE_LIST[cod[field]]}</React.Fragment>
                          </Table.Cell>
                        );
                      }

                      // <React.Fragment> to fix "Invalid prop `children` supplied to `TableCell`" warning
                      switch (fieldType) {
                        case DATA_TYPE.STRING:
                          return (
                            <Table.Cell textAlign="left" key={`TableCell.${field}`} >
                              <React.Fragment>{cod[field]}</React.Fragment>
                            </Table.Cell>
                          );

                        case DATA_TYPE.BOOLEAN:
                          return (
                            <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                              <ActiveField active={cod[field]} />
                            </Table.Cell>
                          );

                        case DATA_TYPE.NUMBER:
                          return (
                            <Table.Cell textAlign="right" key={`TableCell.${field}`} >
                              <React.Fragment>{cod[field]}</React.Fragment>
                            </Table.Cell>
                          );

                        case DATA_TYPE.DATE:
                          return (
                            <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                              <React.Fragment>{cod[field] ? format(new Date(cod[field]), DATE_FORMAT) : ''}</React.Fragment>
                            </Table.Cell>
                          );

                        case DATA_TYPE.DATE_TIME:
                          return (
                            <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                              <React.Fragment>{cod[field] ? format(new Date(cod[field]), DATETIME_FORMAT) : ''}</React.Fragment>
                            </Table.Cell>
                          );

                        default:
                          return (
                            <Table.Cell textAlign="center" key={`TableCell.${field}`} >
                              <React.Fragment>{cod[field]}</React.Fragment>
                            </Table.Cell>
                          );
                      }
                    })
                  }
                </Table.Row>))}
          </Table.Body>);
      }
    }

    return (<Table.Body />);
  },
)((self, state, keyField, cacheName) => cacheName);

const ThisContext = React.createContext({});

class ListSearchResult extends Component {
  static get propTypes() {
    return {
      keyField: PropTypes.string.isRequired,
    };
  }

  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    bindComponentToContext(ListNavigator, ThisContext);

    const { keyField } = this.props;
    const { state } = self;
    const { query, model } = state;

    const { modelName } = model;

    const sortBy = query.sortBy ? query.sortBy.split('.') : [];
    const sortedField = sortBy[0] ? sortBy[0] : '';
    const sortDirection = sortBy[1] === 'desc' ? 'descending' : 'ascending';

    return (
      <ThisContext.Provider value={{ self }}>
        <React.Fragment>
          <ListScrollArea>
            <ListTable>
              {headerRenderSelector(self, state, sortedField, sortDirection, `${modelName}.header`)}
              {objectListRenderSelector(self, self.state, keyField, `${modelName}.objectList`)}
            </ListTable>

          </ListScrollArea>

          <ListNavigator />
        </React.Fragment>
      </ThisContext.Provider>
    );
  }
}

export default ListSearchResult;
