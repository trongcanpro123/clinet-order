/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Button } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

import { PRIMARY_COLOR } from '../constants/config';

export default class ListActionSearch extends React.Component {
  render() {
    const { self } = this.context;
    if (!self || !self.state) return (<React.Fragment />);

    const { state, onSearch } = self;

    const { loading } = state;
    const { permmission } = self.props;

    if (!permmission.canRead) return (<React.Fragment />);

    return (
      <Translation ns="system">
        {
          t => (<Button type="submit" loading={loading} color={PRIMARY_COLOR} onClick={onSearch} > {t('btn.search')} </Button>)
        }
      </Translation>);
  }
}

