/* eslint-disable react/forbid-prop-types */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Table } from 'semantic-ui-react';
import { PropsChildrenPropType } from '../libs/componentHelper';

class ListTableHeader extends Component {
  static get propTypes() {
    return {
      children: PropsChildrenPropType.isRequired,
    };
  }

  render() {
    return (
      <Table.Header>
        <Table.Row>
          { this.props.children }
        </Table.Row>
      </Table.Header>
    );
  }
}

export default ListTableHeader;
