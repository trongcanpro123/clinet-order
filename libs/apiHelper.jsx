import axios from 'axios';
import humps from 'humps';
import pluralize from 'pluralize';
import qs from 'qs';
import _ from 'lodash';

import { API_GATEWAY_URL, UNLIMITED_RETURNED_RESULT, ITEM_AMOUNT_PER_PAGE } from '../constants/config';
import { getToken, getFunctionId } from './commonHelper';

const apiUrlOptions = {
  encode: false,
  arrayFormat: 'indices', // [!] using "brackets" makes merged value issue
};

export function getRequestHeader() {
  const tokenFromStorage = getToken();

  return {
    Authorization: `Bearer ${tokenFromStorage}`,
    'x-function-id': getFunctionId(),
    // [!] TODO User-Agent / Content-Type / Accept
  };
}

export function convertModelName2ApiEndpoint(modelName) {
  return pluralize.plural(humps.camelize(modelName));
}

export async function apiGet(endPoint, queryString) {
  try {
    const result = await axios({
      method: 'GET',
      url: `${API_GATEWAY_URL}/${endPoint}/${queryString}`,
      headers: getRequestHeader(),
    });

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response : error };
  }
}

export function apiGetFullList(endPoint) {
  const queryString = `?limit=${UNLIMITED_RETURNED_RESULT}&active=${true}`;

  return apiGet(endPoint, queryString);
}

export async function apiGetList(endPoint, query) {
  if (query.limit === 0) {
    return {
      model: endPoint,
      data: [],
      query,
    };
  }

  let queryString = '?';
  const apiQuery = {};

  Object.entries(query).forEach(([key, value]) => { // copy and reject null / empty / undefined values
    if (['fields', 'orderBy'].indexOf(key) > -1) {
      if (_.isArray(value)) { // if ARRAY then CONVERT to NOT SPACED STRING
        apiQuery[key] = value.join(',').replace(/\s/g, '');
      } else {
        apiQuery[key] = value.replace(/\s/g, '');
      }
    } else { // other fields
      apiQuery[key] = value;
    }
  });

  if (apiQuery.page) {
    apiQuery.offset = (query.page - 1) * query.itemsPerPage;
    apiQuery.limit = query.itemsPerPage;
  } else {
    if (typeof query.offset === 'undefined') {
      apiQuery.offset = 0;
    } else {
      apiQuery.offset = query.offset;
    }

    if (typeof query.limit === 'undefined') {
      apiQuery.limit = UNLIMITED_RETURNED_RESULT;
    } else {
      apiQuery.limit = query.limit;
    }
  }

  apiQuery.itemsPerPage = undefined;
  apiQuery.page = undefined;

  queryString += qs.stringify(apiQuery, apiUrlOptions);

  return apiGet(endPoint, queryString);
}

export async function apiGetById(endPoint, objectId, fields) {
  const query = fields ? `${objectId}?fields=${fields}` : objectId;

  return apiGet(endPoint, query);
}

export async function apiPost(endPoint, data) {
  try {
    const result = await axios({
      method: 'POST',
      data,
      url: `${API_GATEWAY_URL}/${endPoint}/`,
      headers: getRequestHeader(),
    });

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response : error };
  }
}

export async function apiCreate(endPoint, data) {
  return apiPost(endPoint, data);
}

export async function apiDelete(endPoint, id) {
  try {
    const result = await axios({
      method: 'DELETE',
      url: `${API_GATEWAY_URL}/${endPoint}/${id}`,
      headers: getRequestHeader(),
    });

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response : error };
  }
}

export async function apiDeleteById(endPoint, id) {
  return apiDelete(endPoint, id);
}

export async function apiUpdate(endPoint, data) {
  try {
    const result = await axios({
      method: 'PUT',
      data,
      url: `${API_GATEWAY_URL}/${endPoint}/${data._id}`,
      headers: getRequestHeader(),
    });

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response : error };
  }
}

export async function apiUpdateById(endPoint, data) {
  return apiUpdate(endPoint, data);
}

export async function apiDownloadList(endPoint, action, query) {
  try {
    let queryString = '?';
    let endpointWithAction = '';
    const apiQuery = {};

    if (action) {
      endpointWithAction = `${endPoint}/${action}`;
    } else {
      endpointWithAction = endPoint;
    }

    Object.entries(query).forEach(([key, value]) => {
      // [!] cause of query, fields sent makes missing format in exported file
      if (['fields', 'orderBy'].indexOf(key) < 0) {
        apiQuery[key] = value;
      }
    });

    apiQuery.limit = UNLIMITED_RETURNED_RESULT;
    apiQuery.itemsPerPage = undefined;
    apiQuery.page = undefined;

    queryString += qs.stringify(apiQuery, apiUrlOptions);

    const result = await axios({
      method: 'GET',
      responseType: 'arraybuffer',
      url: `${API_GATEWAY_URL}/${endpointWithAction}/${queryString}`,
      headers: getRequestHeader(),
    });

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response : error };
  }
}

export async function apiPrint(endPoint, action, data) {
  try {
    const result = await axios({
      method: 'POST',
      responseType: 'blob',
      url: `${API_GATEWAY_URL}/${endPoint}/${action}`,
      headers: getRequestHeader(),
      data,
    });

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response : error };
  }
}

export async function apiUpload(fileList) {
  try {
    const tokenFromStorage = getToken();
    const data = new FormData();

    /*
    const config = {
      onUploadProgress: (progressEvent) => {
        var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total );
      }
    };
    */

    fileList.forEach((file) => {
      data.append(file.name, file);
    });

    const result = await axios({
      method: 'POST',
      data,
      url: `${API_GATEWAY_URL}/v2/files`,
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${tokenFromStorage}`,
      },
    });

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response : error };
  }
}

export async function apiDownloadByURL(url) {

  try {
    const result = await axios({
      method: 'GET',
      responseType: 'arraybuffer',
      url,
      headers: getRequestHeader(),
    });
    if (result.status !== 200) {
      return { error: result.response.data };
    }

    return { data: result };
  } catch (error) {
    return { error: error.response ? error.response.data : error };
  }
}

export async function apiDownloadRecodeById(modelName, action, id) {

  try {
    let endPoint = '';

    if (action) {
      endPoint = `${convertModelName2ApiEndpoint(modelName)}/${action}`;
    } else {
      endPoint = `${convertModelName2ApiEndpoint(modelName)}`;
    }
    const result = await axios({
      method: 'GET',
      responseType: 'arraybuffer',
      url: `${API_GATEWAY_URL}/${endPoint}/${id}`,
      headers: getRequestHeader(),
    });

    if (result.status !== 200) {
      return { error: result.response.data };
    }

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response.data : error };
  }
}

export async function apiDownloadIndex(modelName, action, query) {
  try {
    let queryString = '?';
    let endPoint = '';

    if (action) {
      endPoint = `${convertModelName2ApiEndpoint(modelName)}/${action}`;
    } else {
      endPoint = `${convertModelName2ApiEndpoint(modelName)}`;
    }

    if (query.page) {
      queryString += `offset=${(query.page - 1) * ITEM_AMOUNT_PER_PAGE}&limit=${ITEM_AMOUNT_PER_PAGE}`;
    } else {
      if (query.offset) {
        queryString += `offset=${query.offset}`;
      } else {
        queryString += 'offset=0';
      }

      if (query.limit) {
        queryString += `&limit=${query.limit}`;
      } else {
        queryString += `&limit=${ITEM_AMOUNT_PER_PAGE}`;
      }
    }
    Object.entries(query).forEach(([key, value]) => {
      if ((['page', 'offset', 'limit'].indexOf(key) === -1) && (typeof value !== 'undefined')) {
        queryString += `&${key}=${value}`;
      }
    });
    const result = await axios({
      method: 'POST',
      data: query,
      responseType: 'arraybuffer',
      url: `${API_GATEWAY_URL}/${endPoint}/${queryString}`,
      headers: getRequestHeader(),
    });
    if (result.status !== 200) {
      return { error: result.response.data };
    }

    return { data: result.data };
  } catch (error) {
    return { error: error.response ? error.response.data : error };
  }
}