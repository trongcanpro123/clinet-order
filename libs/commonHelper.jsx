/* eslint-disable no-useless-escape */
/* eslint-disable no-control-regex */
import _ from 'lodash';

import { SEPARATE_STRING } from '../constants/config';
import { DATA_TYPE } from '../constants/dataType';

export const SESSION_JWT_KEY = 'jwtToken';
export const SESSION_FUNCTION_ID_KEY = 'functionId';

export function getDefaultValue(dataType, defaultValue) {
  if (typeof defaultValue !== 'undefined') {
    return defaultValue;
  }

  switch (dataType) {
    case DATA_TYPE.ID:
      return '0';
    case DATA_TYPE.BOOLEAN:
    case DATA_TYPE.BOOL:
      return true;
    case DATA_TYPE.ARRAY:
      return [];
    case DATA_TYPE.OBJECT:
      return {};
    case DATA_TYPE.DATE:
    case DATA_TYPE.DATE_TIME:
      return null;
    default:
      return '';
  }
}

export const convert2DataText = (code, name) => code + SEPARATE_STRING + name;

export const convert2CodeAndName = (dataText) => {
  const separatePosition = dataText.indexOf(SEPARATE_STRING);
  let code = '';
  let name = '';

  if (separatePosition > 0) {
    code = dataText.substring(0, separatePosition);
    name = dataText.substring(separatePosition + SEPARATE_STRING.length);
  }

  return { code, name };
};

export const getDataSelectedText = (selectedValue, options) => {
  if (!selectedValue || !options) {
    return null;
  }

  for (let i = 0; i < options.length; i += 1) {
    const option = options[i];
    if (option.value === selectedValue) {
      return option.text;
    }
  }

  return null;
};

export const convertDataList2OptionList = (objectList, keyField = '_id', codeField = 'code', nameField = '') => {
  const optionList = [];

  if (!_.isArray(objectList)) {
    // eslint-disable-next-line no-console
    console.error(`"objectList" is not an array so can not render field set (${keyField}, ${codeField}, ${nameField}) to list options.`);
    return optionList;
  }

  objectList.forEach((obj) => {
    const item = {};

    item.key = obj[keyField];
    item.value = obj[keyField];

    if (nameField !== '') {
      item.text = convert2DataText(obj[codeField], obj[nameField]);
    } else {
      item.text = obj[codeField];
    }

    optionList.push(item);
  });

  const item = {};
  item.key = '';
  item.value = '';
  item.text = 'Chọn giá trị';
  optionList.push(item);

  return optionList;
};

export const convertDataList2OptionListWithOutDuplicate = (objectList, keyField = '_id', codeField = 'code', nameField = '') => {
  const optionList = [];

  if (!_.isArray(objectList)) {
    // eslint-disable-next-line no-console
    console.error(`"objectList" is not an array so can not render field set (${keyField}, ${codeField}, ${nameField}) to list options.`);
    return optionList;
  }

  objectList.forEach((obj) => {
    const item = {};

    item.key = obj[keyField];
    item.value = obj[keyField];

    if (nameField !== '') {
      item.text = convert2DataText(obj[codeField], obj[nameField]);
    } else {
      item.text = obj[codeField];
    }

    optionList.push(item);
  });

  const item = {};
  item.key = '';
  item.value = '';
  item.text = 'Chọn giá trị';
  optionList.push(item);

  var optionListAllList = optionList.map(e => e['text']).map((e, i, final) => final.indexOf(e) === i && i)
    .filter(e => optionList[e]).map(e => optionList[e]);

  return optionListAllList;
};

export const validateSerial = (serial) => {
  const regularExpression = /[A-Za-z0-9\-]{5,50}/;
  return regularExpression.test(serial);
};

export const validateEmail = (emailAddress) => {
  const regularExpression = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))){2,6}$/i;
  return regularExpression.test(emailAddress);
};

export const validatePhone = str => (str ? str.length > 0 : false);

export const getToken = () => localStorage.getItem(SESSION_JWT_KEY);

export const removeToken = () => localStorage.removeItem(SESSION_JWT_KEY);

export const setFunctionId = functionId => localStorage.setItem(SESSION_FUNCTION_ID_KEY, functionId);

export const getFunctionId = () => localStorage.getItem(SESSION_FUNCTION_ID_KEY);

export const getInputValue = (data) => {
  const {
    type, name, value, checked, options,
  } = data;

  return {
    name,
    options,
    value: (type === 'radio' || type === 'checkbox') ? checked : value,
  };
};

export const uniqueConcat = (arr1, arr2) => arr1.concat(arr2.filter(item => arr1.indexOf(item) === -1));

export const uniqueArray = c => c.filter((item, pos) => (c.indexOf(item) === pos));

export const fixOnFocusCursorPosition = (e) => {
  const { value } = e.target;
  e.target.value = '';
  e.target.value = value;
};

export const toNumbericString = number => (number ? number.toLocaleString('de-DE') : '');

export const equalToId = (id, otherId) => {
  if (!id || !otherId) {
    return false;
  }

  return (id.toString() === otherId.toString());
};

export const containId = (objectList, lookUpId) => {
  if (_.isArray(objectList) && lookUpId) {
    return objectList.findIndex(f => equalToId(f, lookUpId)) > -1;
  }

  return false;
};

export const getFieldAttribute = (self, name) => {
  const splitedNameList = name.split('.'); // fieldName.index.subFieldName
  const { model, query, object } = self.state;

  let fieldType;
  let fieldValue;
  if (splitedNameList.length < 3) { // single field or field with '$gt' / '$lt'
    fieldType = model[name];
    fieldValue = query ? query[name] : object[name];
  } else {
    const fieldName = splitedNameList[0];
    const index = splitedNameList[1];
    const subFieldName = splitedNameList[2];
    fieldType = model[fieldName].subModel[subFieldName];
    fieldValue = query ? query[fieldName][index][subFieldName] : object[fieldName][index][subFieldName];
  }

  return {
    fieldType,
    fieldValue,
  };
};
