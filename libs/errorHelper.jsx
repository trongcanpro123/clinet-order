import _ from 'lodash';

export function fieldErrorSelector(fieldName, errorList) {
  if (!errorList) {
    return false;
  }

  if (_.isArray(errorList)) {
    const field = errorList.find(f => f.fieldName === fieldName);

    if (field) {
      return true;
    }
  }

  return false;
}

export function apiError2Messages(apiError) {
  // console.log('apiError', apiError);
  const { status, data } = apiError;

  switch (status) {
    case 401: {
      window.location.href = '/login/';
      return [];
    }

    case 422: {
      let errorObject;

      // console.log('data', data);

      if (data && data.error) {
        errorObject = data.error;

        if (errorObject.errors) {
          errorObject = errorObject.errors;
        } else {
          return errorObject;
        }
      }

      if (errorObject) {
        const messages = [];

        Object.entries(errorObject).forEach(([name, value]) => {
          let message = '';

          switch (value.kind) {
            case 'required':
              message = 'system:msg.validate.required';
              break;

            default:
              message = 'system:msg.validate.failure';
              break;
          }

          messages.push({
            name,
            message,
          });
        });

        return messages;
      }

      return `system:msg.httpResponseCode.${status.toString()}`;
    }

    case 500: {
      const errorObject = data && data.error ? data.error : undefined;

      if (errorObject && errorObject.message) {
        return errorObject.message;
      }

      return `system:msg.httpResponseCode.${status.toString()}`;
    }

    default:
      return `system:msg.httpResponseCode.${status.toString()}`;
  }
}
