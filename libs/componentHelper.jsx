/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import PropTypes from 'prop-types';
import _ from 'lodash';
import { createDraft, finishDraft } from 'immer';
import createCachedSelector from 're-reselect';
import axios from 'axios';
import FileSaver from 'file-saver';
import { createBrowserHistory } from 'history';

import { apiError2Messages } from './errorHelper';
import { apiGetList } from './apiHelper';

import {
  getToken, removeToken,
  getInputValue, getDefaultValue, convertDataList2OptionList, convertDataList2OptionListWithOutDuplicate,
  setFunctionId,
} from './commonHelper';

import { getDefaultModelValue } from '../libs/modelHelper';
import { DATA_TYPE } from '../constants/dataType';
import { APP_CODE, API_GATEWAY_URL } from '../constants/config';


export const LOADING_STATE = {
  loading: true,
  error: false,
  success: false,
  messages: '',
};

export const PermmissionPropType = PropTypes.shape({
  functionId: PropTypes.string,
  moduleId: PropTypes.string,
  functionActionList: PropTypes.array,

  canCreate: PropTypes.bool,
  canRead: PropTypes.bool,
  canUpdate: PropTypes.bool,
  canDelete: PropTypes.bool,
});

export const ObjectListPropType = PropTypes.shape({
  model: PropTypes.string,
  length: PropTypes.number,
  data: PropTypes.array,
  query: PropTypes.object,
});

export const PropsChildrenPropType = PropTypes.oneOfType([
  PropTypes.array,
  PropTypes.object,
]);


export const listOptionsSelectorWithOutDuplicate = createCachedSelector(
  (objectList, keyField, codeField, nameField, fieldName) => objectList,
  (objectList, keyField, codeField, nameField, fieldName) => keyField,
  (objectList, keyField, codeField, nameField, fieldName) => codeField,
  (objectList, keyField, codeField, nameField, fieldName) => nameField,
  (objectList, keyField, codeField, nameField) => {
    if (objectList && objectList.data) {
      return convertDataList2OptionListWithOutDuplicate(objectList.data, keyField, codeField, nameField);
    }
    return [];
  },
)((objectList, keyField, codeField, nameField, fieldName) => fieldName);


export const listOptionsSelector = createCachedSelector(
  (objectList, keyField, codeField, nameField, fieldName) => objectList,
  (objectList, keyField, codeField, nameField, fieldName) => keyField,
  (objectList, keyField, codeField, nameField, fieldName) => codeField,
  (objectList, keyField, codeField, nameField, fieldName) => nameField,
  (objectList, keyField, codeField, nameField) => {
    if (objectList && objectList.data) {
      return convertDataList2OptionList(objectList.data, keyField, codeField, nameField);
    }
    return [];
  },
)((objectList, keyField, codeField, nameField, fieldName) => fieldName);

export const directListOptionsSelector = createCachedSelector(
  (objectList, keyField, codeField, nameField, cacheName) => objectList,
  (objectList, keyField, codeField, nameField, cacheName) => keyField,
  (objectList, keyField, codeField, nameField, cacheName) => codeField,
  (objectList, keyField, codeField, nameField, cacheName) => nameField,
  (objectList, keyField, codeField, nameField) => {
    if (objectList) {
      return convertDataList2OptionList(objectList, keyField, codeField, nameField);
    }
    return [];
  },
)((objectList, keyField, codeField, nameField, cacheName) => cacheName);

export const bindComponentToContext = (componentList, context) => {
  if (_.isArray(componentList)) {
    componentList.forEach((component) => {
      component.contextType = context;
    });
  } else {
    componentList.contextType = context;
  }
};

export const getLinkedObjects = (objectId, objectListData) => {
  const currentIndex = objectListData.findIndex(val => val._id.toString() === objectId.toString());
  const prevObjectId = (currentIndex > -1) && (currentIndex > 0) ? objectListData[currentIndex - 1]._id : '';
  const nextObjectId = (currentIndex > -1) && (currentIndex < (objectListData.length - 1)) ? objectListData[currentIndex + 1]._id : '';

  return {
    prevObjectId,
    nextObjectId,
  };
};

export function removeJunkValue(self, query) {
  const nomalizedQuery = {};
  const { model } = self.state;
  let fieldType = '';

  Object.entries(query).forEach(([key, value]) => { // copy and reject null / empty / undefined values
    switch (value) {
      case undefined:
      case '':
      case null:
        // console.log('removed key, value', key, value);
        break;

      case '0': // remove ID = 0 query
        fieldType = model[key] ? model[key].type : DATA_TYPE.STRING;

        if (fieldType !== DATA_TYPE.ID) {
          nomalizedQuery[key] = value;
        }
        break;

      default:
        // console.log('default', key, value);
        if (['isDefaultQuery', 'hiddenFields'].indexOf(key) === -1) { // other fields
          nomalizedQuery[key] = value;
        }

        break;
    }
  });

  return nomalizedQuery;
}

export async function getList(self, apiEndpoint, query) {
  const { error, data } = await apiGetList(apiEndpoint, removeJunkValue(self, query));

  if (error) {
    self.setState({
      query, // [!] fix onChange with 2rd 3th.. page
      error: true,
      success: false,
      messages: apiError2Messages(error),
      loading: false,
    });
  } else {
    self.setState({
      query, // [!] fix onChange with 2rd 3th.. page
      loading: false,
      objectList: data,
      prevObjectId: '',
      objectId: '',
      nextObjectId: '',
    });
  }
}

function mergeFieldOnChangeList(fieldDef, onChangeHandlerList) {
  if (fieldDef) {
    const onChangeHandler = fieldDef.onChange;

    if (_.isFunction(onChangeHandler)) {
      if (onChangeHandlerList.indexOf(onChangeHandler) < 0) {
        onChangeHandlerList.push(onChangeHandler);
      }
    }
  }
}

export async function onChange(self, event, data) {
  event.preventDefault();

  const currentState = self.state;

  const {
    isListComponent,
    query, object,
    model, pageLoad,
    modelName,
  } = currentState;

  const { name, value } = getInputValue(data);
  const changedFields = createDraft(isListComponent ? query : object);
  const splitedNameList = name.split('.'); // fieldName.index.subFieldName
  const onChangeHandlerList = []; // all handler called after related field changed

  if (splitedNameList.length < 3) { // single field or field with '$gt' / '$lt'
    changedFields[name] = value;

    mergeFieldOnChangeList(model[name], onChangeHandlerList);

    const selectedPageLoad = pageLoad[name];

    if (selectedPageLoad) { // Ref model is found => need to process related fields
      const { relatedFields } = selectedPageLoad;

      if (value) { // EU selected value
        const selectedItem = selectedPageLoad.data.find(item => item[selectedPageLoad.refKeyField] === value);

        if (selectedItem && relatedFields) {
          relatedFields.forEach((field) => {
            if (_.isObject(field)) {
              const { fromField, toField, toPageLoad } = field;

              if (toField) {
                changedFields[toField] = selectedItem[fromField];
                mergeFieldOnChangeList(model[toField], onChangeHandlerList);
              } else if (toPageLoad) {
                pageLoad[toPageLoad] = { // create new pageLoad for related field
                  ...pageLoad[toPageLoad],
                  data: selectedItem[fromField],
                };
              }
            } else {
              changedFields[field] = selectedItem[field];
              mergeFieldOnChangeList(model[field], onChangeHandlerList);
            }
          });
        }
      } else if (relatedFields) { // EU didn't select value
        relatedFields.forEach((field) => {
          if (_.isObject(field)) {
            const { toField } = field;
            const toFieldDef = model[toField];
            // console.log('field, toField, model[toField]', field, toField, model[toField]);

            changedFields[toField] = getDefaultValue(toFieldDef.type, toFieldDef.defaultValue);
            mergeFieldOnChangeList(model[toField], onChangeHandlerList);
          } else {
            const fieldDef = model[field];

            if (fieldDef) {
              changedFields[field] = getDefaultValue(fieldDef.type, fieldDef.defaultValue);
              mergeFieldOnChangeList(model[field], onChangeHandlerList);
            } else {
              console.error(`Field ${field} is NOT defined in model ${modelName}.`);
            }
          }
        });
      }
    }
  } else { // nested field
    const fieldName = splitedNameList[0];
    const index = splitedNameList[1];
    const subFieldName = splitedNameList[2];
    const fieldSubModel = model[fieldName] && model[fieldName].subModel ? model[fieldName].subModel : {};

    const changedItem = changedFields[fieldName][index];
    changedItem[subFieldName] = value;

    mergeFieldOnChangeList(fieldSubModel[subFieldName], onChangeHandlerList);

    // pageLoad[name] => nested instant search field with MANY pageLoad
    // pageLoad[`${fieldName}.${subFieldName}`] => nested instant search field with UNIQUE pageLoad
    const selectedPageLoad = (!_.isUndefined(pageLoad[name])) ? pageLoad[name] : pageLoad[`${fieldName}.${subFieldName}`];

    if (selectedPageLoad) { // Ref model is found
      const { relatedFields } = selectedPageLoad;

      if (value) { // EU selected value
        const selectedItem = selectedPageLoad.data ? selectedPageLoad.data.find(item => item[selectedPageLoad.refKeyField] === value) : undefined;

        if (selectedItem && relatedFields) {
          relatedFields.forEach((field) => {
            if (_.isObject(field)) { // exists { fromField: toField }
              const { fromField, toField, toPageLoad } = field;

              if (toField) {
                changedItem[toField] = selectedItem[fromField];
                mergeFieldOnChangeList(fieldSubModel[toField], onChangeHandlerList);
              } else if (toPageLoad) {
                pageLoad[`${fieldName}.${index}.${toPageLoad}`] = { // create new pageLoad for related field
                  ...pageLoad[`${fieldName}.${toPageLoad}`],
                  data: selectedItem[fromField],
                };
              }
            } else {
              changedItem[field] = selectedItem[field];
              mergeFieldOnChangeList(fieldSubModel[field], onChangeHandlerList);
            }
          });
        }
      } else if (relatedFields) { // EU didn't select value => clear realted field
        relatedFields.forEach((field) => {
          const fieldDef = fieldSubModel[subFieldName];

          if (_.isObject(field)) { // exists { fromField: toField }
            const { toField, toPageLoad } = field;

            if (toField) {
              changedItem[toField] = getDefaultValue(fieldDef.type, fieldDef.defaultValue);
              mergeFieldOnChangeList(fieldSubModel[toField], onChangeHandlerList);
            } else if (toPageLoad) {
              pageLoad[`${fieldName}.${index}.${toPageLoad}`] = {
                ...pageLoad[`${fieldName}.${toPageLoad}`],
                data: [],
              };
            }
          } else {
            changedItem[field] = getDefaultValue(fieldDef.type, fieldDef.defaultValue);
            mergeFieldOnChangeList(fieldSubModel[field], onChangeHandlerList);
          }
        });
      }
    }
  }

  let newState = finishDraft(changedFields);

  for (let i = 0; i < onChangeHandlerList.length; i += 1) {
    const func = onChangeHandlerList[i];
    // eslint-disable-next-line no-await-in-loop
    newState = await func(self, newState);
  }

  if (isListComponent) { // in list page
    self.setState({
      ...currentState,
      query: newState,
    });
  } else { // in form page
    self.setState({
      ...currentState,
      object: newState,
    });
  }
}

export async function onAddSubDocument(self, event, fieldName) {
  event.preventDefault();

  const currentState = self.state;

  const {
    isListComponent,
    query, object, model,
    pageLoad,
  } = currentState;

  const changedFields = createDraft(isListComponent ? query : object);
  const { subModel } = model[fieldName];
  const newSubDocumentIndex = changedFields[fieldName].length;
  changedFields[fieldName].unshift(getDefaultModelValue(subModel));

  // auto clone pageLoad to sub document pageLoad by its index
  Object.entries(subModel).forEach(([subFieldName, subFieldDef]) => {
    const { uniquePageLoad } = subFieldDef; // [!] ~Page[L]oad vs ~Page[l]oad is easy to make error

    if (uniquePageLoad) {
      pageLoad[`${fieldName}.${newSubDocumentIndex}.${subFieldName}`] = { ...pageLoad[`${fieldName}.${subFieldName}`] };
    }
  });

  const newState = finishDraft(changedFields);

  if (isListComponent) { // in list page
    self.setState({
      ...currentState,
      query: {
        ...query,
        ...newState,
      },
    });
  } else { // in form page
    self.setState({
      ...currentState,
      object: {
        ...object,
        ...newState,
      },
    });
  }
}

export async function onDeleteSubDocument(self, event, itemIndex) {
  event.preventDefault();

  const currentState = self.state;
  const { isListComponent, query, object } = currentState;
  const changedFields = createDraft(isListComponent ? query : object);
  const splitedNameList = itemIndex.split('.'); // fieldName.index

  const fieldName = splitedNameList[0];
  const index = splitedNameList[1];

  changedFields[fieldName].splice(index, 1);

  const newState = finishDraft(changedFields);

  if (isListComponent) { // in list page
    self.setState({
      ...currentState,
      query: {
        ...query,
        ...newState,
      },
    });
  } else { // in form page
    self.setState({
      ...currentState,
      object: {
        ...object,
        ...newState,
      },
    });
  }
}


// export function onDownloadFile(self, fileId) {
//   window.open(`${API_GATEWAY_URL}/v2/files/${fileId}`, '_blank');
// }

export async function onDownloadFile(self, fileId) {
  const token = getToken();
  const result = await axios({
    method: 'GET',
    responseType: 'arraybuffer',
    url: `${API_GATEWAY_URL}/v2/files/${fileId}`,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });

  if (result.data) {
    const { filename, contenttype } = result.headers;
    const blob = new Blob([result.data], { type: contenttype });
    FileSaver.saveAs(blob, filename);
  }

  return { data: result };
}

export async function checkLogin(self) {
  const token = getToken();

  if (!token) {
    window.location.href = `/login/${encodeURIComponent(window.location.href)}`;
  }

  const { status, handleReloginUserSuccess } = self.props;

  if (status !== 'authenticated') { //  || !user
    try {
      const data = {
        moduleCode: APP_CODE,
      };

      const result = await axios({
        method: 'POST',
        data,
        url: `${API_GATEWAY_URL}/v2/users/ping`,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const history = createBrowserHistory();
      const location = history.location.pathname;
      const userData = result.data.data;
      const { currentModuleId, moduleList, functionList } = userData;

      let currentFunctionId = '';
      let currentFunctionName = '';
      let currentFunctionUrl = '';
      let currentFunctionActionList = '';

      const secondSlashPos = location.indexOf('/', 1);
      const thirdSlashPos = secondSlashPos > -1 ? location.indexOf('/', secondSlashPos + 1) : -1;
      let requestFunctionUrl = thirdSlashPos > -1 ? location.substring(0, thirdSlashPos + 1) : location;

      if (requestFunctionUrl !== '') {
        for (let i = 0; i < functionList.length; i += 1) {
          const {
            functionId,
            functionName,
            functionUrl,
            functionActionList,
          } = { ...functionList[i] };

          let nomalizedFunctionUrl = `/${functionUrl}/`;

          while (nomalizedFunctionUrl.indexOf('//') > -1) { // remove repeate slash character
            nomalizedFunctionUrl = nomalizedFunctionUrl.replace('//', '/');
          }

          requestFunctionUrl = `${requestFunctionUrl}/`;

          while (requestFunctionUrl.indexOf('//') > -1) { // remove repeate slash character
            requestFunctionUrl = requestFunctionUrl.replace('//', '/');
          }

          if (nomalizedFunctionUrl.length > 2) {
            if (requestFunctionUrl === nomalizedFunctionUrl) {
              currentFunctionId = functionId;
              currentFunctionName = functionName;
              currentFunctionUrl = functionUrl;
              currentFunctionActionList = functionActionList;
              break;
            }
          }
        }
      }

      setFunctionId(currentFunctionId);

      handleReloginUserSuccess(
        userData,

        moduleList,
        currentModuleId,

        functionList,
        currentFunctionId,
        currentFunctionName,
        currentFunctionUrl,
        currentFunctionActionList,
      );
    } catch (error) {
      removeToken();
      window.location.href = '/login/';
      // console.log('error', error);
    }
  }
}
