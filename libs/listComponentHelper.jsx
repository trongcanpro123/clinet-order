/* eslint-disable no-await-in-loop */
/* eslint-disable no-shadow */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable no-param-reassign */
import React from 'react';
import async from 'async';
import _ from 'lodash';
import { Redirect } from 'react-router-dom';
import FileSaver from 'file-saver';
import pluralize from 'pluralize';

import { apiError2Messages } from './errorHelper';
import { apiGetList, apiCreate, apiDeleteById, apiUpdateById, apiDownloadList } from './apiHelper';
import { equalToId, getInputValue } from './commonHelper';
import { VALIDATE_FAILURE } from '../constants/config';
import { OPERATOR_SIGN, OPERATOR_REPLACER } from './constants/mongoOperator';
import { ACTION } from './constants/actionConstant';
import { API_RESERVED_KEY } from './constants/apiConstant';
import { parseServiceSwagger } from './swaggerHelper';

import {
  LOADING_STATE,
  checkLogin,
  getList, getLinkedObjects,
  onChange, onDownloadFile,
  removeJunkValue,
} from './componentHelper';

export const QUERY_SERVICE = 'v2/queries';

export const STATE_OPTION_LIST = [
  { text: 'Có hiệu lực', value: true },
  { text: 'Không có hiệu lực', value: false },
  { text: 'Tất cả', value: '' },
];

export const QUERY_AUTO_ADDED_FIELD = [
  'fields',
  'hiddenFields',
  'page',
  'itemsPerPage',
  'queryName',
  'isDefaultQuery',
  'sortBy',
  'groupBy',
];

async function onPageChange(self, target, navData) {
  target.preventDefault();
  self.setState(LOADING_STATE);

  const { apiEndpoint, query } = self.state;
  query.page = navData.children;

  await getList(self, apiEndpoint.read, query);
}

async function onItemsPerPageChange(self, target, data) {
  target.preventDefault();
  self.setState(LOADING_STATE);

  const { apiEndpoint, query } = self.state;

  query.page = 1;
  query.itemsPerPage = data.value;

  await getList(self, apiEndpoint.read, query);
}

async function onSearch(self, event) {
  event.preventDefault();
  self.setState(LOADING_STATE);

  const { apiEndpoint, query } = self.state;

  const newQuery = { // [?] query.page = 1 => MAKE ERROR
    ...query,
    page: 1,
  };

  await getList(self, apiEndpoint.read, newQuery);
}

async function onExport(self, event) {
  event.preventDefault();
  self.setState(LOADING_STATE);

  const { apiEndpoint, query } = self.state;
  const { data, error } = await apiDownloadList(apiEndpoint.read, 'export', removeJunkValue(self, query));

  if (error) {
    self.setState({
      error: true,
      messages: apiError2Messages(error),
      loading: false,
    });
  } else {
    self.setState({
      loading: false,
    });

    const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    FileSaver.saveAs(blob, 'export.xlsx');
  }
}

async function onSortBy(self, fieldName) {
  self.setState(LOADING_STATE);

  const { apiEndpoint, query } = self.state;
  const sortBy = query.sortBy ? query.sortBy.split('.') : [];
  const sortedField = sortBy[0] ? sortBy[0] : '';
  const sortDirection = sortBy[1] === 'desc' ? 'desc' : 'asc';

  query.page = 1; // reset to 1rst page when REsearch

  if (fieldName === sortedField) {
    query.sortBy = `${fieldName}.${sortDirection === 'desc' ? 'asc' : 'desc'}`;
  } else {
    query.sortBy = `${fieldName}.asc`;
  }

  await getList(self, apiEndpoint.read, query);
}

async function onSelectAllObjectList(self, event, data) {
  event.preventDefault();

  const { objectList } = self.state;
  const { value } = getInputValue(data);
  const selectedObjectList = [];

  if (value && objectList && objectList.length) {
    objectList.data.forEach((o) => {
      selectedObjectList.push(o._id);
    });
  }

  self.setState({
    selectedAll: value,
    selectedObjectList,
  });
}

async function onSelectObject(self, event, data) {
  event.preventDefault();

  const { selectedObjectList } = self.state;
  const { name, value } = getInputValue(data);
  const newSelectedObjectList = Array.from(selectedObjectList); // [!] NOT create new Array make NOT RE-render

  if (value) {
    newSelectedObjectList.push(name);
  } else {
    _.remove(newSelectedObjectList, id => equalToId(id, name));
  }

  self.setState({
    selectedObjectList: newSelectedObjectList,
  });
}

async function onResetQuery(self, event) {
  event.preventDefault();

  const { apiEndpoint, defaultQuery } = self.state;

  self.setState({
    ...LOADING_STATE,
    query: defaultQuery,
    selectedQueryId: '',
  });

  await getList(self, apiEndpoint.read, defaultQuery);
}

async function onObjectClick(self, objectId) {
  const {
    queryList, selectedQueryId,
    query, objectList,
    pageLoad,
  } = self.state;

  const { handleSaveQueryState } = self.props;
  const { prevObjectId, nextObjectId } = getLinkedObjects(objectId, objectList.data ? objectList.data : []);

  handleSaveQueryState(queryList, selectedQueryId, query, objectList, pageLoad, prevObjectId, objectId, nextObjectId); // dispatch saveQueryState action to save [query, objectList]

  self.setState({ // redirect to object detail form
    ...self.state,
    goToObject: true,

    prevObjectId,
    objectId,
    nextObjectId,
  });
}

async function onClickFirstObject(self) {
  const {
    queryList, selectedQueryId,
    query, objectList, objectId,
    pageLoad,
  } = self.state;

  if (!objectId) {
    const { handleSaveQueryState } = self.props;
    const objectListData = objectList.data;
    const newObjectId = objectListData && (objectListData.length > 0) ? objectListData[0]._id.toString() : '';
    const nextObjectId = objectListData && (objectListData.length > 1) ? objectListData[1]._id.toString() : '';

    handleSaveQueryState(queryList, selectedQueryId, query, objectList, pageLoad, '', newObjectId, nextObjectId); // dispatch saveQueryState action to save [query, objectList]

    self.setState({ // redirect to object detail form
      ...self.state,
      goToObject: true,

      prevObjectId: '',
      objectId: newObjectId,
      nextObjectId,
    });
  } else {
    self.setState({ // redirect to object detail form
      ...self.state,
      goToObject: true,
    });
  }
}

async function onClickFunctionRegister(self) {
  self.setState(LOADING_STATE);

  const {
    modelName, baseUrl,
    functionId, functionName, functionActionList,
  } = self.props;

  const functionUrl = baseUrl;
  const serviceNameList = [];
  const serviceActionList = [];
  const serviceList = [];
  const { object, query } = self.props.models;
  const { objectFields } = object;
  const objectFieldsArray = objectFields ? objectFields.split(',') : [];
  const pascalize = str => pluralize.singular(_.upperFirst(str.substring(str.indexOf('/') + 1))); // convert "v2/purchaseOrderRequisitions" => "PurchaseOrderRequisition"

  const convertQueryToRequestFieldList = (modelQuery) => {
    const requestFieldList = [];

    Object.entries(_.omit(modelQuery, API_RESERVED_KEY)).forEach(([key]) => {
      requestFieldList.push(key);
    });

    return requestFieldList.join(',');
  };

  serviceNameList.push(modelName);

  functionActionList.forEach((functionAction) => {
    const pascalizedServiceName = pascalize(modelName);

    switch (functionAction) {
      case ACTION.CAN_READ: {
        serviceActionList.push({
          modelName,
          actionCode: `get${pascalizedServiceName}List`,
          requestFieldList: objectFieldsArray,
          responseFieldList: objectFieldsArray,
        });

        serviceActionList.push({
          modelName,
          actionCode: `get${pascalizedServiceName}ById`,
          requestFieldList: objectFieldsArray,
          responseFieldList: objectFieldsArray,
        });

        break;
      }

      case ACTION.CAN_CREATE: {
        serviceActionList.push({
          modelName,
          actionCode: `create${pascalizedServiceName}`,
          requestFieldList: objectFieldsArray,
          responseFieldList: objectFieldsArray,
        });

        break;
      }

      case ACTION.CAN_UPDATE: {
        serviceActionList.push({
          modelName,
          actionCode: `update${pascalizedServiceName}ById`,
          requestFieldList: objectFieldsArray,
          responseFieldList: objectFieldsArray,
        });

        break;
      }

      case ACTION.CAN_DELETE: {
        serviceActionList.push({
          modelName,
          actionCode: `update${pascalizedServiceName}ById`,
          requestFieldList: [],
          responseFieldList: objectFieldsArray,
        });

        break;
      }

      default: {
        serviceActionList.push({
          modelName,
          actionCode: `${functionAction}${pascalizedServiceName}ById`,
          requestFieldList: objectFieldsArray,
          responseFieldList: objectFieldsArray,
        });

        break;
      }
    }
  });

  if (object && _.isArray(object.refModels)) {
    object.refModels.forEach((m) => {
      const { modelName, query } = m;
      const pascalizedServiceName = pascalize(modelName);

      if (serviceNameList.indexOf(modelName) < 0) {
        serviceNameList.push(modelName);

        serviceActionList.push({
          modelName,
          actionCode: `get${pascalizedServiceName}List`,
          requestFieldList: convertQueryToRequestFieldList(query),
          responseFieldList: query.fields,
        });
      }
    });
  }

  if (query && _.isArray(query.refModels)) {
    query.refModels.forEach((m) => {
      const { modelName, query } = m;
      const pascalizedServiceName = pascalize(modelName);

      if (serviceNameList.indexOf(modelName) < 0) {
        serviceNameList.push(modelName);

        serviceActionList.push({
          modelName,
          actionCode: `get${pascalizedServiceName}List`,
          requestFieldList: convertQueryToRequestFieldList(query),
          responseFieldList: query.fields, // [!] TODO: check query fields much more than object fields??
        });
      }
    });
  }

  // console.log('serviceNameList', serviceNameList);
  // console.log('serviceActionList', serviceActionList);

  // register service list
  for (let i = 0; i < serviceNameList.length; i += 1) {
    const serviceCode = serviceNameList[i];

    const query = {
      serviceCode,
      limit: 1,
      active: true,
      fields: '_id, serviceCode, serviceName, actionList, fieldList',
    };

    const getServiceResult = await apiGetList('v2/services', query);

    if (getServiceResult.error) {
      self.setState({
        loading: false,
        error: true,
        messages: apiError2Messages(getServiceResult.error),
      });

      return;
    }

    const service = getServiceResult.data.data[0];

    if (service) {
      serviceList.push(service);
    } else { // if (service)
      const swaggerResult = await parseServiceSwagger(serviceCode);

      if (swaggerResult.error) {
        self.setState({
          loading: false,
          error: true,
          messages: apiError2Messages(swaggerResult.error),
        });

        return;
      }

      const createdService = {
        serviceCode,
        serviceName: serviceCode,
        actionList: swaggerResult.actionList,
        fieldList: swaggerResult.fieldList,
        active: true,
      };

      const creationResult = await apiCreate('v2/services', createdService);

      if (creationResult.error) {
        self.setState({
          loading: false,
          error: true,
          messages: apiError2Messages(creationResult.error),
        });

        return;
      }

      serviceList.push(creationResult.data.data);
    } // if (service)
  } // for (let i = 0; i < serviceActionList.length; i += 1)

  // console.log('server ServiceList', serviceList);

  // register policy
  for (let i = 0; i < serviceActionList.length; i += 1) {
    const {
      modelName, actionCode,
      requestFieldList, responseFieldList,
    } = serviceActionList[i];

    const service = serviceList.find(s => s.serviceCode === modelName);
    const serviceId = service._id;

    const {
      serviceCode, serviceName,
      actionList, fieldList,
    } = service;

    const action = service.actionList.find(a => a.actionCode === actionCode);

    if (!action) {
      self.setState({
        loading: false,
        error: true,
        messages: `Tài nguyên ${modelName} không có hành động ${actionCode}!`,
      });

      return;
    }

    const actionId = action._id;
    const { path, method } = action;

    if (!action) {
      self.setState({
        loading: false,
        error: true,
        messages: `Tài nguyên ${modelName} hành động ${actionCode} không tìm được actionId!`,
      });

      return;
    }

    const query = {
      functionId,
      serviceId,
      actionId,

      limit: 1,
      active: true,
      fields: '_id, policyName',
    };

    const getPolicyResult = await apiGetList('v2/policies', query);

    // console.log('getPolicyResult', getPolicyResult);

    if (getPolicyResult.error) {
      self.setState({
        loading: false,
        error: true,
        messages: apiError2Messages(getPolicyResult.error),
      });

      return;
    }

    const policy = getPolicyResult.data.data[0];

    if (policy) {
      serviceList.push(service);
    } else { // if (service)
      const createdPolicy = {
        policyName: `${functionName} - ${actionCode}`,

        functionId,
        functionUrl,
        functionName,

        serviceId,
        serviceCode,
        serviceName,
        actionList,
        fieldList,

        userFeatureList: [],

        actionId,
        actionCode,
        path,
        method,

        fullRequestFieldList: fieldList,
        requestFieldList,
        requestExceptFieldList: [],
        allowedRequestFieldList: requestFieldList,

        fullResponseFieldList: fieldList,
        responseFieldList,
        responseExceptFieldList: [],
        allowedResponseFieldList: responseFieldList,

        recordFeatureList: [],

        active: true,
      };

      const creationResult = await apiCreate('v2/policies', createdPolicy);

      if (creationResult.error) {
        self.setState({
          loading: false,
          error: true,
          messages: apiError2Messages(creationResult.error),
        });

        return;
      }
    } // if (policy)
  } // for (let i = 0; i < serviceActionList.length; i += 1)

  self.setState({
    loading: false,
    success: true,
    messages: 'system:msg.create.success',
  });
}

async function onCreateNew(self) {
  const {
    queryList, selectedQueryId,
    query, objectList,
    pageLoad,
  } = self.state;

  const { handleSaveQueryState } = self.props;

  handleSaveQueryState(queryList, selectedQueryId, query, objectList, pageLoad, '', '0', ''); // dispatch saveQueryState action to save [query, objectList]

  self.setState({ // redirect to object detail form
    ...self.state,
    goToObject: true,

    prevObjectId: '',
    objectId: '0',
    nextObjectId: '',
  });
}

async function onSaveQuery(self) {
  const { query } = self.state;
  const { queryName, isDefaultQuery } = query;

  const {
    userId, userName, userFullName,
    functionId, baseUrl, functionName,
  } = self.props;

  const messages = [];

  if (!queryName) {
    messages.push({
      name: 'queryName',
      message: VALIDATE_FAILURE,
    });

    self.setState({
      error: true,
      messages,
    });

    return;
  }

  self.setState(LOADING_STATE);

  const nomalizedQuery = {};

  Object.entries(query).forEach(([key, value]) => {
    if (QUERY_AUTO_ADDED_FIELD.indexOf(key) < 0 && value) {
      nomalizedQuery[key.replace(OPERATOR_SIGN, OPERATOR_REPLACER)] = value;
    }
  });

  const newQuery = {
    userId,
    userName,
    userFullName,

    functionId,
    functionUrl: baseUrl,
    functionName,

    queryName,
    isDefaultQuery,
    query: nomalizedQuery,
  };

  const { error } = await apiCreate(QUERY_SERVICE, newQuery);

  if (error) {
    self.setState({
      loading: false,
      error: true,
      messages: apiError2Messages(error),
    });

    return;
  }

  const savedQueryList = await apiGetList(QUERY_SERVICE, {
    userId,
    functionId,
    fields: ['_id', 'queryName', 'isDefaultQuery', 'query'],
    active: true,
  });

  if (savedQueryList.error) {
    self.setState({
      loading: false,
      error: true,
      messages: apiError2Messages(savedQueryList.error),
    });

    return;
  }

  self.setState({
    loading: false,
    queryName: '',
    isDefaultQuery: false,
    queryList: savedQueryList.data.data,
  });
}

const onRedirect = (self) => {
  if (!self || !self.state) return null;

  const { baseUrl } = self.props;
  const { goToObject, objectId, objectUrlHandler } = self.state;
  const url = `${baseUrl}/${objectId}`.replace('//', '/');

  if (goToObject === true) {
    if (objectUrlHandler) {
      return objectUrlHandler(self);
    }

    return <Redirect to={url} />;
  }

  return null;
};

async function onSetQueryAsDefault(self, queryId, isDefaultQuery) {
  self.setState(LOADING_STATE);

  const { queryList } = self.state;
  let selectedQuery = {};

  if (isDefaultQuery) { // default => no set
    queryList.forEach((query) => {
      if (equalToId(query._id, queryId)) {
        query.isDefaultQuery = !isDefaultQuery;
        selectedQuery = query;
      }
    });
  } else { // not set => default
    queryList.forEach((query) => {
      if (equalToId(query._id, queryId)) {
        query.isDefaultQuery = !isDefaultQuery;
        selectedQuery = query;
      } else {
        query.isDefaultQuery = isDefaultQuery;
      }
    });
  }

  const { error } = await apiUpdateById(QUERY_SERVICE, selectedQuery); // TODO: auto unset other "isDefaultQuery" with same functionId & userId

  if (error) {
    self.setState({
      loading: false,
      error: true,
      messages: apiError2Messages(error),
    });
  } else {
    self.setState({
      loading: false,
      queryList,
      success: true,
      messages: 'system:msg.update.success',
    });
  }
}

async function onDeleteQuery(self, queryId) {
  self.setState(LOADING_STATE);

  const { error } = await apiDeleteById(QUERY_SERVICE, queryId); // TODO: delete NOT work

  if (error) {
    self.setState({
      loading: false,
      error: true,
      messages: apiError2Messages(error),
    });
  } else {
    self.setState({
      loading: false,
      queryList: self.state.queryList.filter(f => !equalToId(f._id, queryId)),
      success: true,
      messages: 'system:msg.delete.success',
    });
  }
}

async function onRunAsQuery(self, event, data) {
  event.preventDefault();

  const { query, queryList } = self.state;
  const { value } = getInputValue(data);

  if (value) {
    const selectedQuery = queryList.find(f => f._id, value);

    if (query) {
      self.setState({
        query: _.merge(query, selectedQuery.query),
        selectedQueryId: value,
      });
    }
  }
}

function onClickAdvancedSearch(self) {
  self.setState({
    showAdvancedSearch: !self.state.showAdvancedSearch,
  });
}

const getInitalStateFromProps = (props) => {
  const {
    modelName, models, apiEndpoint,
    pageLoad,
    queryList, selectedQueryId,
    query, objectList, defaultQuery,
    prevObjectId, objectId, nextObjectId,
    objectUrlHandler,
  } = props;

  const { model, refModels } = models.query;

  return {
    modelName,
    model,
    apiEndpoint,
    refModels,

    isListComponent: true,
    pageLoad: pageLoad || {},

    query: {
      ...query,
      queryName: '',
      isDefaultQuery: false,
    },

    defaultQuery,
    queryList,
    selectedQueryId,
    showAdvancedSearch: false,

    objectList,
    selectedAll: false,
    selectedObjectList: [],

    prevObjectId,

    goToObject: false,
    objectId,
    objectUrlHandler,

    nextObjectId,

    error: null,
    loading: false,
  };
};

// TODO: confirm before delete
export const deleteSelectedItem = async (self) => {
  self.setState(LOADING_STATE);

  const { selectedObjectList, apiEndpoint, query } = self.state;
  const errorList = [];

  selectedObjectList.forEach(async (objectId) => {
    const { error } = await apiDeleteById(apiEndpoint.delete, objectId);

    if (error) {
      errorList.push(error);
    }
  });

  if (errorList.length > 0) {
    self.setState({
      loading: false,
      error: true,
      messages: 'msg.delete.failure',
    });

    return;
  }

  // [?] query.page = 1 => MAKE ERROR
  const newQuery = {
    ...query,
    page: 1,
  };

  const { error, data } = await apiGetList(apiEndpoint.read, removeJunkValue(self, newQuery));

  if (error) {
    self.setState({
      query, // [!] fix onChange with 2rd 3th.. page
      error: true,
      success: false,
      messages: apiError2Messages(error),
      loading: false,
    });
  } else {
    self.setState({
      query, // [!] fix onChange with 2rd 3th.. page
      loading: false,
      objectList: data,
      prevObjectId: '',
      objectId: '',
      nextObjectId: '',
      selectedObjectList: [],
    });
  }
};

// TODO: approveSelectedItem, processSelectedItem, finishSelectedItem

export const initComponent = (self, props) => {
  self.state = getInitalStateFromProps(props);
  // console.log('initComponent > self.state', self.state);

  self.onChange = onChange.bind(self, self);
  self.onPageChange = onPageChange.bind(self, self);
  self.onItemsPerPageChange = onItemsPerPageChange.bind(self, self);
  self.onSearch = onSearch.bind(self, self);
  self.onExport = onExport.bind(self, self);
  self.onResetQuery = onResetQuery.bind(self, self);
  self.onObjectClick = onObjectClick.bind(self, self);
  self.onCreateNew = onCreateNew.bind(self, self);
  self.onClickFirstObject = onClickFirstObject.bind(self, self);
  self.onClickFunctionRegister = onClickFunctionRegister.bind(self, self);
  self.onClickAdvancedSearch = onClickAdvancedSearch.bind(self, self);
  self.onDownloadFile = onDownloadFile.bind(self, self);

  self.onSaveQuery = onSaveQuery.bind(self, self);
  self.onDeleteQuery = onDeleteQuery.bind(self, self);
  self.onSetQueryAsDefault = onSetQueryAsDefault.bind(self, self);
  self.onRunAsQuery = onRunAsQuery.bind(self, self);

  self.onSortBy = onSortBy.bind(self, self);
  self.onRedirect = onRedirect.bind(self, self);

  self.onSelectAllObjectList = onSelectAllObjectList.bind(self, self);
  self.onSelectObject = onSelectObject.bind(self, self);
};

export async function loadComponentData(self) {
  const {
    apiEndpoint,
    refModels,
    query,
  } = self.state;

  self.setState(LOADING_STATE);

  await checkLogin(self);

  const taskList = [];

  // TODO: not reload refModel if redirect from form view

  // [!] Cause of props change twice but component load once => CAN NOT save in state
  const { actionList, permmission } = self.props;

  if (permmission.canDelete) {
    actionList.push({
      actionCode: 'deleteSelectedItem',
      actionName: 'Xóa', // TODO: fix language hardcode
      actionHandler: deleteSelectedItem,
    });
  }

  // if (canApprove) {
  //   actionList.push({
  //     actionCode: 'approveSelectedItem',
  //     actionName: 'Phê duyệt', // TODO: fix language hardcode
  //     actionHandler: approveSelectedItem,
  //   });
  // }

  // if (canProcess) {
  //   actionList.push({
  //     actionCode: 'approveSelectedItem',
  //     actionName: 'Phê duyệt', // TODO: fix language hardcode
  //     actionHandler: processSelectedItem,
  //   });
  // }

  // if (canFinish) {
  //   actionList.push({
  //     actionCode: 'approveSelectedItem',
  //     actionName: 'Phê duyệt', // TODO: fix language hardcode
  //     actionHandler: finishSelectedItem,
  //   });
  // }

  refModels.forEach((tmpModel) => {
    if (tmpModel.autoPageLoad) {
      taskList.push(async (cb) => {
        const { error, data } = await apiGetList(tmpModel.modelName, removeJunkValue(self, tmpModel.query));

        if (error) {
          cb(error);
        } else {
          const pageLoad = {
            fieldName: tmpModel.fieldName,
            data: data.data,
          };

          cb(null, pageLoad);
        }
      });
    } // if (tmpModel.autoPageLoad)
  });

  await async.series(taskList, async (err, loadedRefData) => {
    if (err) {
      self.setState({
        error: true,
        messages: apiError2Messages(err),
        loading: false,
      });
    } else {
      let defaultQuery = null;
      let queryList = [];
      let mergedQuery = query;
      let selectedQueryId = '';

      const { userId, functionId } = self.props;

      if (userId && functionId) { // TODO: fix CAN NOT GET { userId, functionId } if app reload by F5
        const savedQueryList = await apiGetList(QUERY_SERVICE, {
          userId,
          functionId,
          fields: ['_id', 'queryName', 'isDefaultQuery', 'query'],
          active: true,
        });

        if (!savedQueryList.error) {
          queryList = savedQueryList.data.data;

          defaultQuery = queryList.find(f => f.isDefaultQuery);
          const nomilizedQuery = {};

          if (_.isObject(defaultQuery)) {
            Object.entries(defaultQuery.query).forEach(([key, value]) => {
              nomilizedQuery[key.replace(OPERATOR_REPLACER, OPERATOR_SIGN)] = value;
            });

            mergedQuery = _.merge(query, nomilizedQuery);
            selectedQueryId = defaultQuery._id;
          }
        }
      }

      const { error, data } = await apiGetList(apiEndpoint.read, removeJunkValue(self, mergedQuery));

      if (error) {
        self.setState({
          error: true,
          messages: apiError2Messages(error),
          loading: false,
        });
      } else {
        const pageLoad = {};

        if (_.isArray(loadedRefData)) { // convert array to object to easy access
          loadedRefData.forEach((refData) => {
            pageLoad[refData.fieldName] = refData;
          });
        }

        self.setState({
          loading: false,
          pageLoad,
          objectList: data,

          queryList,
          query: mergedQuery,
          selectedQueryId,

          prevObjectId: '',
          objectId: '',
          nextObjectId: '',
        });
      }
    }
  });

  window.scrollTo(0, 0); // scroll to Top
}

