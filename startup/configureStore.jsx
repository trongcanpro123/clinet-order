import { createStore, applyMiddleware, compose } from 'redux';
import promise from 'redux-promise';
import rootReducer from '../modules/rootReducer';

export default function configureStore(initialState) {
  const store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(promise)),
  );

  if (process.env.NODE_ENV !== 'production') {
    if (module.hot) {
      // Enable Webpack hot module replacement for reducers
      module.hot.accept('../modules/rootReducer', () => {
        const nextReducer = require('../modules/rootReducer');
        store.replaceReducer(nextReducer);
      });
    }
  }

  return store;
}
