import { connect } from 'react-redux';
import { action } from '../actions/masterPageAction';
import Layout from '../components/Layout';

function mapStateToProps(state) {
  const {
    user,
    account,
    currentCompanyId, currentCompanyName,
    moduleList, currentModuleId,
    functionList,
    currentFunctionId,
    currentFunctionName,
    currentFunctionUrl,
    currentFunctionActionList,
  } = state.system;

  return {
    user,
    account,

    currentCompanyId,
    currentCompanyName,

    moduleList,
    currentModuleId,

    functionList,
    currentFunctionId,
    currentFunctionName,
    currentFunctionUrl,
    currentFunctionActionList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    handleReloginUserSuccess: ( // not use directly but by model component
      user,

      moduleList,
      currentModuleId,

      functionList,
      currentFunctionId,
      currentFunctionName,
      currentFunctionUrl,
      currentFunctionActionList,
    ) => dispatch(action.reloginUserSuccess(
      user,

      moduleList,
      currentModuleId,

      functionList,
      currentFunctionId,
      currentFunctionName,
      currentFunctionUrl,
      currentFunctionActionList,
    )),

    handleChangeCurrentFunction: functionId => dispatch(action.changeCurrentFunction(functionId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
