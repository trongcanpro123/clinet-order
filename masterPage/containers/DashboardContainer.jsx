import { connect } from 'react-redux';
import { getDispatchProps } from '../../../libs/formContainerHelper'; // [!] FORM container helper
import Dashboard from '../components/Dashboard';
import { action } from '../actions/dashboardAction';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return getDispatchProps(dispatch, action);
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
