import { connect } from 'react-redux';
import { getStateProps, getDispatchProps } from '../../libs/formContainerHelper';
import UserForm from '../components/UserProfile';
import { action } from '../actions/userProfileAction';
import { model } from '../models/userProfileModel';

function mapStateToProps(state) {
  return getStateProps(state, 'system', model.stateName);
}

function mapDispatchToProps(dispatch) {
  return getDispatchProps(dispatch, action);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserForm);
