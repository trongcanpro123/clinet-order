import { DATA_TYPE } from '../../constants/dataType';

export const model = {
  stateName: 'user',
  modelName: 'v2/user',

  data: {
    _id: { type: DATA_TYPE.ID, defaultValue: '0', required: true },

    fullName: {
      type: DATA_TYPE.STRING,
      required: true,
      canQuery: true,
    },

    userName: {
      type: DATA_TYPE.STRING,
      required: true,
      canQuery: true,
    },

    password: { type: DATA_TYPE.STRING },
    verifyPassword: { type: DATA_TYPE.STRING },

    email: { type: DATA_TYPE.STRING, required: true },
    phone: { type: DATA_TYPE.STRING },
    bio: { type: DATA_TYPE.STRING },
    avatarFileId: { type: DATA_TYPE.ID },

    roleList: { type: DATA_TYPE.ARRAY },
    divisionList: { type: DATA_TYPE.ARRAY },
  },

  query: {
    fields: {
      type: DATA_TYPE.STRING,
      defaultValue: ['userName', 'fullName', 'email', 'phone'],
    },

    sortBy: {
      type: DATA_TYPE.STRING,
      defaultValue: 'userName.asc',
    },
  },
};

export default model;
