import { createActionType, createAction } from '../../libs/actionHelper';

export const ACTIONS = createActionType('USER_PROFILE');
export const action = createAction(ACTIONS);
