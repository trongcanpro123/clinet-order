import { createInitalState, getNewState } from '../../libs/reducerHelper';
import { model } from '../models/userProfileModel';
import { ACTIONS } from '../actions/userProfileAction';

const INITIAL_STATE = createInitalState(model);

export default function (state = INITIAL_STATE, action) {
  return getNewState(state, ACTIONS, action);
}
