/* eslint-disable react/forbid-prop-types */
import React, { Component, createRef } from 'react';
import { withRouter } from 'react-router';
import { Divider, Message, Form, Grid, Responsive } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import parseError from 'parse-error';
import { Trans } from 'react-i18next';

import { setFunctionId } from '../../libs/commonHelper';
import { WideMainMenu, LiteMainMenu } from './MainMenu';
import Footer from './Footer';
import SubMenu from './SubMenu';

const getErrorBox = (error) => {
  const MAX_ERROR_STACK_LENGTH = 600;
  const {
    message,
    line, filename,
    stack,
  } = parseError(error);

  return (
    <React.Fragment>
      <Message attached >
        <Trans i18nKey="system:msg.title.error" />
      </Message>
      <Form className="attached fluid segment">
        <div style={{ textAlign: 'left' }}>
          <b>Message</b>: {message} <br />
          <b>Line</b>: {line} <br />
          <b>Filename</b>: {filename} <br /><br />
          <b>Stack</b>: { stack.length < MAX_ERROR_STACK_LENGTH ? stack : `${stack.substring(0, MAX_ERROR_STACK_LENGTH)}...` } <br />
        </div>
      </Form>
      <Message attached="bottom" />
    </React.Fragment>
  );
};

class Layout extends Component {
  static get propTypes() {
    return {
      currentFunctionName: PropTypes.string.isRequired,
      currentFunctionUrl: PropTypes.string.isRequired,
      moduleList: PropTypes.array.isRequired,
      children: PropTypes.object.isRequired,

      handleChangeCurrentFunction: PropTypes.func.isRequired,
    };
  }

  constructor(props) {
    super(props);
    // console.log('MasterPage.constructor(props)');

    this.state = {
      error: null,
      isFunctionChanged: false,
    };
    this._isMounted = false; // Prevent React setState on unmounted Component
    this.contextRef = createRef();

    this.onChangeCurrentModule = this.onChangeCurrentModule.bind(this);
    this.onChangeCurrentFunction = this.onChangeCurrentFunction.bind(this);
  }

  async componentDidMount() {
    this._isMounted = true;
    // console.log('MasterPage.componentDidMount');
  }

  static getDerivedStateFromError(error) {
    return { error };
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  onChangeCurrentModule(moduleId) {
    const { moduleList } = this.props;
    const mod = moduleList.find(f => (f.moduleId.toString() === moduleId.toString()));

    window.location.href = `/${mod.moduleCode}/`;
  }

  async onChangeCurrentFunction(functionId) { // [!] remove async will make mal-function
    const { handleChangeCurrentFunction } = this.props;

    setFunctionId(functionId);
    await handleChangeCurrentFunction(functionId);

    this.setState({ isFunctionChanged: true });

    if (this._isMounted) {
      this.setState({ isFunctionChanged: false });
    }

    window.scrollTo(0, 0); // scroll to Top
  }

  render() {
    const { currentFunctionUrl, moduleList } = this.props;
    const { error, isFunctionChanged } = this.state;

    if (isFunctionChanged) {
      return (<Redirect to={currentFunctionUrl} />);
    }

    return (
      <React.Fragment>
        <Responsive maxWidth={1100} >
          <div
            id="liteMainMenu"
            style={{
              position: 'fixed',
              top: 0,
              left: 0,
              right: 0,
              height: 56,
              zIndex: 999,
              backgroundColor: '#fff',
              paddingLeft: 10,
            }}
          >
            <LiteMainMenu self={this} />
          </div>
        </Responsive>

        <Responsive minWidth={1101} >
          <div
            id="mainMenu"
            style={{
              position: 'fixed',
              top: 0,
              left: 0,
              right: 0,
              height: 56,
              minWidth: 1100,
              zIndex: 999,
              backgroundColor: '#fff',
              paddingLeft: 10,
            }}
          >
            <WideMainMenu self={this} />
          </div>
        </Responsive>

        <div
          id="subMenu"
          style={{
            position: 'fixed',
            display: 'flex',
            flexDirection: 'column',
            top: 58,
            bottom: 0,
            width: 213,
            left: 10,
            flex: 1,
            paddingLeft: 10,
            paddingRight: 10,
            overflowY: 'auto',
            borderRadius: '0.28571429rem',
            border: 'solid 1px #dedede',
          }}
        >
          <SubMenu self={this} />
        </div>

        <Responsive maxWidth={1250} >
          <div
            id="liteBody"
            style={{
              marginTop: 58,
              marginLeft: 233,
              minWidth: 550,
              paddingRight: 10,
            }}
          >
            { !error ? this.props.children : getErrorBox(error) }

            <Divider hidden />
            <Divider hidden />

            <Footer />
          </div>
        </Responsive>

        <Responsive minWidth={1251} >
          <div
            id="wideBody"
            style={{
              marginTop: 58,
              marginLeft: 233,
              maxWidth: 1680,
              paddingRight: 10,
            }}
          >
            <Grid columns={2}>
              <Grid.Column width={12} >
                <div>
                  { !error ? this.props.children : getErrorBox(error) }

                  <Divider hidden />
                  <Divider hidden />

                  <Footer />
                </div>
              </Grid.Column>
              <Grid.Column width={4}>
                <br /><br />&#34;Digital transformation is not about technology - it is about strategy and new ways of thinking.&#34;
              </Grid.Column>
            </Grid>
          </div>
        </Responsive>
      </React.Fragment>
    );
  }
}

export default withRouter(Layout);

