import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';

import { PRIMARY_COLOR } from '../../constants/config';
import { bindComponentToContext, listOptionsSelector } from '../../libs/componentHelper';
import { initComponent, loadComponentData } from '../../libs/formComponentHelper'; // [!] component FORM helper
import TextField from '../../userControls/TextField';
import TextAreaField from '../../userControls/TextAreaField';
import RadioField from '../../userControls/RadioField';
import SelectionField from '../../userControls/SelectionField';
import Footer from '../../userControls/Footer';
import FormTitle from '../../userControls/FormTitle';
import FormBody from '../../userControls/FormBody';
import FormTable from '../../userControls/FormTable';
import FormRow from '../../userControls/FormRow';
import FormActionList from '../../userControls/FormActionList';
import FormScrollArea from '../../userControls/FormScrollArea';
import TabSeparator from '../../userControls/TabSeparator';

import {
  roleListHeaderRender, roleListRenderSelector,
  divisionListHeaderRender, divisionListRenderSelector,
} from '../selectors/userProfileSelector';

const ThisContext = React.createContext({});

export default class UserProfile extends Component {
  constructor(props) {
    super(props);

    initComponent(this, props);
  }

  async componentDidMount() {
    await loadComponentData(this);
  }

  render() {
    console.log('this.state', this.state);

    const redirectInjection = this.onRedirect();
    if (redirectInjection) { return redirectInjection; }

    bindComponentToContext(
      [
        FormTitle, FormActionList,
        TextField, TextAreaField, RadioField,
        SelectionField, FormScrollArea, SelectionField,
        Footer,
      ],
      ThisContext,
    );

    const { pageLoad, object } = this.state;
    const roleListOptions = listOptionsSelector(pageLoad['roleList.roleId'], '_id', 'roleCode', 'roleName', 'roleList.roleId');

    return (
      <ThisContext.Provider value={{ self: this }}>
        <FormTitle />

        <FormBody>
          <FormRow>
            <TextField name="userName" />
            <TextField name="fullName" />
          </FormRow>

          <FormRow>
            <TextField name="password" type="password" />
            <TextField name="verifyPassword" type="password" />
          </FormRow>

          <FormRow>
            <TextField name="email" />
            <TextField name="phone" />
          </FormRow>

          <TextAreaField name="bio" />

          <FormScrollArea name="roleList">
            <FormTable>
              { roleListHeaderRender }
              { roleListRenderSelector(this, this.state, roleListOptions, 'roleList') }
            </FormTable>
          </FormScrollArea>

          <TabSeparator />

          <FormScrollArea name="divisionList" >
            <FormTable>
              { divisionListHeaderRender }
              { divisionListRenderSelector(this, object.divisionList, 'divisionList') }
            </FormTable>
          </FormScrollArea>

          <FormActionList />
        </FormBody>

        <Footer />
      </ThisContext.Provider>
    );
  }
}
