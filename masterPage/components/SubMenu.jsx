/* eslint-disable react/forbid-prop-types */
import React from 'react';
import { Menu } from 'semantic-ui-react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import createCachedSelector from 're-reselect';

const FunctionPropTypes = PropTypes.shape({
  functionId: PropTypes.string,
  functionName: PropTypes.string,
  functionUrl: PropTypes.string,
});

const SubFunctionRender = ({ func, currentFunctionId, onChangeCurrentFunction }) => {
  const { functionId, functionName, functionUrl } = func;

  return (
    <Menu.Item
      key={functionId}
      active={functionId === currentFunctionId}
    >
      <Link
        to={functionUrl || '#'}
        onClick={() => { onChangeCurrentFunction(functionId); return false; }}
      >
        { functionName }
      </Link>
    </Menu.Item>
  );
};

SubFunctionRender.propTypes = {
  currentFunctionId: PropTypes.string.isRequired,
  func: FunctionPropTypes.isRequired,

  onChangeCurrentFunction: PropTypes.func.isRequired,
};

const SubFunctionListRender = ({ subFunctionList, currentFunctionId, onChangeCurrentFunction }) => (
  <Menu.Menu >
    {subFunctionList.map(func => (
      <SubFunctionRender
        key={func.functionId}
        func={func}
        currentFunctionId={currentFunctionId}
        onChangeCurrentFunction={onChangeCurrentFunction}
      />))}
  </Menu.Menu>
);

SubFunctionListRender.propTypes = {
  currentFunctionId: PropTypes.string.isRequired,
  subFunctionList: PropTypes.arrayOf(FunctionPropTypes).isRequired,
  onChangeCurrentFunction: PropTypes.func.isRequired,
};

export const menuRenderSelector = createCachedSelector(
  self => self,
  (self, fullFunctionList) => fullFunctionList,
  (self, fullFunctionList, currentFunctionId) => currentFunctionId,

  (self, fullFunctionList, currentFunctionId) => {
    const { onChangeCurrentFunction } = self;
    const parentFunctionList = [];
    const liteFunctionList = Array.from(fullFunctionList);
    let parentFunctionId;

    for (let i = 0; i < liteFunctionList.length; i += 1) {
      let parentFunction;

      if (liteFunctionList[i]) {
        parentFunction = { ...liteFunctionList[i] };

        if (!parentFunction.functionParentId) {
          parentFunctionId = parentFunction.functionId;
          liteFunctionList[i] = undefined;

          parentFunction.functionList = [];

          for (let j = 0; j < liteFunctionList.length; j += 1) {
            const subFunction = { ...liteFunctionList[j] };

            if (subFunction.functionParentId === parentFunctionId) {
              liteFunctionList[j] = undefined;

              parentFunction.functionList.push(subFunction);
            }
          }

          parentFunctionList.push(parentFunction);
        }
      }
    }

    parentFunctionList.sort((f1, f2) => (f1.order - f2.order < 0));

    parentFunctionList.forEach((parentFunction) => {
      parentFunction.functionList.sort((f1, f2) => (f1.order - f2.order < 0));
    });

    return (
      <Menu borderless compact vertical fluid>
        {parentFunctionList.map((m) => {
            const { functionList, functionId, functionName } = m;

                if (functionList.length > 0) {
                  return (
                    <Menu.Item key={functionId}>
                      <Menu.Header>
                        {functionName}
                      </Menu.Header>

                      <Menu.Menu >
                        {functionList.map(func => (
                          <SubFunctionRender
                            key={func.functionId}
                            func={func}
                            currentFunctionId={currentFunctionId}
                            onChangeCurrentFunction={onChangeCurrentFunction}
                          />))}
                      </Menu.Menu>
                    </Menu.Item>
                  );
                }
                  return (
                    <SubFunctionRender
                      key={`${m.functionId}.sub`}
                      func={m}
                      currentFunctionId={currentFunctionId}
                      onChangeCurrentFunction={onChangeCurrentFunction}
                    />
                  );
              })}
      </Menu>);
  },
)((self, fullFunctionList, currentFunctionId, cacheName) => cacheName);

const SubMenu = ({ self }) => {
  const {
    functionList,
    currentFunctionId,
  } = self.props;

  return menuRenderSelector(self, functionList, currentFunctionId, 'masterPage.subMenu');
};

SubMenu.propTypes = {
  self: PropTypes.object.isRequired,
};

export default SubMenu;
