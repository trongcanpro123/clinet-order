import React from 'react';
import PropTypes from 'prop-types';
import { Menu, Dropdown, Image } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import createCachedSelector from 're-reselect';

import { PRIMARY_COLOR, APP_NAME, API_GATEWAY_URL } from '../../constants/config';
import { removeToken } from '../../libs/commonHelper';
import { imageAvatar } from '../../constants/htmlResource';

const trigger = (userName, avatarFileId) => {
  const imageSrc = (avatarFileId && avatarFileId !== '' && avatarFileId !== '0') ? `${API_GATEWAY_URL}/v2/files/${avatarFileId}` : imageAvatar;

  return (
    <span>
      <Image src={imageSrc} avatar bordered /> { userName }
    </span>);
};

const options = [
  {
    key: 'profile',
    value: 'profile',
    text: 'Thông tin tài khoản',
    onClick: () => { window.location.href = '/partner/profile/'; },
  },

  {
    key: 'logOut',
    value: 'logOut',
    text: 'Đăng xuất',
    onClick: () => {
      removeToken();
      window.location.href = '/login/';
    },
  },
];

const userProfile = (userName, avatarFileId) => (
  <Dropdown
    trigger={trigger(userName, avatarFileId)}
    options={options}
    pointing="top right"
  />
);

export const wideMenuRenderSelector = createCachedSelector(
  self => self,
  (self, user) => user,
  (self, user, fullModuleList) => fullModuleList,
  (self, user, fullModuleList, currentModuleId) => currentModuleId,

  (self, user, fullModuleList, currentModuleId) => {
    if (!fullModuleList) {
      return (
        <Menu stackable inverted color={PRIMARY_COLOR} className="clear_both">
          <Menu.Item>
            <Link to="/">
              <strong>{ APP_NAME }</strong>
            </Link>
          </Menu.Item>
        </Menu>
      );
    }

    const { onChangeCurrentModule } = self;
    const { userName, avatarFileId } = user;

    return (
      <Menu stackable inverted color={PRIMARY_COLOR} className="clear_both">
        <Menu.Item>
          <Link to="/">
            <strong>{ APP_NAME }</strong>
          </Link>
        </Menu.Item>

        {fullModuleList.map((m) => {
              const {
                moduleId, moduleCode, moduleName,
                moduleOrder,
              } = m;

              if (moduleOrder === 0) {
                return (<React.Fragment key={moduleId} />);
              }

              return (
                <Menu.Item key={moduleId} name={moduleName} active={currentModuleId === moduleId} >
                  <Link
                    to={`/${moduleCode}/`}
                    onClick={() => { onChangeCurrentModule(moduleId); return false; }}
                  >
                    {moduleName}
                  </Link>
                </Menu.Item>
              );
          })}

        <Menu.Menu position="right">
          <Menu.Item>
            { userProfile(userName, avatarFileId) }
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  },
)((self, user, fullModuleList, currentModuleId, cacheName) => cacheName);

export const WideMainMenu = ({ self }) => {
  const {
    user,
    moduleList,
    currentModuleId,
  } = self.props;

  return wideMenuRenderSelector(self, user, moduleList, currentModuleId, 'masterPage.mainMenu');
};

WideMainMenu.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  self: PropTypes.object.isRequired,
};

export const liteMenuRenderSelector = createCachedSelector(
  self => self,
  (self, user) => user,
  (self, user, fullModuleList) => fullModuleList,
  (self, user, fullModuleList, currentModuleId) => currentModuleId,

  (self, user, fullModuleList, currentModuleId) => {
    if (!fullModuleList) {
      return (
        <Menu compact>
          <Menu.Item>
            <Link to="/">
              <strong>{ APP_NAME }</strong>
            </Link>
          </Menu.Item>
        </Menu>
      );
    }

    const { onChangeCurrentModule } = self;
    const { userName } = user;
    const moduleListOptions = [];

    fullModuleList.forEach((mod) => {
      const { moduleId, moduleName, moduleOrder } = mod;

      if (moduleOrder) {
        const item = {};

        item.key = moduleId;
        item.value = moduleId;
        item.text = `Phân hệ ${moduleName}`; // [!] TODO: fix hard code

        moduleListOptions.push(item);
      }
    });

    return (
      <Menu inverted color={PRIMARY_COLOR} className="clear_both">
        <Menu.Item>
          <Link to="/">
            <strong>{ APP_NAME }</strong>
          </Link>
        </Menu.Item>

        <Menu.Item>
          <Dropdown
            value={currentModuleId}
            options={moduleListOptions}
            onChange={(e, d) => { e.preventDefault(); onChangeCurrentModule(d.value); console.log('d.value', d.value); }}
            pointing="top left"
          />
        </Menu.Item>

        <Menu.Menu position="right">
          <Menu.Item>
            { userProfile(userName) }
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  },
)((self, user, fullModuleList, currentModuleId, cacheName) => cacheName);

export const LiteMainMenu = ({ self }) => {
  const {
    user,
    moduleList,
    currentModuleId,
  } = self.props;

  return liteMenuRenderSelector(self, user, moduleList, currentModuleId, 'masterPage.liteMainMenu');
};

LiteMainMenu.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  self: PropTypes.object.isRequired,
};
