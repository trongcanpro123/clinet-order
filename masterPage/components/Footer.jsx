/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Container, Divider } from 'semantic-ui-react';
import { Translation } from 'react-i18next';

export default class Footer extends Component {
  render() {
    return (
      <Translation ns="system">
        {
          t => (<Container textAlign="center"> {t('app.copyright')} <Divider hidden /></Container>)
        }
      </Translation>);
  }
}
