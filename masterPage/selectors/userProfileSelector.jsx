/* eslint-disable no-unused-vars */
/* eslint-disable import/prefer-default-export */
import React from 'react';
import { Trans } from 'react-i18next';
import createCachedSelector from 're-reselect';
import { Table, Select, Button } from 'semantic-ui-react';
import _ from 'lodash';

import FormTableHeader from '../../userControls/FormTableHeader';

export const roleListHeaderRender = (
  <FormTableHeader>
    <Table.HeaderCell width={1} collapsing textAlign="center">
      <Trans i18nKey="index" />
    </Table.HeaderCell>
    <Table.HeaderCell width={13} textAlign="center">
      <Trans i18nKey="roleId" />
    </Table.HeaderCell>
  </FormTableHeader>
);

export const roleListRenderSelector = createCachedSelector(
  self => self,
  (self, state) => state.object.roleList,
  (self, state, roleListOptions) => roleListOptions,
  (self, roleList, roleListOptions) => {
    if (roleList) {
      const { onChange, onDeleteSubDocument } = self;

      return (
        <Table.Body>
          { roleList.map((product, index) =>
          // eslint-disable-next-line react/no-array-index-key
          (<Table.Row key={`roleList.${index}`}>
            <Table.Cell width={1} textAlign="center" verticalAlign="top">{index + 1}</Table.Cell>
            <Table.Cell width={13} textAlign="center" verticalAlign="top">
              <Select name={`roleList.${index}.roleId`} value={product.roleId} onChange={onChange} selection search fluid options={roleListOptions} />
            </Table.Cell>
          </Table.Row>))}
        </Table.Body>);
    }

    return (<Table.Body />);
  },
)((self, state, bomListOptions, cacheName) => cacheName);

export const divisionListHeaderRender = (
  <FormTableHeader>
    <Table.HeaderCell collapsing textAlign="center">
      <Trans i18nKey="index" />
    </Table.HeaderCell>

    <Table.HeaderCell textAlign="center">
      <Trans i18nKey="titleName" />
    </Table.HeaderCell>

    <Table.HeaderCell textAlign="center">
      <Trans i18nKey="companyName" />
    </Table.HeaderCell>

    <Table.HeaderCell textAlign="center">
      <Trans i18nKey="departmentName" />
    </Table.HeaderCell>

    <Table.HeaderCell textAlign="center">
      <Trans i18nKey="divisionName" />
    </Table.HeaderCell>

  </FormTableHeader>
);

export const divisionListRenderSelector = createCachedSelector(
  self => self,
  (self, divisionList) => divisionList,

  (self, divisionList) => {
    if (_.isArray(divisionList)) {
      return (
        <Table.Body>
          { divisionList.map((duty, index) =>
          // eslint-disable-next-line react/no-array-index-key
          (<Table.Row key={`divisionList.${index}`}>
            <Table.Cell textAlign="center" verticalAlign="middle">{index + 1}</Table.Cell>
            <Table.Cell textAlign="left" verticalAlign="middle">{duty.titleName}</Table.Cell>
            <Table.Cell textAlign="left" verticalAlign="middle">{duty.companyName}</Table.Cell>
            <Table.Cell textAlign="left" verticalAlign="middle">{duty.departmentName}</Table.Cell>
            <Table.Cell textAlign="left" verticalAlign="middle">{duty.divisionName}</Table.Cell>
          </Table.Row>))}
        </Table.Body>);
    }

    return (<Table.Body />);
  },
)((self, divisionList, cacheName) => cacheName);

