import React, { Component } from 'react';

import UserProfileContainer from '../containers/UserProfileContainer';
import MasterPage from './MasterPage';

export default class UserProfilePage extends Component {
  render() {
    return (
      <MasterPage>
          <UserProfileContainer id={this.props.match.params.id} />
      </MasterPage>
    );
  };
};
