import React from 'react';
import { Form, Message } from 'semantic-ui-react';
import { Translation } from 'react-i18next';
import MasterPage from './MasterPage';

const PageNotFound = () => (
  <Translation ns="system">
    {
      t => (
        <MasterPage>
          <React.Fragment>
            <Message attached header={t('msg.title.info').toUpperCase()} />
            <Form className="attached fluid segment">
              <div style={{ textAlign: 'left' }}>
                {`${t('msg.httpResponseCode.404')}.`}
              </div>
            </Form>
            <Message attached="bottom" />
          </React.Fragment>
        </MasterPage>)
    }
  </Translation>
);

export default PageNotFound;
