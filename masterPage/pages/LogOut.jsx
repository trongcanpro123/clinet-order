import React from 'react';
import { removeToken } from '../../libs/commonHelper';

const LogOut = () => {
  removeToken();
  window.location.href = '/login/';

  return (<React.Fragment />);
};

export default LogOut;
