/* eslint-disable react/prop-types */
import React from 'react';
import MasterPageContainer from '../containers/MasterPageContainer';

const MasterPage = props => (
  <MasterPageContainer >
    {props.children}
  </MasterPageContainer>
);

export default MasterPage;
