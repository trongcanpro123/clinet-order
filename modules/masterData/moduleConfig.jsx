export const moduleConfig = {
  moduleCode: 'masterData',
  moduleName: 'Master Data',
  description: 'Including all function define master data for whole system',
};

export default moduleConfig;
