import { createActionType, createAction } from '../../../libs/actionHelper';

export const ACTIONS = createActionType('DASHBOARD', []);
export const action = createAction(ACTIONS);
