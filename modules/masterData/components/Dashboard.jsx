import React, { Component } from 'react';
import { Table, Grid, Message } from 'semantic-ui-react';
import RC2 from 'react-chartjs2';

import { PRIMARY_COLOR } from '../../../constants/config';
import { checkLogin } from '../../../libs/componentHelper';

export default class Dashboard extends Component {
  async componentDidMount() {
    await checkLogin(this);
  }

  render() {
    const jobCount = {
      labels: ['Tháng 6', 'Tháng 7', 'Tháng 9', 'Tháng 10', 'Tháng 11'],
      datasets: [
        {
          label: 'Doanh số (tỷ đồng)',
          backgroundColor: ['#f07328', '#d47b2d', '#948f38', '#4fa544', '#24b24b'],
          borderWidth: 1,
          data: [199, 310, 403, 501, 610],
        },
      ],
    };

    const processingJobCount = {
      labels: ['Tháng 6', 'Tháng 7', 'Tháng 9', 'Tháng 10'],
      datasets: [
        {
          label: 'Tồn kho',
          backgroundColor: ['#f07328', '#d47b2d', '#948f38', '#4fa544', '#24b24b'],
          data: [500, 150, 200, 30, 110],
        },
      ],
    };

    return (
      <React.Fragment>
        <Table striped selectable compact celled color={PRIMARY_COLOR} >
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={1} textAlign="center">STT</Table.HeaderCell>
              <Table.HeaderCell width={4} textAlign="center">Thông báo</Table.HeaderCell>
              <Table.HeaderCell textAlign="center">Nội dung thông báo</Table.HeaderCell>
              <Table.HeaderCell width={2} textAlign="center">Người gửi</Table.HeaderCell>
              <Table.HeaderCell width={2} textAlign="center">Thời gian</Table.HeaderCell>
            </Table.Row>

            <Table.Row>
              <Table.Cell textAlign="center">1</Table.Cell>
              <Table.Cell textAlign="left">SFJ Insight ver#0.1</Table.Cell>
              <Table.Cell textAlign="left">Tính năng cho PM & NVKD</Table.Cell>
              <Table.Cell textAlign="center">anhdn4</Table.Cell>
              <Table.Cell textAlign="center">12/06/2018</Table.Cell>
            </Table.Row>

          </Table.Header>
          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell colSpan="8" />
            </Table.Row>
          </Table.Footer>
        </Table>

        <Message attached header="Dashboard" />

        <Grid divided="vertically" className="attached fluid segment">
          <Grid.Row columns={2}>
            <Grid.Column>
              <RC2 data={jobCount} type="bar" />
            </Grid.Column>
            <Grid.Column>
              <RC2 data={processingJobCount} type="doughnut" />
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Message attached="bottom" />

      </React.Fragment>
    );
  }
}