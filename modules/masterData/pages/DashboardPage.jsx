import React from 'react';
import DashboardContainer from '../containers/DashboardContainer';
import MasterPage from '../../../masterPage/pages/MasterPage';

const DashboardPage = () => (
  <MasterPage>
    <DashboardContainer />
  </MasterPage>
);

export default DashboardPage;
