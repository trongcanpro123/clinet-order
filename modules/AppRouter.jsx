import React from 'react';
import { BrowserRouter as Router, Route, BrowserHistory, Switch, Redirect } from 'react-router-dom';

import PageNotFound from '../masterPage/pages/PageNotFound';
import LogOut from '../masterPage/pages/LogOut';
import DashboardPage from './masterData/pages/DashboardPage';
import OrderListPage from './order/pages/OrderListPage';
import OrderFormPage from './order/pages/OrderFormPage';

const AppRouter = () => (
  <Router history={BrowserHistory} >
    <Switch>
      <Route exact path="/order/" render={() => <Redirect to="/order/dashboard/" />} />
      <Route path="/logout" component={LogOut} />
      <Route exact path="/order/dashboard/" component={DashboardPage} />
      <Route exact path="/order/s_order/" component={OrderListPage} />
      <Route path="/order/s_order/:id" component={OrderFormPage} />
      <Route path="*" exact component={PageNotFound} />
    </Switch>
  </Router>
);

export default AppRouter;
