import { combineReducers } from 'redux';

import masterPageReducer from '../masterPage/reducers/masterPageReducer';
import orderReducer from './order/reducers/orderReducer';
const rootReducer = combineReducers({
  system: masterPageReducer,
  order_s_orders: orderReducer,
});

export default rootReducer;

