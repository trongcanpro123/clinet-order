import { connect } from 'react-redux';
import { getStateProps, getDispatchProps } from '../../../libs/formContainerHelper';
import { moduleConfig } from '../moduleConfig';
import OrderForm from '../components/OrderForm';
import { action } from '../actions/orderAction';
import { model } from '../models/orderModel';

function mapStateToProps(state) {
  return getStateProps(state, moduleConfig.moduleCode, model.stateName);
}

function mapDispatchToProps(dispatch) {
  return getDispatchProps(dispatch, action);
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderForm);
