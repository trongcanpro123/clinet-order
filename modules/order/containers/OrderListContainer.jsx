import { connect } from 'react-redux';
import { getStateProps, getDispatchProps } from '../../../libs/listContainerHelper';
import { moduleConfig } from '../moduleConfig';
import OrderList from '../components/OrderList';
import { action } from '../actions/orderAction';
import { model } from '../models/orderModel';

function mapStateToProps(state) {
  return getStateProps(state, moduleConfig.moduleCode, model.stateName);
}

function mapDispatchToProps(dispatch) {
  return getDispatchProps(dispatch, action);
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
