/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable arrow-parens */
import React, { Component } from "react";
import {
  Message,
  Container,
  Step,
  FormField,
  TextArea,
  Button,
} from "semantic-ui-react";
import NumberFormat from "react-number-format";

import {
  bindComponentToContext,
  listOptionsSelector,
} from "../../../libs/componentHelper";
import {
  initComponent,
  loadComponentData,
} from "../../../libs/formComponentHelper"; // [!] component FORM helper
import TextField from "../../../userControls/TextField";
import SelectionField from "../../../userControls/SelectionField";
import Footer from "../../../userControls/Footer";
import FormTitle from "../../../userControls/FormTitle";
import FormBody from "../../../userControls/FormBody";
import FormRow from "../../../userControls/FormRow";
import FormActionList from "../../../userControls/FormActionList";
import EmptyField from "../../../userControls/EmptyField";
import FormScrollArea from "../../../userControls/FormScrollArea";
import FormActionContainer from "../../../userControls/FormActionContainer";
import FormActionGoBack from "../../../userControls/FormActionGoBack";
import FormAction from "../../../userControls/FormAction";
import DateField from "../../../userControls/DateField";
import FormTable from "../../../userControls/FormTable";
import FileUploader from "../../../userControls/FileUploader";
import FormWorkFlow from "../../../userControls/FormWorkFlow";
import TabSeparator from "../../../userControls/TabSeparator";
import RadioField from "../../../userControls/RadioField";
import { PRIMARY_COLOR } from "../../../constants/config";
import TextAreaField from "../../../userControls/TextAreaField";
// import { onPriceChange } from '../functions/orderFormFunction';
// import { createDraft, finishDraft } from 'immer';
import {
  onProductSearchChange,
  getLicence,
} from "../functions/orderFormFunction";

import { ORDERSTATE, ORDER_STATE } from "../constants/orderStateConstants";
import {
  orderListOptionsHeaderRender,
  orderListRenderSelector,
  getTotalPrice,
  orderListHeaderRender,
  orderListProductRenderSelector,
} from "../selectors/orderSelector";

const ThisContext = React.createContext({});

export default class OrderForm extends Component {
  constructor(props) {
    super(props);

    initComponent(this, props);
    // this.getSalePrice = getSalePrice.bind(this);
    // this.onPriceChange = onPriceChange.bind(this, this);
    this.onProductSearchChange = onProductSearchChange.bind(this, this);
    this.getLicence = getLicence.bind(this, this);
  }

  async componentDidMount() {
    await loadComponentData(this);

    bindComponentToContext(
      [
        FormTitle,
        FormActionList,
        FormScrollArea,
        RadioField,
        EmptyField,
        FormField,
        FormTable,
        TextAreaField,
        TextField,
        SelectionField,
        DateField,
        FileUploader,
        Footer,
        TextArea,
        FormWorkFlow,
        TabSeparator,
        FormActionContainer,
        FormActionGoBack,
        FormAction,
      ],
      ThisContext
    );
  }

  render() {
    const { object, pageLoad } = this.state;
    const redirectInjection = this.onRedirect();
    if (redirectInjection) {
      return redirectInjection;
    }

    const customerList = listOptionsSelector(
      pageLoad.customerId,
      "_id",
      "customerName",
      "customerOracleCode",
      "customerList"
    );

    return (
      <ThisContext.Provider value={{ self: this }}>
        <FormTitle />

        <FormBody>
          <Message attached>
            <Container textAlign="left">
              <Step.Group
                items={ORDERSTATE(object.orderState)}
                size="tiny"
                fluid
              />
            </Container>
          </Message>
          <TabSeparator />

          <FormRow>
            <TextField name="orderNumber" readOnly />
            <DateField name="createdAt" />
          </FormRow>

          <FormRow>
            <SelectionField name="customerId" options={customerList} />
          </FormRow>

          <FormRow>
            <TextField name="salesteamName" readOnly />
            <TextField name="salesmanName" readOnly />
          </FormRow>

          <FormRow>
            <TextField name="paymentTerm" readOnly />
            <EmptyField />
          </FormRow>

          <FormRow>
            <TextField name="billToName" readOnly />
          </FormRow>
          <FormRow>
            <TextField name="shipToName" readOnly />
          </FormRow>
          <FormScrollArea name="orderList">
            <FormTable>
              {orderListOptionsHeaderRender}
              {orderListRenderSelector(
                this,
                object.orderLineAll,
                pageLoad,
                "orderLineAll"
              )}
            </FormTable>
          </FormScrollArea>
          <Button
            type="button"
            icon="add"
            color={PRIMARY_COLOR}
            onClick={(e) => this.onAddSubDocument(e, "orderLineAll")}
            size="mini"
          />
          <div
            style={{ textAlign: "right", fontWeight: "bold", marginTop: 10 }}
          >
            Thành tiền: &nbsp;
            <NumberFormat
              value={getTotalPrice(object.orderLineAll, "totalPrice")}
              thousandSeparator="."
              decimalSeparator=","
              displayType="text"
            />
            {object.currencyCode}
          </div>
          <FormActionList />
          <TabSeparator />
          <TextAreaField name="note" />
          <React.Fragment>
            <FormActionContainer>
              {object.orderState === ORDER_STATE.DONE && (
                <FormAction name="btn_getLicence" onClick={this.getLicence} />
              )}
              <FormActionGoBack />
            </FormActionContainer>
          </React.Fragment>
          <FormActionList />
        </FormBody>

        <Footer />
      </ThisContext.Provider>
    );
  }
}
