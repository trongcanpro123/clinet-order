import React, { Component } from 'react';

import { initComponent, loadComponentData, STATE_OPTION_LIST } from '../../../libs/listComponentHelper'; // [!] component LIST helper
import { bindComponentToContext, listOptionsSelector } from '../../../libs/componentHelper';
import TextField from '../../../userControls/TextField';
import SelectionField from '../../../userControls/SelectionField';
import Footer from '../../../userControls/Footer';
import ListTitle from '../../../userControls/ListTitle';
import ListBody from '../../../userControls/ListBody';
import ListRow from '../../../userControls/ListRow';
import ListActionList from '../../../userControls/ListActionList';
import ListSearchResult from '../../../userControls/ListSearchResult';
import DateField from '../../../userControls/DateField';

// import DateTimeHour from '../subcomponents/DateTimeHour';
import EmptyField from '../../../userControls/EmptyField';
import ListNavigator from '../../../userControls/ListNavigator';
import ListActionContainer from '../../../userControls/ListActionContainer';
import { ORDER_STATE_OPTIONS } from '../constants/renderSetting';
import ListActionCreate from '../../../userControls/ListActionCreate';
import ListActionSearchAll from '../../../userControls/ListActionSearchAll';
import ListActionSearch from '../../../userControls/ListActionSearch';
import ListActionExport from '../../../userControls/ListActionExport';

const ThisContext = React.createContext({});

export default class OrderList extends Component {
  constructor(props) {
    super(props);

    initComponent(this, props);
  }

  async componentDidMount() {
    await loadComponentData(this);
  }

  render() {
    const redirectInjection = this.onRedirect();
    if (redirectInjection) { return redirectInjection; }

    bindComponentToContext(
      [
        ListTitle, ListBody, ListActionList, ListNavigator, ListActionContainer,
        TextField, DateField, SelectionField, Footer, ListRow, ListActionSearch, ListActionSearchAll, ListActionCreate, ListActionExport,
        ListSearchResult,
      ],
      ThisContext,
    );

    return (
      <ThisContext.Provider value={{ self: this }}>
        <ListTitle />
        <ListBody>
          <ListRow>
            <TextField name="orderNumber" />
            <SelectionField name="orderState" options={ORDER_STATE_OPTIONS} />
          </ListRow>
          <ListRow widths="equal">
            <DateField name="createdAt.$gte" />
            <DateField name="createdAt.$lte" />
          </ListRow>
          <ListRow>
            <TextField name="customerName" />
          </ListRow>
          <ListRow>
            <TextField name="productName" />
          </ListRow>
          <ListActionContainer>
            <ListActionSearch />
            <ListActionSearchAll />
            <ListActionCreate />
            <ListActionExport />
          </ListActionContainer>
        </ListBody>
        <Footer />
        <ListSearchResult keyField="orderNumber" />
      </ThisContext.Provider >
    );
  }
}
