import React from 'react';
import OrderListContainer from '../containers/OrderListContainer';
import MasterPage from '../../../masterPage/pages/MasterPage';

const OrderListPage = () => (
  <MasterPage>
    <OrderListContainer />
  </MasterPage>
);

export default OrderListPage;
