/* eslint-disable react/prop-types */
import React from 'react';

import OrderFormContainer from '../containers/OrderFormContainer';
import MasterPage from '../../../masterPage/pages/MasterPage';

const OrderFormPage = props => (
  <MasterPage>
    <OrderFormContainer id={props.match.params.id} />
  </MasterPage>
);

export default OrderFormPage;
