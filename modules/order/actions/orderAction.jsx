import { createActionType, createAction } from '../../../libs/actionHelper';

export const ACTIONS = createActionType('S_ORDER', []);
export const action = createAction(ACTIONS);

