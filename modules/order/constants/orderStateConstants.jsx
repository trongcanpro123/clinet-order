export const DRAFT = "draft";
export const CONFIRMED = "confirmed";
export const DELIVERED = "delivered";
export const CANCELLED = "cancelled";
export const ORDER_STATE = {
  DRAFT,
  CONFIRMED,
  DELIVERED,
  CANCELLED
};
export const ORDER_STATE_LIST = {
  [DRAFT]: "Dự thảo",
  [CONFIRMED]: "Đã tạo",
  [DELIVERED]: "Đã giao",
  [CANCELLED]: "Đã hủy"
};

export const ORDERSTATE = (currentState) => [
  {
    key: DRAFT,
    title: ORDER_STATE_LIST[DRAFT],
    active: currentState === DRAFT,
    completed: currentState !== DRAFT,
  },
  {
    key: CONFIRMED,
    title: ORDER_STATE_LIST[CONFIRMED],
    active: currentState === CONFIRMED,
    completed: currentState !== CONFIRMED,
  },
  {
    key: DELIVERED,
    title: ORDER_STATE_LIST[DELIVERED],
    active: currentState === DELIVERED,
    completed: currentState !== DELIVERED,
  },
  {
    key: CANCELLED,
    title: ORDER_STATE_LIST[CANCELLED],
    active: currentState === CANCELLED,
    completed: currentState !== CANCELLED,
  },
];
