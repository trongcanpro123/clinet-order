import React from 'react';
import { Trans } from 'react-i18next';
import createCachedSelector from 're-reselect';
import { Table, Button, TextArea, Input, Form, Select, TableBody, TabPane } from 'semantic-ui-react';
import _ from 'lodash';
import FormTableHeader from '../../../userControls/FormTableHeader';
import SelectionField from '../../../userControls/SelectionField';
import TextField from '../../../userControls/TextField';
import { bindComponentToContext, listOptionsSelector } from '../../../libs/componentHelper';
// import NumberField from '../../../../csp/userControls/NumberField';
// import InstantSearchField from '../../../userControls/InstantSearchField';
// import { listOptionsSelector } from '../../../libs/componentHelper';

export const orderListOptionsHeaderRender = (
  <FormTableHeader>
    <Table.HeaderCell collapsing textAlign="center">
      <Trans i18nKey="index" />
    </Table.HeaderCell>
    <Table.HeaderCell textAlign="center">
      <Trans i18nKey="productId" />
    </Table.HeaderCell>
    <Table.HeaderCell textAlign="center">
      <Trans i18nKey="productName" />
    </Table.HeaderCell>
    <Table.HeaderCell textAlign="center">
      <Trans i18nKey="quantity" />
    </Table.HeaderCell>
    <Table.HeaderCell textAlign="center">
      <Trans i18nKey="saleprice" />
    </Table.HeaderCell>
    <Table.HeaderCell collapsing />
  </FormTableHeader>
);

export const orderListRenderSelector = createCachedSelector(
  (self, orderLineAll, pageLoad) => self,
  (self, orderLineAll, pageLoad) => orderLineAll,
  (self, orderLineAll, pageLoad) => pageLoad,
  (self, orderLineAll, pageLoad) => {
    if (pageLoad['orderLineAll.productId']) {
      const { onDeleteSubDocument, onProductSearchChange } = self;
      const productList = listOptionsSelector(pageLoad['orderLineAll.productId'], '_id', 'oracleCode', 'productName', 'productList');
      console.log('pageLoad', pageLoad);
      console.log('productList', productList);
      return (
        <Table.Body >
          {orderLineAll.map((product, index) => (
            <Table.Row key={`orderLineAll.${index}`}>
              <Table.Cell textAlign="center" verticalAlign="middle">{index + 1}</Table.Cell>
              <Table.Cell textAlign="center" verticalAlign="middle">
                <SelectionField name={`orderLineAll.${index}.productId`} options={productList} label={false} />
              </Table.Cell>
              <Table.Cell>
                <TextField name={`orderLineAll.${index}.productName`} label={false} size="tiny" />
              </Table.Cell>
              <Table.Cell>
                <TextField name={`orderLineAll.${index}.quantity`} label={false} size="tiny" />
              </Table.Cell>
              <Table.Cell>
                <TextField name={`orderLineAll.${index}.unitPrice`} label={false} readOnly />
              </Table.Cell>
              <Table.Cell textAlign="center" verticalAlign="middle">
                <Button type="button" icon="delete" primary onClick={(e) => { onDeleteSubDocument(e, `orderLineAll.${index}`); }} size="mini" />
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body >
      );
    }
    return (
      <Table.Body />
    );
  },
)((self, orderLineAll, productList, cacheName) => cacheName);

// export const orderListOptionsHeaderRender = (
//   <FormTableHeader>
//     <Table.HeaderCell collapsing textAlign="center">
//       <Trans i18nKey="index" />
//     </Table.HeaderCell>
//     <Table.HeaderCell textAlign="center" style={{ minWidth: 450 }}>
//       <Trans i18nKey="productId" />
//     </Table.HeaderCell>
//     <Table.HeaderCell collapsing />
//   </FormTableHeader>
// );
// export const orderListHeaderRender = (
//   <FormTableHeader>
//     <Table.HeaderCell collapsing textAlign="center">
//       <Trans i18nKey="index" />
//     </Table.HeaderCell>
//     <Table.HeaderCell textAlign="center">
//       <Trans i18nKey="productId" />
//     </Table.HeaderCell>
//     <Table.HeaderCell collapsing />
//   </FormTableHeader>
// );

// export const orderListProductRenderSelector = createCachedSelector(
//   (self, pageLoad, orderLineAll) => self,
//   (self, pageLoad, orderLineAll) => pageLoad,
//   (self, pageLoad, orderLineAll) => orderLineAll,
//   (self, pageLoad, orderLineAll) => {
//     if (orderLineAll) {
//       const { onDeleteSubDocument, onProductSearchChange, state } = self;

//       return (
//         <Table.Body>

//           {orderLineAll.map((permission, index) => {
//                       const lineKey = `orderLineAll.${index}`;
//                       const cellKey = `${lineKey}.orderLineAll`;
//                       console.log("CanNT3: cellKey", cellKey)
//                       const productListOptions = listOptionsSelector(state.pageLoad[cellKey], '_id', 'oracleCode', 'productName', 'productId');
//                       console.log("CanNT3: productListOptions", productListOptions)
//                       return (
//                         <Table.Row key={`orderLineAll.${index}`}>
//                           <Table.Cell>{index + 1}</Table.Cell>
//                           <Table.Cell textAlign="center" verticalAlign="middle">
//                             <InstantSearchField name={cellKey} options={productListOptions} label={false} onSearchChange={(e, { searchQuery }) => onProductSearchChange(e, searchQuery, cellKey)} />
//                           </Table.Cell>
//                           <Table.Cell textAlign="center" verticalAlign="middle">
//                             <Button type="button" icon="delete" primary onClick={(e) => { onDeleteSubDocument(e, `orderLineAll.${index}`); }} size="mini" />
//                           </Table.Cell>
//                         </Table.Row>
//                       );
//                   })}
//         </Table.Body>
//       );
//     }
//     return (
//       <Table.Body />
//     );
//   },
// )((self, pageLoad, orderLineAll, cacheName) => cacheName);

// export const orderListRenderSelector = createCachedSelector(
//   (self, pageLoad, orderLineAll ) => self,
//   (self, pageLoad, orderLineAll ) => pageLoad,
//   (self, pageLoad, orderLineAll ) => orderLineAll,
//   (self, pageLoad, orderLineAll ) => {
//     if (orderLineAll) {
//       console.log("CanNT3: orderLineAll", orderLineAll)
//       const {
//         onDeleteSubDocument, onProductSearchChange, state,
//       } = self;
//       const { object, pageLoad } = state;
//       return (
//         <Table.Body >
//           {orderLineAll.map((product, index) => {
//                       const lineKey = `orderLineAll.${index}`;
//                       const cellKey = `${lineKey}.productId`;
//                       const productListOptions = listOptionsSelector(pageLoad[cellKey], '_id', 'productName', 'offerId', cellKey);
//                       return (
//                         <Table.Row key={lineKey}>
//                           <Table.Cell textAlign="center" verticalAlign="middle">{index + 1}</Table.Cell>
//                           <Table.Cell textAlign="center" verticalAlign="middle">
//                             <InstantSearchField name={`${lineKey}.productId`} options={productListOptions} style={{ maxWidth: 500 }} onSearchChange={(e, { searchQuery }) => onProductSearchChange(e, searchQuery, cellKey)} label={false} />
//                           </Table.Cell>
//                           <Table.Cell textAlign="center" verticalAlign="middle">
//                             <Button type="button" icon="delete" primary onClick={(e) => { onDeleteSubDocument(e, `${lineKey}`); }} size="mini" />
//                           </Table.Cell>
//                         </Table.Row>);
//                   })}
//         </Table.Body >
//       );
//     }
//     return (
//       <Table.Body />
//     );
//   },
// )((self, pageLoad, orderLineAll, handleStatus, cacheName) => cacheName);

export const getTotalPrice = createCachedSelector(
  orderLineAll => orderLineAll,

  (orderLineAll) => {
    console.log('CanNT3: orderLineAll', orderLineAll);
    let total = 0;

    if (_.isArray(orderLineAll)) {
      for (let i = 0; i < orderLineAll.length; i += 1) {
        const line = orderLineAll[i];

        if (line.quantity && line.unitPrice) {
          total += line.quantity * line.unitPrice;
        } else {
          return '0';
        }
      }
    }

    return total;
  },
)((orderLineAll, cacheName) => cacheName);
