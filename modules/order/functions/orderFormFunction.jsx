import { LOADING_STATE } from '../../../libs/componentHelper';
import { apiGetList } from '../../../libs/apiHelper';
import { createDraft, finishDraft } from 'immer';

export const getUpdatePrice = async (self) => {
  let exchangeRate = 0;
  const { state } = self;
  const { object, pageLoad } = state;
  console.log('CanNT3: getUpdatePrice -> object', object);
  const { orderLineAll } = object;
  let totalFix = 0;
  let totalBudget = 0;
  let total = 0;
  if (orderLineAll != null) {
    orderLineAll.forEach((element) => {
      if (!(element.payType === PAY_TYPE.PAUG)) {
        if (element.quantity && element.suggestPriceVND && element.countTime) {
          totalFix += (+element.quantity) * (+element.suggestPriceVND) * (+element.countTime);
        }
      } else if (element != null && (element.payType) === PAY_TYPE.PAUG) {
        if (element.budgetVND) {
          totalBudget += +element.budgetVND;
        }
      }
    });
    total = totalFix + totalBudget;
  }

  if (pageLoad.exchangeRateId) {
    console.log('CanNT3: getUpdatePrice -> pageLoad.exchangeRateId', pageLoad.exchangeRateId);
    exchangeRate = pageLoad.exchangeRateId.data[0].exchangeRate;
  }
  const currentState = self.state;
  const changedFields = createDraft(object);
  changedFields.exchangeRate = exchangeRate;
  changedFields.orderLineAll.map((product) => {
    product.resellerPriceVND = parseInt(product.resellerPrice * exchangeRate);
  });
  changedFields.totalFix = totalFix;
  changedFields.totalBudget = totalBudget;
  changedFields.total = total;

  const newState = finishDraft(changedFields);
  self.setState({
    ...currentState,
    object: newState,
  });
};

export const getSalePrice = async (self) => {
  const { state } = self;
  const { object } = state;
  const changeObject = createDraft(object);
  const { orderLineAll } = object;
  const currentState = self.state;
  for (let index = 0; index <= orderLineAll.length; index++) {
    const element = orderLineAll[index];
    console.log("CanNT3: getSalePrice -> element", element)
    let price = element.unitPrice;
    orderLineAll[index].saleprice = index;
    console.log("CanNT3: getSalePrice -> price", price)
  }
  const newObject = finishDraft(changeObject);
  self.setState({
    ...currentState,
    object: newObject,
  });
};

export const onProductSearchChange = async (self, e, searchQuery, cacheKey) => {
  e.persist();
  self.setState(LOADING_STATE);

  const pageLoad = { ...self.state.pageLoad }; // change pageLoad to rebuild selector cache
  const { refModels } = self.state;
  const model = refModels.find(f => f.modelName === 'v2/users');
  const { query } = model;

  const listResult = {
    fieldName: model.fieldName,
    refKeyField: model.refKeyField,
    relatedFields: model.relatedFields,
  };

  query.limit = 10;

  if (searchQuery) {
    query.userName = searchQuery;
  }

  const { error, data } = await apiGetList(model.modelName, query);

  if (error) {
    self.setState({
      loading: false,
      error: true,
      messages: apiError2Messages(error),
    });
    return;
  }

  listResult.data = Array.from(data.data);
  pageLoad[cacheKey] = listResult;

  self.setState({
    ...self.state,

    loading: false,
    pageLoad, // cause of pointer, no need to change in setState
  });
};

export const getLicence = async (self, event) => {
  event.preventDefault();
  self.setState(LOADING_STATE);

  const { object } = self.state;

  const { data } = await apiGetList(`v2/esdOrders/getLicenceByOrder`, { orderNumber: object.orderNumber });
  console.log('hungnv104 -> data:', data.data)

  if (data.data === null) {
    self.setState({
      loading: false,
      success: false,
      error: true,
      messages: 'Tạo licence không thành công',
    });
    return;
  }

  self.setState({
    loading: false,
    success: true,
    messages: 'Licence đã được tạo thành công',

    object: {
      ...object,
      orderState: 'completed'
      // receiptList: data.dataReceipt,
      // appliedInvoiceList: data.dataInvoice,
    },
  });
  console.log('hungnv104 -> self.state:', self.state)
}
