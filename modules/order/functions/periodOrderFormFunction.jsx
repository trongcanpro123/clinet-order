import FileSaver from 'file-saver';
import { LOADING_STATE } from '../../../libs/componentHelper';
import { apiGet, apiPost, apiDownloadList } from '../../../libs/apiHelper';
import { apiError2Messages } from '../../../libs/errorHelper';

export const onSendedSalesman = async (self, event) => {
  event.preventDefault();
  self.setState(LOADING_STATE);
  const orderListIDs = [];
  const { refModels, pageLoad, object } = self.state;
  const apiEndpoint = refModels.find(rm => rm.fieldName === 'orderLists').modelName;
  const periodOrderList = pageLoad.orderLists;
  periodOrderList.data.forEach((order, index) => {
    orderListIDs.push(order._id);
  });
  const result = await apiPost(`${apiEndpoint}/statusSM`, orderListIDs);
  const error2 = result.error;
  if (error2) {
    self.setState({
      loading: false,
      error: true,
      messages: apiError2Messages(error2),
    });
  } else {
    const orderListData = await apiGet(apiEndpoint, `?customerFtgsmId=${object.customerFtgsmId}&active=true`);
    pageLoad.orderLists = orderListData.data;
    self.setState({
      loading: false,
      success: true,
      pageLoad: {
        ...self.state.pageLoad,
      },
      messages: 'system:msg.update.success',
    });
  }
};

export const onExports = async (self, event) => {
  event.preventDefault();
  self.setState(LOADING_STATE);
  const { object } = self.state;
  const queryForm = {
    startDate: object.startDate,
    endDate: object.endDate,
    customerOracleCode: object.customerOracleCode,
  };
  const { data, error } = await apiDownloadList('v2/msRequests', 'exportReport', queryForm);
  if (data) {
    self.setState({
      ...self.state,
      loading: false,
    });
    const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    FileSaver.saveAs(blob, 'exportedMS.xlsx');
  }
  else {
    self.setState({
      loading: false,
      error: true,
      messages: apiError2Messages(error),
    });
  }
};
