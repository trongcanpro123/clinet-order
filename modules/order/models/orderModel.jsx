import _ from 'lodash';
import axios from 'axios';
import { DATA_TYPE } from '../../../constants/dataType';
import { ORDER_STATE } from '../constants/orderStateConstants';
import { getRequestHeader } from '../../../libs/apiHelper';
import { API_GATEWAY_URL } from '../../../constants/config';
import { apiError2Messages } from '../../../libs/errorHelper';


export const model = {
  stateName: 's_order',
  modelName: 'v2/sOrders',

  data: {
    _id: { type: DATA_TYPE.ID, defaultValue: 0, required: true },

    orderNumber: {
      type: DATA_TYPE.STRING,
    },
    

  },
  query: {
    fields: {
      type: DATA_TYPE.ARRAY,
      defaultValue: ['orderNumber', 'customerName', 'unitPrice', 'orderState', 'createdAt'],
    },
    sortBy: {
      type: DATA_TYPE.STRING,
      defaultValue: 'orderNumber.desc',
    },
  },
};

export default model;
